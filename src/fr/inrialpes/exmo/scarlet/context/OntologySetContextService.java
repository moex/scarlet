package fr.inrialpes.exmo.scarlet.context;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;
import org.semanticweb.owl.align.Cell;

import fr.inrialpes.exmo.align.cli.Procalign;
import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.BasicParameters;
import fr.inrialpes.exmo.ontowrap.BasicOntology;
import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntologyCache;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.inrialpes.exmo.scarlet.context.jena25Ontoset.OntologyCachedClass;
import fr.inrialpes.exmo.scarlet.context.jena25Ontoset.OntologyCachedOntology;
import fr.inrialpes.exmo.scarlet.context.jena25Ontoset.OntologyDirectConcept;
import fr.inrialpes.exmo.scarlet.context.jena25Ontoset.OntologyDirectOntology;
import fr.inrialpes.exmo.scarlet.context.jena25Ontoset.OntologyCachedClass.RelType;
import fr.inrialpes.exmo.scarlet.matcher.CtxtMatcherConfiguration;
import fr.inrialpes.exmo.scarlet.matcher.conf.Configurable;
import fr.inrialpes.exmo.scarlet.matcher.task.MatchingTask;
import fr.inrialpes.exmo.scarlet.param.RelationRestrictionParam;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.Assertion;
import fr.inrialpes.exmo.scarlet.struct.ConfigurationBean;
import fr.inrialpes.exmo.scarlet.struct.Mapping;
import fr.inrialpes.exmo.scarlet.struct.OntologyFragment;
import fr.inrialpes.exmo.scarlet.util.ReflectionHelper;
import fr.inrialpes.exmo.scarlet.util.filter.CollectionFilter;
import fr.inrialpes.exmo.scarlet.util.filter.FilterCriteria;

public class OntologySetContextService implements ContextService {
		
	static List<String> ontouris = new ArrayList<String>();
	static OntologyCache<HeavyLoadedOntology> ontocache = new OntologyCache<HeavyLoadedOntology>();
	static private ConfigurationBean confOper;
	
	static private Map<String, Map<String, Set<Mapping>>> allMappedSource = new HashMap<String, Map<String, Set<Mapping>>>();
	static private Map<String, Map<String, Set<Mapping>>> allMappedTarget = new HashMap<String, Map<String, Set<Mapping>>>();
	
	//the keyset for this HashMap must be the concept with its entire URI
	private Map<String, Set<String>> allMappedOnlyOntology = new HashMap<String, Set<String>>();
	
	private Set<String> allOntologiesSelected = new HashSet<String>();
	
	// Initializes a set of intermediary ontologies
	public OntologySetContextService(ConfigurationBean confOper){
		this.confOper = confOper;
	}
	
	@Override
	public void init(Properties params) {
		String[] ontoset = {};
	
		for(Enumeration<?> args = params.propertyNames();args.hasMoreElements();){
			String paramName = (String)args.nextElement();
			if(paramName.equalsIgnoreCase("OntologySet")){
				Object paramValue = params.getProperty(paramName);
				ontoset = paramValue.toString().split(" ");
				break;
			}
		}
		for(int i=0; i < ontoset.length; i++){
			ontouris.add(ontoset[i]);
			OntologyCachedOntology o = new OntologyCachedOntology(ontoset[i]);
			URI ontouri = null;
			try {
				ontouri = new URI(o.getOntoURI());
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ontocache.recordOntology(ontouri, (HeavyLoadedOntology)o.getDirectOntology().getOnto());
		}
		
		for(int i =0; i < ontouris.size(); i++){
			HeavyLoadedOntology o = null;
			try {
				o = ontocache.getOntology(new URI(ontouris.get(i)));
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
			List<OntologyFragment<URI>> sourceT = confOper.getSourceTargetOnto();
			allMappedSource = initDataStructures(sourceT.get(0).toStringVersion(), o.getFile().toString());
			allMappedTarget = initDataStructures(sourceT.get(1).toStringVersion(), o.getFile().toString());
			for(String onto : allMappedSource.keySet()){
				Map<String, Set<Mapping>> oftc = allMappedSource.get(onto);
				for(String sourceconcept : oftc.keySet()){
					Set<String> ontos = new HashSet<String>();
					if(null == allMappedOnlyOntology.get(sourceconcept) || allMappedOnlyOntology.get(sourceconcept).isEmpty()){
						ontos.add(onto);
						allMappedOnlyOntology.put(sourceconcept, ontos);
					}
					else{
						ontos = allMappedOnlyOntology.get(sourceconcept);
					    ontos.add(onto);
					    allMappedOnlyOntology.put(sourceconcept, ontos);
					}
				}
			}
			for(String onto : allMappedTarget.keySet()){
				Map<String, Set<Mapping>> oftc = allMappedTarget.get(onto);
				for(String sourceconcept : oftc.keySet()){
					Set<String> ontos = new HashSet<String>();
					if(null == allMappedOnlyOntology.get(sourceconcept) || allMappedOnlyOntology.get(sourceconcept).isEmpty()){
						ontos.add(onto);
						allMappedOnlyOntology.put(sourceconcept, ontos);
					}
					else{
					ontos = allMappedOnlyOntology.get(sourceconcept);
				    ontos.add(onto);
				    allMappedOnlyOntology.put(sourceconcept, ontos);
					}
				}
			}
		}
	}	
	
	public Map<String, Set<String>> getAllMappedOnto(){
		return allMappedOnlyOntology;
	}

	
	// at the moment it anchors...
	public Set<String> getOntosForTerm(String source) throws OntowrapException, URISyntaxException {
		Set<String> ontologies = new HashSet<String>();
		if(null != allMappedOnlyOntology.get(source)){
		ontologies.addAll(allMappedOnlyOntology.get(source));
		}
		return ontologies;
	}
	
	
	public static String localName(String cpt){
		int idx = Math.max(cpt.indexOf("#"), cpt.lastIndexOf("/")) + 1;
		return cpt.substring(idx);
	}


	@Override
	public Set<String> getAllSelectedOntologies() {
//		Set<String> ontos = new HashSet<String>();
//		for(int o=0; o < ontouris.size(); o++){
//			ontos.add(ontouris.get(o));
//		}
//		return ontos;
		return allOntologiesSelected;
	}
	
	public void setAllSelectedOntologies(Set<String> allontos){
		allOntologiesSelected.addAll(allontos);
	}

	@Override
	public Map<String, Assertion<String>> exploreNeighbors(String cpt,
			String ontology, RelationEnum[] restriction) {
		Map<String, Assertion<String>> result = new HashMap<String, Assertion<String>>();
		OntologyDirectOntology o = new OntologyDirectOntology(ontology);
		OntologyDirectConcept dirc = new OntologyDirectConcept(o, cpt);
		OntologyCachedClass co = new OntologyCachedClass(o, dirc);
		for(RelationEnum rel : restriction){
			Collection<String> relby = null;
			try {
				relby = co.getCptsRelatedBy(rel);
			} catch (OntowrapException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(String rb : relby){
				Assertion<String> a = new Assertion<String>(rel, cpt, rb);
				result.put(rb, a);
			}
			
		}
		return result;
	}


	@Override
	public Set<String> getOntologiesForConcept(String concept) {
		Set<String> filtered = null;
//		if(null != filterCriteria){
//			CollectionFilter<String> filter = new CollectionFilter<String>();
//			filter.addFilterCriteria(filterCriteria);
			try {
				filtered = getOntosForTerm( concept );
			} catch (OntowrapException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//filter.filter(filtered);
		//}
		 return filtered;
	}

	
	@Override
	public Map<String, Set<Mapping>> anchor(OntologyFragment<String> sourceOnto, String targetURI, MatchingTask task, String alignmentClassName){
		Map<String, Set<Mapping>> result = new HashMap<String, Set<Mapping>>();
		
		URI uri1 = null; 
		URI uri2 = null;
		try {
			uri1 = new URI(sourceOnto.uri());
			uri2 = new URI (targetURI);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		AlignmentProcess A1 = null;
		try{
		Object[] mparams = {};
		Class<?> alignmentClass = Class.forName(alignmentClassName);
		Class[] cparams = {};
		java.lang.reflect.Constructor alignmentConstructor = alignmentClass.getConstructor(cparams);
		A1 = (AlignmentProcess)alignmentConstructor.newInstance(mparams);
		A1.init( uri1, uri2 );
	    } catch (Exception ex) {
		System.err.println("Cannot create alignment "+alignmentClassName+"\n"
				   +ex.getMessage());
		((Procalign)A1).usage();
		//throw ex;
	    }
	    
		try {			
		
		 	Properties p = new BasicParameters();
		    A1.align((Alignment)null,p);

	    Alignment result3 = null;
		result3 = (BasicAlignment)A1.clone();
		
		//System.out.println("Num cells: " + result3.nbCells());
		
		BasicOntology<Object> bo1 = new BasicOntology<Object>();
		bo1.setFile(uri1);
		LoadedOntology lo1 = bo1.load();
		//JENAOntologyFactory fact = new JENAOntologyFactory();
		//HeavyLoadedOntology o = fact.loadOntology(uri1);
		
		
		for(Object cptl : lo1.getClasses()){
			//Object concept = sourceOnto.iterator().next();
			Set<Cell> cellsForCpt = new HashSet<Cell>();
			//List<Cell> cfcpt = new ArrayList<Cell>();
			try {
				if(null != result3.getAlignCells1(cptl)){
				cellsForCpt.addAll(result3.getAlignCells1(cptl));
				//cfcpt.addAll(result3.getAlignCells1(cptl));
				}
			} catch (AlignmentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Collection<String> anchors = new ArrayList<String>();
			//Collection<String> anchors;
			String Cpt = null;
			if(null != cellsForCpt){
				for(Cell c : cellsForCpt){
					String anchor = c.getObject2AsURI(result3).toString();
					//String anch = anchor.substring(1, anchor.length() - 1);
					anchors.add(anchor);
					Cpt = c.getObject1AsURI(result3).toString();
					//System.out.println("Cell: " + Cpt + " and " + anchor);
				}
				
				task.addMappings(anchors, Cpt, RelationEnum.equiv, result);
			}
		}
		
		} 			
		  catch (AlignmentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OntowrapException owe){
			owe.printStackTrace();
		}
		
		return result;
	}

	
	@Override
	public Set<String> getAllConcepts(String ontologyURI) {
		Set<String> finalc = new HashSet<String>();
		HeavyLoadedOntology o = null;
		try {
			o = ontocache.getOntology(new URI(ontologyURI));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Collection<String> oc = null;
		try {
			oc = o.getClasses();
		} catch (OntowrapException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(String s : oc){
			finalc.add(s);
		}
		return finalc;
	}
	
	
	private Configurable setServiceConfiguration(Properties arguments) {
		Object configuration = arguments.getProperty("conf");
		if (configuration!=null){
			return ReflectionHelper.initMatcherConfiguration((String)configuration);
		} else {
			return new CtxtMatcherConfiguration();
		}
	}

	private Configurable setMatcherConfiguration(Properties arguments) {
		Object configuration = arguments.getProperty("conf");
		if (configuration!=null){
			return ReflectionHelper.initMatcherConfiguration((String)configuration);
		} else {
			return new CtxtMatcherConfiguration();
		}
	}
	
	@Override
	public Set<Mapping> getMappingsForConcept(String concept,
			Set<Mapping> anchors) {
		
		Set<Mapping> maps = new HashSet<Mapping>();
		for(Mapping m : anchors){
			if(localName(m.getDomain()).equalsIgnoreCase(localName(concept))){
					maps.add(m);
			}
		}
		// TODO Auto-generated method stub
		return maps;
	}

	private Map<String, Map<String, Set<Mapping>>> initDataStructures(OntologyFragment<String> source, String target){
		//ds will contain the following structure
		//c1 from source	---> onto2 ----> map c1 c2-from-onto2
		//					---> onto2 ----> map c1 c3-from-onto2
		Map<String, Map<String, Set<Mapping>>> ds = new HashMap<String, Map<String, Set<Mapping>>>();
		MatchingTask task = new MatchingTask();
		Map<String, Set<Mapping>> localmap = anchor(source, target, task, "fr.inrialpes.exmo.align.impl.method.NameEqAlignment");
		for(String concept : localmap.keySet()){
			for(Mapping map : localmap.get(concept)){
				String dom = map.getDomain();
				Set<Mapping> sourcemap = new HashSet<Mapping>();
				Map<String, Set<Mapping>> smap = new HashMap<String, Set<Mapping>>();
				smap = ds.get(target);
				if(null != smap){
					Set<Mapping> lm = smap.get(dom);
					if(null == lm){
						sourcemap.add(map);
						smap.put(dom, sourcemap);
						ds.put(target, smap);
					}
					else{
						sourcemap = ds.get(target).get(dom);
						sourcemap.add(map);
						smap.put(dom, sourcemap);
						ds.put(target, smap);
					}
				}
				else{
						smap = new HashMap<String, Set<Mapping>>();
						sourcemap.add(map);
						smap.put(dom, sourcemap);
						ds.put(target, smap);
					}
	
				}
			}
//		System.out.println("Printing ds for source " + source.uri().toString() + " and target " + target);
//		for(String ta : ds.keySet()){
//			Map<String, Set<Mapping>> c = ds.get(ta);
//			System.out.println("Key ds : " + ta);
//			for(String s : c.keySet()){
//				System.out.println("Key inside " + s);
//				Set<Mapping> mas = c.get(s);
//				for(Mapping m : mas){
//					System.out.println("Mapping set of subkey " + s + " and main key " + ta);
//					System.out.println("Mapping dom " + m.getDomain() + " and range " + m.getRange());
//				}
//			}
//		}
		return ds;
		}
	
	public Map<String, Map<String, Set<Mapping>>> getAllMappedSource(){
		return allMappedSource;
	}
	
	public Map<String, Map<String, Set<Mapping>>> getAllMappedTarget(){
		return allMappedTarget;
	}

	@Override
	public RelationEnum[] getRelationRestriction() {
		//TO DO: pass this as user set parameter
		RelationRestrictionParam relrestr = confOper.getRelationRestrictionParameter();
		if(relrestr.getRelRestriction().equalsIgnoreCase("subsumption")){
			RelationEnum[] restriction = {RelationEnum.subClass, RelationEnum.superClass, 
					RelationEnum.equiv};
			return restriction;
		}
		else if(relrestr.getRelRestriction().equalsIgnoreCase("disjoint")){
			RelationEnum[] restriction = {RelationEnum.subClass, RelationEnum.superClass, 
					RelationEnum.equiv, RelationEnum.disjoint, RelationEnum.overlaps};
			return restriction;
		}
		else{
			RelationEnum[] restriction = {RelationEnum.subClass, RelationEnum.superClass, 
					RelationEnum.equiv, RelationEnum.disjoint, RelationEnum.overlaps, 
					RelationEnum.subsumedOverlaps, RelationEnum.subsumesOverlaps, 
					RelationEnum.notSubsumed, RelationEnum.notSubsumes,
					RelationEnum.disjunctive, RelationEnum.notIncompatible,
					RelationEnum.named, RelationEnum.all};
			return restriction;
		}
	}
}
