package fr.inrialpes.exmo.scarlet.context.jena25Ontoset;

import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDisjointClassesAxiom;
import org.semanticweb.owlapi.model.OWLOntology;

import uk.ac.manchester.cs.owl.owlapi.OWLClassImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataPropertyImpl;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Resource;

import fr.inrialpes.exmo.align.impl.BasicRelation;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.IncompatRelation;
import fr.inrialpes.exmo.align.impl.rel.NonTransitiveImplicationRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumeRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;
import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntologyFactory;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.inrialpes.exmo.ontowrap.owlapi30.OWLAPI3Ontology;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;



public class OntologyDirectConcept implements OntologyConceptProxy {

	
	private HeavyLoadedOntology<OntModel> onto;
	private String concept;
	private String localName = null;

	public OntologyDirectConcept(OntologyDirectOntology onto, String concept) {
		this.onto = onto.getOnto();
		this.concept = concept;
	}
	
	public String getConceptURI(){
		return concept;
	}
	
	public String getOntologyURI(){
		return onto.getURI().toString();
	}
	
	public String getEntityFormat(Object entity){
		String correctF = entity.toString().substring(1, entity.toString().length()-1);
		return correctF;
	}
	
	//@Override
//	public String[] getAllDisjointWith() throws OntowrapException {
//		List<String> dw = new ArrayList<String>();
//			Object c = null;
//			try {
//				c = onto.getEntity(new URI(concept));
//			} catch (URISyntaxException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			Set<OntClass> oc = ((OntClass) c).listDisjointWith().toSet();
//			for(OntClass ooc : oc){
//				dw.add(ooc.getURI().toString());
//		} 
//			String[] finalres = dw.toArray(new String[dw.size()]);
//		return finalres;
//	}
	
	


	//@Override
	public String[] getAllSubClasses() throws OntowrapException {
		List<String> alldesc = new ArrayList<String>();
		List<String> allsub = new LinkedList<String>();
		Object c = null;
		try {
			c = onto.getEntity(new URI(this.concept));
			allsub = getDescendents(c);
			if(null != allsub && !allsub.isEmpty()){
				int i=0;
				for(String s : allsub){
					Object ococ = onto.getEntity(new URI(s));
					alldesc.add(ococ.toString());
				}
			}
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] fianlresStrings = alldesc.toArray(new String[alldesc.size()]);
		return fianlresStrings;
	}

	//@Override
	public String[] getAllSuperClasses() throws OntowrapException {
		List<String> allanc = new ArrayList<String>();
		List<String> allsub = new LinkedList<String>();
		Object c = null;
		try {
			c = onto.getEntity(new URI(concept));
			allsub = getAncestors(c);
			if(null != allsub && !allsub.isEmpty()){
				for(String s : allsub){
					Object ococ = onto.getEntity(new URI(s));
					allanc.add(ococ.toString());
				}
			}
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] fianlresStrings = allanc.toArray(new String[allanc.size()]);
		return fianlresStrings;
	}	

	//@Override
	public String[] getPropertiesFrom() throws OntowrapException {
		List<String> propsFrom = new ArrayList<String>();
		Object c = null;
		try {
			c = onto.getEntity(new URI (concept));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set<Object> conceptP = (Set<Object>) onto.getProperties(c, OntologyFactory.LOCAL, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
		if(null != conceptP && !conceptP.isEmpty()){
			for(Object op : conceptP){
				Set<OWLClassImpl> pres = (Set<OWLClassImpl>) onto.getDomain(op, OntologyFactory.ASSERTED);
				for(OWLClassImpl or : pres){
					String cr = or.toString();
					if(concept.equalsIgnoreCase(cr)){
					propsFrom.add(this.getEntityFormat(op.toString()));
					}
				}
			}
		}
		String[] fianlresStrings = propsFrom.toArray(new String[propsFrom.size()]);
		return fianlresStrings;
	}

	//@Override
	public String[] getPropertiesTo() throws OntowrapException {
		List<String> propsFrom = new ArrayList<String>();
		Object c = null;
		try {
			c = onto.getEntity(new URI (concept));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set<Object> conceptP = (Set<Object>) onto.getProperties(c, OntologyFactory.LOCAL, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
		if(null != conceptP && !conceptP.isEmpty()){
			for(Object op : conceptP){
				Set<OWLClassImpl> pres = (Set<OWLClassImpl>) onto.getDomain(op, OntologyFactory.ASSERTED);
				for(OWLClassImpl or : pres){
					String cr = or.toString();
					if(concept.equalsIgnoreCase(cr)){
					propsFrom.add(this.getEntityFormat(op));
					}
				}
			}
		}
		String[] fianlresStrings = propsFrom.toArray(new String[propsFrom.size()]);
		return fianlresStrings;
	}

	//@Override
	public String[] getSubClasses() throws OntowrapException {
		List<String> cdirectsubclasses = new ArrayList<String>();
		Object c = null;
		try {
			c = onto.getEntity(new URI (concept));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set<? extends Object> oc= (Set<? extends Object>) onto.getSubClasses(c, OntologyFactory.LOCAL, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
		if(null != oc && !oc.isEmpty()){
			for(Object c1 : oc){
				cdirectsubclasses.add(this.getEntityFormat(c1));
			}
		}
		 String[] finalres = cdirectsubclasses.toArray(new String[cdirectsubclasses.size()]);
		 return finalres;
		}

	private List<String> getDescendents(Object concept){
		List<String> desc = new LinkedList<String>();
		Set<? extends Object> oc= (Set<? extends Object>) onto.getSubClasses(concept, OntologyFactory.LOCAL, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
		if(null != oc && !oc.isEmpty()){
			for(Object c : oc){
				desc.addAll(getDescendents(c));
			}
		}
		return desc;
	}
	
	private List<String> getAncestors(Object concept)throws OntowrapException{
		List<String> ance = new LinkedList<String>();
		Set<? extends Object> oc= (Set<? extends Object>) onto.getSuperClasses(concept, OntologyFactory.LOCAL, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
		if(null != oc && !oc.isEmpty()){
			for(Object c : oc){
				ance.addAll(getAncestors(c));
			}
		}
		return ance;
	}

	
	//@Override
	public String[] getSuperClasses() throws OntowrapException {
		List<String> cdirectsupclasses = new ArrayList<String>();
		Object c = null;
		try {
			c = onto.getEntity(new URI (concept));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set<? extends Object> oc= (Set<? extends Object>) onto.getSuperClasses(c, OntologyFactory.LOCAL, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
		if(null != oc && !oc.isEmpty()){
			for(Object c2 : oc){
				cdirectsupclasses.add(this.getEntityFormat(c2));
			}
		}
		String[] finalres = cdirectsupclasses.toArray(new String[cdirectsupclasses.size()]);
		 return finalres;
	}

	//@Override
//	public String[] getEquivalentClasses() throws OntowrapException {
//		List<String> equiv = new ArrayList<String>();
//		Object c = null;
//		try {
//			c = onto.getEntity(new URI(this.concept));
//		} catch (URISyntaxException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Set<OntClass> occ = ((OntClass)c).listEquivalentClasses().toSet();
//		for(OntClass oocc : occ){
//			equiv.add(oocc.getURI());
//		}
//		String[] finalres = equiv.toArray(new String[equiv.size()]);
//		 return finalres;
//	}
	
	//@Override
	public String[] getLabels() throws OntowrapException{
		List<String> en = new ArrayList<String>();
		Object c = null;
		try {
			c = onto.getEntity(new URI (concept));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set<String> sn = onto.getEntityNames(c);
		if(null != sn && !sn.isEmpty()){
			for(String ena : sn){
				en.add(this.getEntityFormat(ena));
			}
		}
		String[] finalres = en.toArray(new String[en.size()]);
		 return finalres;
	}

	//@Override
	public String[] getLiterals() throws OntowrapException {
		//TODO
		return null;
	}

	//@Override
	public String getLocalName() {
		if (localName==null){
			localName = concept.substring(concept.indexOf("#")+1);
		}
		return localName;
	}

	@Override
	public String[] getDataPropertiesFrom() throws OntowrapException {
		List<String> cdataprops = new ArrayList<String>();
		Object c = null;
		try {
			c = onto.getEntity(new URI (concept));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set<Object> conceptP = (Set<Object>) onto.getProperties(c, OntologyFactory.LOCAL, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
		if(null != conceptP && !conceptP.isEmpty()){
			for(Object op : conceptP){
				Set<OWLClassImpl> pres = (Set<OWLClassImpl>) onto.getDomain(op, OntologyFactory.ASSERTED);
				for(OWLClassImpl or : pres){
					String cr = or.toString();
					if(concept.equalsIgnoreCase(cr)){
						cdataprops.add(this.getEntityFormat(op));
					}
				}
			}
		}
		String[] finalres = cdataprops.toArray(new String[cdataprops.size()]);
		 return finalres;
	}

	@Override
	public String[] getObjectPropertiesFrom() throws OntowrapException {
		List<String> oprops = new ArrayList<String>();
		Object c = null;
		try {
			c = onto.getEntity(new URI (concept));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set<Object> conceptP = (Set<Object>) onto.getProperties(c, OntologyFactory.LOCAL, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
		if(null != conceptP && !conceptP.isEmpty()){
			for(Object op : conceptP){
				Set<OWLClassImpl> pres = (Set<OWLClassImpl>) onto.getDomain(op, OntologyFactory.ASSERTED);
				for(OWLClassImpl or : pres){
					String cr = or.toString();
					if(concept.equalsIgnoreCase(cr)){
						oprops.add(this.getEntityFormat(op));
					}
				}
			}
		}
		String[] finalres = oprops.toArray(new String[oprops.size()]);
		 return finalres;
	}

	@Override
	public String[] getObjectPropertiesTo() throws OntowrapException {
		List<String> oprops = new ArrayList<String>();
		Object c = null;
		try {
			c = onto.getEntity(new URI (concept));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set<Object> conceptP = (Set<Object>) onto.getProperties(c, OntologyFactory.LOCAL, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
		if(null != conceptP && !conceptP.isEmpty()){
			for(Object op : conceptP){
				Set<OWLClassImpl> pres = (Set<OWLClassImpl>) onto.getDomain(op, OntologyFactory.ASSERTED);
				for(OWLClassImpl or : pres){
					String cr = or.toString();
					if(concept.equalsIgnoreCase(cr)){
						oprops.add(this.getEntityFormat(op));
					}
				}
			}
		}
		String[] finalres = oprops.toArray(new String[oprops.size()]);
		 return finalres;
	}

	@Override
	public String[] getInstances() throws OntowrapException {
		List<String> ind = new ArrayList<String>();
		Object c = null;
		try {
			c = onto.getEntity(new URI (concept));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set<?> ins = onto.getInstances(c, OntologyFactory.LOCAL, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
		if(null != ins && !ins.isEmpty()){
			for(Object in : ins){
				ind.add(this.getEntityFormat(in));
			}
		}
		String[] finalres = ind.toArray(new String[ind.size()]);
		 return finalres;
	}
}
