package fr.inrialpes.exmo.scarlet.context.jena25Ontoset;

import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.ontology.OntModel;

import fr.inrialpes.exmo.ontowrap.Annotation;
import fr.inrialpes.exmo.ontowrap.BasicOntology;
import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.inrialpes.exmo.ontowrap.jena25.JENAOntology;

import uk.ac.open.kmi.scarlet.utils.OntologyFinder;
import uk.ac.open.kmi.scarlet.utils.Utilities;
import uk.ac.open.kmi.watson.clientapi.EntityResult;
import uk.ac.open.kmi.watson.clientapi.EntitySearch;
import uk.ac.open.kmi.watson.clientapi.Measures;
import uk.ac.open.kmi.watson.clientapi.SearchConf;
import uk.ac.open.kmi.watson.clientapi.SemanticContentSearch;
//import fr.inrialpes.exmo.scarlet.concurrent.NotThreadSafe;

//@NotThreadSafe
public class OntologyDirectOntology extends JENAOntology implements OntologyProxy<OntModel> {

	private HeavyLoadedOntology<OntModel> onto;
	
	public OntologyDirectOntology(String ontoUri){
		BasicOntology<Object> o = new BasicOntology<Object>();
		try {
			o.setURI(new URI(ontoUri));
			o.setFile(new URI(ontoUri));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LoadedOntology<OntModel> loadedonto = null;
		try {
			loadedonto = (LoadedOntology<OntModel>)o.load();
		} catch (OntowrapException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		onto = (HeavyLoadedOntology)loadedonto;
	}
	
	//@Override
	public OntologyConceptProxy getConcept(String concept) {
		return new OntologyDirectConcept(this, concept);
	}

	//@Override
	public OntologyPropertyProxy getRelation(String relation) {
		return new OntologyDirectProperty(this, relation);
	}
	
	public String getOntoUri(){
		return this.onto.getURI().toString();
	}

	public HeavyLoadedOntology<OntModel> getOnto(){
		return onto;
	}

}
