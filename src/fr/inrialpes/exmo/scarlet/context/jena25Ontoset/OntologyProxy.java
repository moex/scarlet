package fr.inrialpes.exmo.scarlet.context.jena25Ontoset;

import org.semanticweb.owl.align.Alignment;

import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;

public interface OntologyProxy<O> extends HeavyLoadedOntology<O> {

	public OntologyConceptProxy getConcept(String concept);

	public OntologyPropertyProxy getRelation(String relation);

}
