package fr.inrialpes.exmo.scarlet.context.jena25Ontoset;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import uk.ac.open.kmi.watson.clientapi.EntitySearch;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.inrialpes.exmo.scarlet.cache.Computable;
import fr.inrialpes.exmo.scarlet.cache.Memoizer;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;

public class OntologyCachedClass implements OntologyConceptProxy {

	private static OntologyDirectConcept concept;
	private String localName = null;
	private static OntologyDirectOntology ontology;
	
	private final Map<RelType, String[]> relatedConcepts = new HashMap<RelType, String[]>();
	
	public static OntologyDirectConcept getConcept(){
		return concept;
	}
	public static OntologyDirectOntology getOntology(){
		return ontology;
	}

	
	public static enum RelType { // FIXME: all sub classes -> too much in the cache!
		ALL_SUPER_CLASSES(){
			String[] get() throws OntowrapException{
				return getConcept().getAllSuperClasses();
			}
		}, SUPER_CLASSES {
			@Override
			String[] get() throws OntowrapException{
				return getConcept().getSuperClasses();
			}
		}, ALL_SUB_CLASSES {
			@Override
			String[] get() throws OntowrapException{
				return getConcept().getAllSuperClasses();
			}
		}, SUB_CLASSES {
			@Override
			String[] get() throws OntowrapException{
				return getConcept().getSubClasses();
			}
		}, 
//			ALL_DISJOINT_WITH {
//			@Override
//			String[] get() throws OntowrapException{
//				return getConcept().getAllDisjointWith(); // FIXME or 	getIsDisjointWith ?
//					// getIsDisjointWith = list of classes that are DECLARED to be disjoint 
//					// with the given class in the given document
//			}
//		}, 
		PROPERTIES_FROM {
			@Override
			String[] get() throws OntowrapException {
				return getConcept().getPropertiesFrom(); 
			}
		}, PROPERTIES_TO{
			@Override
			String[] get() throws OntowrapException {
				return getConcept().getPropertiesTo(); 
			}
		}, OBJECT_PROPERTIES_FROM {
			@Override
			String[] get() throws OntowrapException {
				return getConcept().getObjectPropertiesFrom(); 
			}
		}, OBJECT_PROPERTIES_TO{
			@Override
			String[] get() throws OntowrapException {
				return getConcept().getObjectPropertiesTo(); 
			}
		}, DATA_PROPERTIES_FROM {
			@Override
			String[] get() throws OntowrapException {
				return getConcept().getDataPropertiesFrom(); 
			}
		},
//		EQUIVALENT{
//			@Override
//			String[] get() throws OntowrapException {
//				return getConcept().getEquivalentClasses(); 
//			}
//		}, 
		LITERALS {
			@Override
			String[] get() throws OntowrapException {
				return getConcept().getLiterals();
			}
		}
		, LABELS{
			@Override
			String[] get() throws OntowrapException {
				return getConcept().getLabels(); 
			}
		},
		INSTANCES{
			String[] get() throws OntowrapException {
				return getConcept().getInstances(); 
			}
			
		};
		
		abstract String[] get() throws OntowrapException;
	};

	public String cgetURI() {
		return getConcept().getConceptURI();
	}
	
	public String ogetURI() {
		return getOntology().getOntoUri();
	}
	
	public OntologyCachedClass(OntologyDirectOntology ontology, OntologyDirectConcept concept){
		this.ontology = ontology;
		this.concept = concept;
		for(int j=0; j < RelType.values().length; j++){
			RelType rel = RelType.values()[j];
		try {
			String[] relconc = rel.get();
			relatedConcepts.put(rel, relconc);
		} catch (OntowrapException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		
//		= new Memoizer<RelType, String[]> (new Computable<RelType, String[]>(){
//			//@Override
//			public String[] compute(RelType arg) throws InterruptedException, RemoteException, OntowrapException{
//				return arg.get();
//			}
//		});
	}
	

	//@Override
//	public String[] getAllDisjointWith() throws OntowrapException {
//		//try {
//			//return relatedConcepts.compute(RelType.ALL_DISJOINT_WITH);
//			return relatedConcepts.get(RelType.ALL_DISJOINT_WITH);
////		} catch (InterruptedException e) {
////			return new String[0]; 
////		}
//	}

	//@Override
	public String[] getAllSubClasses() throws OntowrapException {
		//try {
		//return relatedConcepts.compute(RelType.ALL_SUB_CLASSES);
		return relatedConcepts.get(RelType.ALL_SUB_CLASSES);
//	} catch (InterruptedException e) {
//		return new String[0]; 
//	}
	}

	//@Override
	public String[] getAllSuperClasses() throws OntowrapException {
		//try {
		//return relatedConcepts.compute(RelType.ALL_SUPER_CLASSES);
		return relatedConcepts.get(RelType.ALL_SUPER_CLASSES);
//	} catch (InterruptedException e) {
//		return new String[0]; 
//	}
	}

	//@Override
	public String[] getSubClasses() throws OntowrapException {
		//try {
		//return relatedConcepts.compute(RelType.SUB_CLASSES);
		return relatedConcepts.get(RelType.SUB_CLASSES);
//	} catch (InterruptedException e) {
//		return new String[0]; 
//	}
	}

	//@Override
	public String[] getSuperClasses() throws OntowrapException {
		//try {
		//return relatedConcepts.compute(RelType.SUPER_CLASSES);
		return relatedConcepts.get(RelType.SUPER_CLASSES);
//	} catch (InterruptedException e) {
//		return new String[0]; 
//	}
	}

	//@Override
	public String[] getPropertiesFrom() throws OntowrapException {
		//try {
		//return relatedConcepts.compute(RelType.PROPERTIES_FROM);
		return relatedConcepts.get(RelType.PROPERTIES_FROM);
//	} catch (InterruptedException e) {
//		return new String[0]; 
//	}
	}

	//@Override
	public String[] getPropertiesTo() throws OntowrapException {
		//try {
		//return relatedConcepts.compute(RelType.PROPERTIES_TO);
		return relatedConcepts.get(RelType.PROPERTIES_TO);
//	} catch (InterruptedException e) {
//		return new String[0]; 
//	}
	}
	
	//@Override
	public String[] getObjectPropertiesFrom() throws OntowrapException {
		//try {
		//return relatedConcepts.compute(RelType.OBJECT_PROPERTIES_FROM);
		return relatedConcepts.get(RelType.OBJECT_PROPERTIES_FROM);
//	} catch (InterruptedException e) {
//		return new String[0]; 
//	}
	}

	//@Override
	public String[] getObjectPropertiesTo() throws OntowrapException {
		//try {
		//return relatedConcepts.compute(RelType.OBJECT_PROPERTIES_TO);
		return relatedConcepts.get(RelType.OBJECT_PROPERTIES_TO);
//	} catch (InterruptedException e) {
//		return new String[0]; 
//	}
	}
	
	//@Override
	public String[] getDataPropertiesFrom() throws OntowrapException {
		//try {
		//return relatedConcepts.compute(RelType.DATA_PROPERTIES_FROM);
		return relatedConcepts.get(RelType.DATA_PROPERTIES_FROM);
//	} catch (InterruptedException e) {
//		return new String[0]; 
//	}
	}

	//@Override
	public String[] getInstances() throws OntowrapException {
		//try {
		//return relatedConcepts.compute(RelType.INSTANCES);
		return relatedConcepts.get(RelType.INSTANCES);
//	} catch (InterruptedException e) {
//		return new String[0]; 
//	}
	}
	
	//@Override
//	public String[] getEquivalentClasses() throws OntowrapException {
//		//try {
//		//return relatedConcepts.compute(RelType.EQUIVALENT);
//		return relatedConcepts.get(RelType.EQUIVALENT);
////	} catch (InterruptedException e) {
////		return new String[0]; 
////	}
//	}

	//@Override
	public String[] getLabels() throws OntowrapException {
		//try {
		//return relatedConcepts.compute(RelType.LABELS);
		return relatedConcepts.get(RelType.LABELS);
//	} catch (InterruptedException e) {
//		return new String[0]; 
//	}
	}

	//@Override
	public String[] getLiterals() throws OntowrapException {
		//try {
		//return relatedConcepts.compute(RelType.LITERALS);
		return relatedConcepts.get(RelType.LITERALS);
//	} catch (InterruptedException e) {
//		return new String[0]; 
//	}
	}

	//@Override
	public String getLocalName() {
		if (localName ==null){
			localName = concept.getLocalName().substring(concept.getLocalName().indexOf("#")+1);
		}
		return localName;
	}
	
	private RelType[] fromRelationEnumToRelType(RelationEnum rel){
		switch(rel){
		case subClass :return new RelType[]{RelType.SUPER_CLASSES};
		case superClass :return new RelType[]{RelType.SUB_CLASSES};
		//case disjoint:return new RelType[]{RelType.ALL_DISJOINT_WITH};
		case named:return new RelType[]{RelType.PROPERTIES_FROM, RelType.PROPERTIES_TO};
		//case equiv:return new RelType[]{RelType.EQUIVALENT};
		case nontransitiveimplication: 
		case overlaps:
		case doNotKnow:
		default:return new RelType[]{}; 
		}
	}
		
	public Collection<String> getCptsRelatedBy(RelationEnum rel) throws OntowrapException {
		Collection<String> result = new ArrayList<String>();
		//try {
			String[] data = new String[]{};
			for(RelType info: fromRelationEnumToRelType(rel)){
				//data = relatedConcepts.compute(info);
				relatedConcepts.put(info, info.get());
				data = info.get();
			}
			result.addAll(Arrays.asList(data));
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
		return result;
	}
}


