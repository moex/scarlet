package fr.inrialpes.exmo.scarlet.context.jena25Ontoset;

import fr.inrialpes.exmo.ontowrap.OntowrapException;


public interface OntologyConceptProxy {

    public java.lang.String[] getSuperClasses() throws OntowrapException;
    public java.lang.String[] getSubClasses() throws OntowrapException;

    public java.lang.String[] getAllSubClasses() throws OntowrapException;
    public java.lang.String[] getAllSuperClasses() throws OntowrapException;

    //public java.lang.String[] getAllDisjointWith() throws OntowrapException;
    
    public java.lang.String[] getPropertiesFrom() throws OntowrapException;
    public java.lang.String[] getPropertiesTo() throws OntowrapException;
    public java.lang.String[] getDataPropertiesFrom() throws OntowrapException;
    public java.lang.String[] getObjectPropertiesFrom() throws OntowrapException;
    public java.lang.String[] getObjectPropertiesTo() throws OntowrapException;

    //public java.lang.String[] getEquivalentClasses() throws OntowrapException;
	
    public String[] getLabels() throws OntowrapException;
    public String[] getLiterals() throws OntowrapException;
    
    public String getLocalName();
    
    public java.lang.String[] getInstances() throws OntowrapException;
}
