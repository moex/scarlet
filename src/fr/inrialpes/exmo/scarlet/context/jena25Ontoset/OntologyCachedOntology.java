package fr.inrialpes.exmo.scarlet.context.jena25Ontoset;

import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.xml.rpc.ServiceException;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;

import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.inrialpes.exmo.ontowrap.jena25.JENAOntology;
import fr.inrialpes.exmo.scarlet.cache.Computable;
import fr.inrialpes.exmo.scarlet.cache.Memoizer;

public class OntologyCachedOntology extends JENAOntology implements OntologyProxy<OntModel> {
	
	private final String ontology;	
	private OntologyDirectOntology insideOntologyProxy; 

	private final ConcurrentMap<String, OntologyCachedClass> cachedConcepts = new ConcurrentHashMap<String, OntologyCachedClass>();
	private final ConcurrentMap<String, OntologyDirectProperty> cachedRelations = new ConcurrentHashMap<String, OntologyDirectProperty>();

	private Collection<String> allClasses = null;
	
	private boolean initialized = false;
	
	public OntologyCachedOntology(String ontology) {
		this.ontology = ontology;
		insideOntologyProxy = new OntologyDirectOntology(ontology);
	}
		
	public String getOntoURI() {
		return this.ontology.toString();
	}
	
	//@Override
	public OntologyCachedClass getConcept(String concept){
		if (cachedConcepts.containsKey(concept)){
			return cachedConcepts.get(concept);	
		} else {
			OntologyDirectOntology odo = new OntologyDirectOntology(ontology);
			OntologyDirectConcept odc = new OntologyDirectConcept(odo, concept);
			OntologyCachedClass newlyCreated = new OntologyCachedClass(odo, odc);
			cachedConcepts.put(concept, newlyCreated); 
			return newlyCreated;
		}
	}
	
	//@Override
	public OntologyDirectProperty getRelation(String property){
		if (cachedConcepts.containsKey(property)){
			return cachedRelations.get(property);	
		} else {
			OntologyDirectProperty newlyCreated = new OntologyDirectProperty(this, property);
			cachedRelations.put(property, newlyCreated); 
			return newlyCreated;
		}
	}

	//@Override
	public synchronized Collection<String> getAllClasses() throws OntowrapException {
		if (allClasses==null){
			Set<OntClass> alc = insideOntologyProxy.getClasses();
			for(OntClass oc : alc){
				allClasses.add(oc.getURI());
			}
		}
		return allClasses;
	}
	
	public OntologyDirectOntology getDirectOntology(){
		return insideOntologyProxy;
	}

}
