package fr.inrialpes.exmo.scarlet.context.jena25Ontoset;

import java.rmi.RemoteException;
import java.util.Set;

import com.hp.hpl.jena.ontology.OntResource;

import fr.inrialpes.exmo.ontowrap.OntologyFactory;
import fr.inrialpes.exmo.ontowrap.OntowrapException;

import uk.ac.open.kmi.watson.clientapi.EntitySearch;

public class OntologyDirectProperty implements OntologyPropertyProxy {

	private OntologyDirectOntology onto;
	private OntologyCachedOntology conto;
	private String property;
	
	public OntologyDirectProperty(OntologyDirectOntology onto, String property) {
		this.onto = onto;
		this.property = property;
	}
	
	public OntologyDirectProperty(OntologyCachedOntology onto, String property) {
		this.conto = onto;
		this.property = property;
	}
	
	//@Override
	public String[] getDomain() throws OntowrapException {
		String[] domClasses = {};
		Set<? extends OntResource> or = onto.getDomain(this.getProperty(), OntologyFactory.ASSERTED);
		int i = 0;
		for(OntResource ores : or){
				domClasses[i] = ores.toString();
				i++;
			}
		return domClasses;
	}

	//@Override
	public String[] getRange() throws OntowrapException {
		String[] rangeClasses = {};
		Set<? extends OntResource> or = onto.getRange(this.getProperty(), OntologyFactory.ASSERTED);
		int i = 0;
		for(OntResource ores : or){
			if(isObjectProperty(this)){
				rangeClasses[i] = ores.toString();
				i++;
			}
		}
		return rangeClasses;
	}

	@Override
	public boolean isDataProperty(Object o) throws OntowrapException {
		return onto.isDataProperty(o);
	}

	@Override
	public boolean isObjectProperty(Object o) throws OntowrapException {
		return onto.isObjectProperty(o);
	}

	public String getProperty() {
		return property;
	}
	
}
