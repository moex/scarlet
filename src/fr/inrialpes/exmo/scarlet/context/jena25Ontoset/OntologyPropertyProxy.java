package fr.inrialpes.exmo.scarlet.context.jena25Ontoset;

import fr.inrialpes.exmo.ontowrap.OntowrapException;


public interface OntologyPropertyProxy {
	
	boolean isDataProperty( Object o ) throws OntowrapException;
    boolean isObjectProperty( Object o ) throws OntowrapException;

	String[] getRange() throws OntowrapException;

	String[] getDomain() throws OntowrapException;

}
