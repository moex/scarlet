package fr.inrialpes.exmo.scarlet.context;

import java.lang.reflect.Constructor;
import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;
import org.semanticweb.owl.align.Cell;

import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.BasicParameters;
import fr.inrialpes.exmo.align.impl.method.SubsDistNameAlignment;
import fr.inrialpes.exmo.ontowrap.BasicOntology;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonCachedService;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonConceptProxy;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonCachedClass;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonOntologyProxy;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonService;
import fr.inrialpes.exmo.scarlet.matcher.CtxtMatcherConfiguration;
import fr.inrialpes.exmo.scarlet.matcher.conf.Configurable;
import fr.inrialpes.exmo.scarlet.matcher.conf.ParameterCombination;
import fr.inrialpes.exmo.scarlet.matcher.task.MatchingTask;
import fr.inrialpes.exmo.scarlet.matcher.task.SelectionTask;
import fr.inrialpes.exmo.scarlet.param.RelationRestrictionParam;
import fr.inrialpes.exmo.scarlet.param.method.MatchingMethod;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.Assertion;
import fr.inrialpes.exmo.scarlet.struct.ConfigurationBean;
import fr.inrialpes.exmo.scarlet.struct.Mapping;
import fr.inrialpes.exmo.scarlet.struct.OntologyFragment;
import fr.inrialpes.exmo.scarlet.util.ReflectionHelper;
import fr.inrialpes.exmo.scarlet.util.filter.CollectionFilter;
import fr.inrialpes.exmo.scarlet.util.filter.FilterCriteria;
import fr.inrialpes.exmo.align.cli.Procalign;

public class WatsonContextService implements ContextService{

	
	private static WatsonService wcs = null;
	private ConfigurationBean confOper;
	//private Configurable config;

//	static private Map<String, Map<String, Set<Mapping>>> allMappedSource = new HashMap<String, Map<String, Set<Mapping>>>();
//	static private Map<String, Map<String, Set<Mapping>>> allMappedTarget = new HashMap<String, Map<String, Set<Mapping>>>();
	
	//the keyset for this HashMap must be the concept with its entire URI
	private Map<String, Set<String>> allMappedOnlyOntology = new HashMap<String, Set<String>>();
	
	private Set<String> allOntologiesSelected = new HashSet<String>();

	
	public WatsonContextService(ConfigurationBean confOper){
		this.confOper = confOper;
	}
	
	@Override
	public void init(Properties params) {
		wcs = WatsonCachedService.getInstance();
	}

	@Override
	public Set<String> getAllSelectedOntologies() throws RemoteException {
//		//This is how it should be
//		//Test on Watson retrieval of ontologies crawled
//		String[] allontologies = wcs.getSemanticContentSearch().listSemanticContents(0, 500);
//		List<String> allontos = Arrays.asList(allontologies);
//		//to be refined for example with getSemanticContentLanguages and / or getDLExpressivness methods
//		//to see if we are retrieving an ontology or some other document that is in Watson
//		Set<String> finalallontos = new HashSet<String>();
//		for(String onto : allontos){
//			finalallontos.add(onto);
//		}
//		return finalallontos;
		
		//This is how it is at the moment
		return allOntologiesSelected;
	}
	
	public void setAllSelectedOntologies(Set<String> allontos){
		allOntologiesSelected.addAll(allontos);
	}
	
	public Map<String, Set<String>> getAllMappedOnto(){
		return allMappedOnlyOntology;
	}

	@Override
	public Map<String, Assertion<String>> exploreNeighbors(String cpt,
			String ontology, RelationEnum[] restriction) throws RemoteException {
		Map<String, Assertion<String>> result = new HashMap<String, Assertion<String>>();
		WatsonConceptProxy cptProxy = wcs.getOntology(ontology).getConcept(cpt);
	 	for(RelationEnum rel: restriction){
					for(String related: ((WatsonCachedClass)cptProxy).getCptsRelatedBy(rel)){
	 				Assertion<String> assertion = new Assertion<String>(rel, cpt, related);
	 				result.put(related, assertion);
	 				}
	 		}
	 	return result;
	}

	@Override
	public Collection<String> getOntologiesForConcept(String concept) throws RemoteException {
		Set<String> ontologies = new HashSet<String>();
		String term = localName(concept);
		try {
			ontologies.addAll(wcs.getOntosForTerm(term));
//			if(null != filterCriteria){
//				CollectionFilter<String> filter = new CollectionFilter<String>();
//				filter.addFilterCriteria(filterCriteria);
//				filter.filter(ontologies);
//			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		allMappedOnlyOntology.put(concept, ontologies);
		return ontologies;
	}
	
	
	public Map<String, Set<Mapping>> anchor(OntologyFragment<String> sourceOnto, String targetURI, MatchingTask task, String method){
		Map<String, Set<Mapping>> anchors = new HashMap<String, Set<Mapping>>();
		//MatchingMethod method = task.getConfigurationBean().getMatchingMethod();
		//anchors = method.match(this, task, sourceOnto, targetURI);
		if(method.equals("WATSON_URI_BASED")){
			Collection<String> targetConcepts = null;
			try {
				targetConcepts = getAllConcepts(targetURI);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			targetConcepts.retainAll(sourceOnto.concepts());
			for (String anchor : targetConcepts) {
				task.addMapping(anchor, new Mapping(anchor, anchor,
						RelationEnum.equiv), anchors);
			}
		}
		if(method.equals("WATSON_LOCALNAME_BASED")){
			for (String sourceConcept : sourceOnto) {
				String term = sourceConcept.substring(sourceConcept.indexOf("#")+1);
				Collection<String> targetConcepts =  null;
				try {
					targetConcepts = getConceptsByExactMatch(
							term, targetURI);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				task.addMappings(targetConcepts, sourceConcept, RelationEnum.equiv,
						anchors);
			}
		}
		if(method.equals("WATSON_TOKEN_BASED")){
			for (String sourceConcept : sourceOnto) {
				String term = sourceConcept.substring(sourceConcept.indexOf("#")+1);
				Collection<String> targetConcepts = null;
				try {
					targetConcepts = getConceptsByKeyword(
							term, targetURI);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				task.addMappings(targetConcepts, sourceConcept, RelationEnum.equiv,
						anchors);
			}
		}
		return anchors;
	}

	@Override
	public Collection<String> getAllConcepts(String ontologyURI) throws RemoteException {
		 WatsonOntologyProxy ontoProxy = wcs.getOntology(ontologyURI);
			return ontoProxy.getAllClasses();
	}

    private Collection<String> getConceptsByKeyword(String concept, String ontologyURI) throws RemoteException{
	 	WatsonOntologyProxy ontoProxy = wcs.getOntology(ontologyURI);
		return ontoProxy.getConceptBasedOnKeyword(concept);
	}
    
    private Collection<String> getConceptsByExactMatch(String concept, String ontologyURI) throws RemoteException{
	 	WatsonOntologyProxy ontoProxy = wcs.getOntology(ontologyURI);
	 	return ontoProxy.getConceptsByExactMatch(concept);
         }
	
	private Configurable setServiceConfiguration(Properties arguments) {
		Object configuration = arguments.getProperty("conf");
		if (configuration != null){
			return ReflectionHelper.initMatcherConfiguration((String)configuration);
		} else {
			return new CtxtMatcherConfiguration();
		}
	}

	@Override
	public Set<Mapping> getMappingsForConcept(String concept,
			Set<Mapping> anchors) {
		
		Set<Mapping> maps = new HashSet<Mapping>();
		for(Mapping m : anchors){
			if(m.getDomain().equalsIgnoreCase(concept)){
					maps.add(m);
			}
		}
		// TODO Auto-generated method stub
		return maps;
	}
	
	public static String localName(String cpt){
		int idx = Math.max(cpt.indexOf("#"), cpt.lastIndexOf("/")) + 1;
		return cpt.substring(idx);
	}
	
	@Override
	public RelationEnum[] getRelationRestriction() {
		//TO DO: pass this as user set parameter
		RelationRestrictionParam relrestr = confOper.getRelationRestrictionParameter();
		if(relrestr.getRelRestriction().equalsIgnoreCase("subsumption")){
			RelationEnum[] restriction = {RelationEnum.subClass, RelationEnum.superClass, 
					RelationEnum.equiv};
			return restriction;
		}
		else if(relrestr.getRelRestriction().equalsIgnoreCase("disjoint")){
			RelationEnum[] restriction = {RelationEnum.subClass, RelationEnum.superClass, 
					RelationEnum.equiv, RelationEnum.disjoint, RelationEnum.overlaps};
			return restriction;
		}
		else{
			RelationEnum[] restriction = {RelationEnum.subClass, RelationEnum.superClass, 
					RelationEnum.equiv, RelationEnum.disjoint, RelationEnum.overlaps, 
					RelationEnum.subsumedOverlaps, RelationEnum.subsumesOverlaps, 
					RelationEnum.notSubsumed, RelationEnum.notSubsumes,
					RelationEnum.disjunctive, RelationEnum.notIncompatible,
					RelationEnum.named, RelationEnum.all};
			return restriction;
		}
	}
	
}
