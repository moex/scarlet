package fr.inrialpes.exmo.scarlet.context;

import java.net.URI;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.semanticweb.owl.align.AlignmentProcess;

import fr.inrialpes.exmo.scarlet.matcher.task.MatchingTask;
import fr.inrialpes.exmo.scarlet.matcher.task.SelectionTask;
import fr.inrialpes.exmo.scarlet.param.method.MatchingMethod;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.Assertion;
import fr.inrialpes.exmo.scarlet.struct.Mapping;
import fr.inrialpes.exmo.scarlet.struct.OntologyFragment;
import fr.inrialpes.exmo.scarlet.util.filter.FilterCriteria;

public interface ContextService {

	 // Initialises the service with relevant parameters
	  public void init(Properties params);
	  
	  // Return all the objects (ontologies, alignments, and so on...) available for a ContextService
	  public Set<String> getAllSelectedOntologies() throws RemoteException;
	  public void setAllSelectedOntologies(Set<String> selectedOntos);
	  //public Set<String> getAllContextObjects();
	  
	  // Return all the concepts in a particular ontology URI
	  public Collection<String> getAllConcepts(String ontologyURI) throws RemoteException;
  
	  /* Inside traversal */
	  // Find all concepts related to cpt by a relation in restriction within an ontology
      public Map<String, Assertion<String>> exploreNeighbors(String cpt, String ontology, RelationEnum[] restriction) throws RemoteException;
 
	  /* Outside traversal */
 	  // Find the ontologies (satisfying criteria? e.g. popularity, domain, size, etc...) which contain a concept named concept
      public Collection<String> getOntologiesForConcept(String concept) throws RemoteException;
      
      //providing anchoring methods based on AlignmentProcess and on MatchingMethod
      public Map<String, Set<Mapping>> anchor(OntologyFragment<String> sourceOnto, String targetURI, MatchingTask task, String method);

      public Set<Mapping> getMappingsForConcept(String concept, Set<Mapping> anchors);
      
      public RelationEnum[] getRelationRestriction();
}
