package fr.inrialpes.exmo.scarlet.context.watson;

import java.rmi.RemoteException;
import java.util.Collection;

import fr.inrialpes.exmo.scarlet.context.ContextService;

import uk.ac.open.kmi.watson.clientapi.EntitySearch;
import uk.ac.open.kmi.watson.clientapi.Measures;
import uk.ac.open.kmi.watson.clientapi.SemanticContentSearch;

public interface WatsonService {

	public WatsonOntologyProxy getOntology(String onto);
		
	public Collection<String> getOntosForKeywords(String source, String target) throws RemoteException;
	
	public Collection<String> getOntosForTerm(String term) throws RemoteException;

	public EntitySearch getEntitySearch();

	public Measures getMeasures();

	public SemanticContentSearch getSemanticContentSearch();
	
	
}
