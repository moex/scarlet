package fr.inrialpes.exmo.scarlet.context.watson;

import java.rmi.RemoteException;
import java.util.Collection;

public interface WatsonOntologyProxy {

	public WatsonConceptProxy getConcept(String concept);

	public WatsonPropertyProxy getRelation(String relation);
	
	public Collection<String> getConceptBasedOnKeyword (String term) throws RemoteException;

	public Collection<String> getAllClasses() throws RemoteException;

	public int getSize();

	public double getPopularity();

	public Collection<String> getConceptsByExactMatch(String localName) throws RemoteException;
	
}
