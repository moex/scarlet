package fr.inrialpes.exmo.scarlet.context.watson;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.xml.rpc.ServiceException;

public class WatsonDirectService extends AbstractWatsonService implements WatsonService {

	private static WatsonDirectService watsonService = null;
	
	private WatsonDirectService() throws ServiceException{
		super(Address.KMIaddress);
	}
	
	//@Override
	public WatsonOntologyProxy getOntology(String onto) {
		return new WatsonDirectOntology(onto, watsonService);
	}

	//@Override
	public Collection<String> getOntosForKeywords(String source, String target) throws RemoteException {
		return of.getOntosForKeywords(source, target);
	}

	//@Override
	public Collection<String> getOntosForTerm(String term)throws RemoteException {
		return of.getOntosForTerm(term);
	}

	public static synchronized WatsonService getInstance() throws ServiceException {
		if (watsonService==null){
			watsonService = new WatsonDirectService();
		}
		return watsonService;
	}

}
