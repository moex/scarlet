package fr.inrialpes.exmo.scarlet.context.watson;

import javax.xml.rpc.ServiceException;

import uk.ac.open.kmi.scarlet.utils.OntologyFinder;
import uk.ac.open.kmi.watson.clientapi.EntitySearch;
import uk.ac.open.kmi.watson.clientapi.EntitySearchServiceLocator;
import uk.ac.open.kmi.watson.clientapi.Measures;
import uk.ac.open.kmi.watson.clientapi.MeasuresServiceLocator;
import uk.ac.open.kmi.watson.clientapi.SemanticContentSearch;
import uk.ac.open.kmi.watson.clientapi.SemanticContentSearchServiceLocator;

public abstract class AbstractWatsonService implements WatsonService {

	protected EntitySearch es;
	protected Measures measures; 
	protected SemanticContentSearch scs;

	protected OntologyFinder of = new OntologyFinder();
	
	public enum Address {
		KMIaddress("http://watson.kmi.open.ac.uk/watson-ws-v2/services/"),
		INRIAaddress("http://disamis.inrialpes.fr:8080/watson-ws-v2/services/");
		
		private String value;

		Address(String value){
			this.value = value;
		}
		
		public String get(){
			return value;
		}
	}
	
	protected AbstractWatsonService(Address address) throws ServiceException{
		this(address.get());
	}
	
	public AbstractWatsonService(String address) throws ServiceException{
		initEntitySearch(address);
		initMeasures(address);
		initSemanticContentSearch(address);
	}
	
	public void initEntitySearch(String address) throws ServiceException{
		EntitySearchServiceLocator locator = new EntitySearchServiceLocator();		
		locator.setUrnEntitySearchEndpointAddress(address + "urn:EntitySearch");
		es = locator.getUrnEntitySearch();			
	}
	
	public void initMeasures(String address) throws ServiceException{
		MeasuresServiceLocator msl = new MeasuresServiceLocator();
		msl.setUrnMeasuresEndpointAddress(address + "urn:Measures");
		measures = msl.getUrnMeasures();
	}

	public void initSemanticContentSearch(String address) throws ServiceException{
		SemanticContentSearchServiceLocator scslocator = new SemanticContentSearchServiceLocator();
		scslocator.setUrnSemanticContentSearchEndpointAddress(address + "urn:SemanticContentSearch");
		scs = scslocator.getUrnSemanticContentSearch();		
	}
	
	public Measures getMeasures(){
		return measures;
	}

	public EntitySearch getEntitySearch(){
		return es;
	}
	
	public SemanticContentSearch getSemanticContentSearch(){
		return scs;
	}
	
}
