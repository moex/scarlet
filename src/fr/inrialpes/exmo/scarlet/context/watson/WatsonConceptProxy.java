package fr.inrialpes.exmo.scarlet.context.watson;

import java.rmi.RemoteException;


public interface WatsonConceptProxy {

    public java.lang.String[] getSuperClasses() throws java.rmi.RemoteException;
    public java.lang.String[] getSubClasses() throws java.rmi.RemoteException;

    public java.lang.String[] getDisjointWith() throws java.rmi.RemoteException;

    public java.lang.String[] getAllSubClasses() throws java.rmi.RemoteException;
    public java.lang.String[] getAllSuperClasses() throws java.rmi.RemoteException;

    public java.lang.String[] getIsDisjointWith() throws java.rmi.RemoteException;
    public java.lang.String[] getAllDisjointWith() throws java.rmi.RemoteException;
    
    public java.lang.String[] getPropertiesFrom() throws java.rmi.RemoteException;
    public java.lang.String[] getPropertiesTo() throws java.rmi.RemoteException;
    
    public java.lang.String[] getEquivalentClasses() throws java.rmi.RemoteException;
	
    public String[] getLabels() throws RemoteException;
    public String[] getLitterals() throws RemoteException;
    
    public String getLocalName();
    
}
