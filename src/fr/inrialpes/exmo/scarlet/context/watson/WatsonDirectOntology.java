package fr.inrialpes.exmo.scarlet.context.watson;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import uk.ac.open.kmi.scarlet.utils.OntologyFinder;
import uk.ac.open.kmi.scarlet.utils.Utilities;
import uk.ac.open.kmi.watson.clientapi.EntityResult;
import uk.ac.open.kmi.watson.clientapi.EntitySearch;
import uk.ac.open.kmi.watson.clientapi.Measures;
import uk.ac.open.kmi.watson.clientapi.SearchConf;
import uk.ac.open.kmi.watson.clientapi.SemanticContentSearch;
//import fr.inrialpes.exmo.scarlet.concurrent.NotThreadSafe;

//@NotThreadSafe
public class WatsonDirectOntology implements WatsonOntologyProxy {
	private final static OntologyFinder of = new OntologyFinder();
	
	private final static SearchConf confExactMatchOfClassLocalName = new SearchConf();
	private final static SearchConf confTokenMatchOfClassLocalNameAndLabel = new SearchConf();

	static {
	 	confExactMatchOfClassLocalName.setEntities(SearchConf.CLASS);
		confExactMatchOfClassLocalName.setScope(SearchConf.LOCAL_NAME);
		confExactMatchOfClassLocalName.setMatch(SearchConf.EXACT_MATCH);
		
		confTokenMatchOfClassLocalNameAndLabel.setEntities(SearchConf.CLASS);
		confTokenMatchOfClassLocalNameAndLabel.setScope(SearchConf.LOCAL_NAME+SearchConf.LABEL);
		confTokenMatchOfClassLocalNameAndLabel.setMatch(SearchConf.TOKEN_MATCH);
	}
		
	private String onto;
	private EntitySearch es;
	private Measures measures;
	private SemanticContentSearch semanticSearch;

	public WatsonDirectOntology(String onto, WatsonService watson) {
		this.onto = onto;
		this.measures = watson.getMeasures();
		this.es = watson.getEntitySearch();
		this.semanticSearch = watson.getSemanticContentSearch();
	}

	//@Override
	public WatsonConceptProxy getConcept(String concept) {
		return new WatsonDirectConcept(onto, concept, es);
	}

	//@Override
	public Collection<String> getConceptBasedOnKeyword(String term) throws RemoteException {
		return searchConcepts(term, confTokenMatchOfClassLocalNameAndLabel);
	}

	//@Override
	public WatsonPropertyProxy getRelation(String relation) {
		return new WatsonDirectProperty(onto, relation, es);
	}

	//@Override
	public Collection<String> getAllClasses() throws RemoteException {
		String[] listClasses = semanticSearch.listClasses(onto); 
		return Arrays.asList(listClasses);
	}

	//@Override
	public int getSize() {
		try {
			return measures.ontologyNumberOfClasses(onto);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return 0; // not found
	}

	//@Override
	public double getPopularity() {
		try {
			return measures.ontologyDirectPopularity(onto);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return 0.0;
	}

	//@Override
	public Collection<String> getConceptsByExactMatch(String localName) throws RemoteException {
		return searchConcepts(localName, confExactMatchOfClassLocalName);
	}
	
	private Collection<String> searchConcepts(String input, SearchConf conf) throws RemoteException {
		List<String> result = new ArrayList<String>();
		 String label;
		 String anchor;
		 
		 for (EntityResult e : es.getEntitiesByKeyword(onto, input, conf)) {	//for those that match the keyword exactly
			 anchor = e.getURI();
			 label = Utilities.getIDFromURL(anchor);
			 if (Utilities.sameClassBasedOnID(input, label)){
				 result.add(anchor);
			 }
		 }
		 return result;
	}
	
}
