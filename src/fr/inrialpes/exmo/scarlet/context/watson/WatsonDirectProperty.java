package fr.inrialpes.exmo.scarlet.context.watson;

import java.rmi.RemoteException;

import uk.ac.open.kmi.watson.clientapi.EntitySearch;

public class WatsonDirectProperty implements WatsonPropertyProxy {

	private String onto;
	private String property;
	private EntitySearch es;
	
	public WatsonDirectProperty(String onto, String property, EntitySearch es) {
		this.onto = onto;
		this.property = property;
		this.es = es;
	}
	
	//@Override
	public String[] getDomain() throws RemoteException {
		return es.getDomain(onto, property);
	}

	//@Override
	public String[] getRange() throws RemoteException {
		return es.getRange(onto, property);
	}

}
