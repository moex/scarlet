package fr.inrialpes.exmo.scarlet.context.watson;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import uk.ac.open.kmi.watson.clientapi.EntitySearch;
import fr.inrialpes.exmo.scarlet.cache.Computable;
import fr.inrialpes.exmo.scarlet.cache.Memoizer;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;

public class WatsonCachedClass implements WatsonConceptProxy {

	private static EntitySearch es;
	
	private final String concept;
	private String localName = null;
	private String ontology;
	
	private final Memoizer<RelType, String[]> relatedConcepts;
	
	private static enum RelType { // FIXME: all sub classes -> too much in the cache!
		ALL_SUPER_CLASSES(){
			String[] get(String onto, String source) throws RemoteException{
				return es.getAllSuperClasses(onto, source);
			}
		}, SUPER_CLASSES {
			@Override
			String[] get(String onto, String source) throws RemoteException{
				return es.getSuperClasses(onto, source);
			}
		}, ALL_SUB_CLASSES {
			@Override
			String[] get(String onto, String source) throws RemoteException{
				return es.getAllSuperClasses(onto, source);
			}
		}, SUB_CLASSES {
			@Override
			String[] get(String onto, String source) throws RemoteException{
				return es.getSubClasses(onto, source);
			}
		}, DISJOINT_WITH {
			@Override
			String[] get(String onto, String source) throws RemoteException{
				return es.getDisjointWith(onto, source); // FIXME or 	getIsDisjointWith ?
					// getIsDisjointWith = list of classes that are DECLARED to be disjoint 
					// with the given class in the given document
			}
		}, ALL_DISJOINT_WITH {
			@Override
			String[] get(String onto, String source) throws RemoteException {
				return es.getAllDisjointWith(onto, source); 
			}
		}, IS_DISJOINT_WITH {
			@Override
			String[] get(String onto, String source) throws RemoteException {
				return es.getIsDisjointWith(onto, source); 
			}
		}, PROPERTIES_FROM {
			@Override
			String[] get(String onto, String source) throws RemoteException {
				return es.getRangeOf(onto, source); 
			}
		}, PROPERTIES_TO{
			@Override
			String[] get(String onto, String source) throws RemoteException {
				return es.getDomainOf(onto, source); 
			}
		}, EQUIVALENT{
			@Override
			String[] get(String onto, String source) throws RemoteException {
				return es.getEquivalentClasses(onto, source); 
			}
		}, LITTERALS {
			@Override
			String[] get(String onto, String source) throws RemoteException {
				String[][] litterals = es.getLiteralsFor(onto, source);
				String[] result = new String[litterals.length];
				for(int i = 0;i<result.length;i++){
					result[i] = litterals[i][2];
				}
				return result;
			}
		}
		, LABELS{
			@Override
			String[] get(String onto, String source) throws RemoteException {
				return es.getLabels(onto, source);
			}
		};
		
		abstract String[] get(String onto, String source) throws RemoteException;
	};

	public String getURI() {
		return this.concept;
	}
	
	WatsonCachedClass(final String ontology, final String conceptUri){
		this.ontology = ontology;
		this.concept = conceptUri;

		es = WatsonCachedService.getInstance().es;
		
		relatedConcepts = new Memoizer<RelType, String[]> (new Computable<RelType, String[]>(){
			//@Override
			public String[] compute(RelType arg) throws InterruptedException, RemoteException {
				return arg.get(ontology, conceptUri);
			}
		});
	}
	

	//@Override
	public String[] getAllDisjointWith() throws RemoteException {
		try {
			return relatedConcepts.compute(RelType.ALL_DISJOINT_WITH);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String[] getAllSubClasses() throws RemoteException {
		try {
			return relatedConcepts.compute(RelType.ALL_SUB_CLASSES);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String[] getAllSuperClasses() throws RemoteException {
		try {
			return relatedConcepts.compute(RelType.ALL_SUPER_CLASSES);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String[] getDisjointWith() throws RemoteException {
		try {
			return relatedConcepts.compute(RelType.DISJOINT_WITH);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String[] getIsDisjointWith() throws RemoteException {
		try {
			return relatedConcepts.compute(RelType.IS_DISJOINT_WITH);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String[] getSubClasses() throws RemoteException {
		try {
			return relatedConcepts.compute(RelType.SUB_CLASSES);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String[] getSuperClasses() throws RemoteException {
		try {
			return relatedConcepts.compute(RelType.SUPER_CLASSES);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String[] getPropertiesFrom() throws RemoteException {
		try {
			return relatedConcepts.compute(RelType.PROPERTIES_FROM);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String[] getPropertiesTo() throws RemoteException {
		try {
			return relatedConcepts.compute(RelType.PROPERTIES_TO);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String[] getEquivalentClasses() throws RemoteException {
		try {
			return relatedConcepts.compute(RelType.EQUIVALENT);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String[] getLabels() throws RemoteException {
		try {
			return relatedConcepts.compute(RelType.LABELS);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String[] getLitterals() throws RemoteException {
		try {
			
			return relatedConcepts.compute(RelType.LITTERALS);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String getLocalName() {
		if (localName ==null){
			localName = concept.substring(concept.indexOf("#")+1);
		}
		return localName;
	}
	
	private RelType[] fromRelationEnumToRelType(RelationEnum rel){
		switch(rel){
		//case subClass :return new RelType[]{RelType.SUB_CLASSES};
		//case superClass :return new RelType[]{RelType.SUPER_CLASSES};
		case subClass :return new RelType[]{RelType.SUPER_CLASSES};
		case superClass :return new RelType[]{RelType.SUB_CLASSES};
		case disjoint:return new RelType[]{RelType.DISJOINT_WITH};
		case named:return new RelType[]{RelType.PROPERTIES_FROM, RelType.PROPERTIES_TO};
		case equiv:return new RelType[]{RelType.EQUIVALENT};
		case nontransitiveimplication: 
		case overlaps:
		case doNotKnow:
		default:return new RelType[]{}; 
		}
	}
		
	public Collection<String> getCptsRelatedBy(RelationEnum rel) throws RemoteException {
		Collection<String> result = new ArrayList<String>();
		try {
			String[] data = new String[]{};
			for(RelType info: fromRelationEnumToRelType(rel)){
				data = relatedConcepts.compute(info);
			}
			result.addAll(Arrays.asList(data));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return result;
	}
}


