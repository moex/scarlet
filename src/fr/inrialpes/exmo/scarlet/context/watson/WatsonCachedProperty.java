package fr.inrialpes.exmo.scarlet.context.watson;

import java.rmi.RemoteException;

import uk.ac.open.kmi.watson.clientapi.EntitySearch;
import fr.inrialpes.exmo.scarlet.cache.Computable;
import fr.inrialpes.exmo.scarlet.cache.Memoizer;

public class WatsonCachedProperty implements WatsonPropertyProxy {

	static protected EntitySearch es;

	private final WatsonCachedOntology ontology;		
	private final String entity;

	private final Memoizer<RelOp, String[]> relations;
	//private static boolean initialized = false;


	private static enum RelOp { // FIXME: all sub classes -> too much in the cache!
		RANGE(){
			String[] get(String onto, String source) throws RemoteException{
				return es.getRange(onto, source);
			}
		}, DOMAIN {
			@Override
			String[] get(String onto, String source) throws RemoteException{
				return es.getDomainOf(onto, source);
			}
		};

		abstract String[] get(String onto, String source) throws RemoteException;
	};


	public String getURI() {
		return this.entity;
	}

	WatsonCachedProperty(final WatsonCachedOntology ontology, final String uri) {
		this.ontology = ontology;
		this.entity = uri;

		es = WatsonCachedService.getInstance().es;
		
//		if (!initialized){
//			try {		
//				es = WatsonCachedService.getInstance().es;
//				if (es!=null){
//					initialized = true; //to ensure the initialization is appropriate: if an exception occurs
//				}
//			} catch (ServiceException e) { // should not arrive: the first WatsonCachedRelation is created 
//				// after the WatsonCachedOntology -> exception already throwed!
//				initialized = false;
//			}
//		}
		relations = new Memoizer<RelOp, String[]> (new Computable<RelOp, String[]>(){
			//@Override
			public String[] compute(RelOp arg) throws InterruptedException, RemoteException {
				return arg.get(ontology.getURI(), uri);
			}
		});
	}

	//@Override
	public String[] getRange() throws RemoteException {
		try {
			return relations.compute(RelOp.RANGE);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

	//@Override
	public String[] getDomain() throws RemoteException {
		try {
			return relations.compute(RelOp.DOMAIN);
		} catch (InterruptedException e) {
			return new String[0]; 
		}
	}

}
