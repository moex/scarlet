package fr.inrialpes.exmo.scarlet.context.watson;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;

import uk.ac.open.kmi.watson.clientapi.SearchConf;
import uk.ac.open.kmi.watson.clientapi.SemanticContentResult;
import fr.inrialpes.exmo.scarlet.cache.Computable;
import fr.inrialpes.exmo.scarlet.cache.Memoizer;
import fr.inrialpes.exmo.scarlet.param.method.MatchingMethod;
//import fr.inrialpes.exmo.scarlet.concurrent.ThreadSafe;

//@ThreadSafe
public class WatsonCachedService extends AbstractWatsonService implements WatsonService {


	private static Logger log = Logger.getLogger("fr.inrialpes.exmo.scarlet");
	
	private static WatsonCachedService cacheService = null;
	private static boolean initialized = false;
	
	private final ConcurrentMap<String, WatsonCachedOntology> cachedOntologies= new ConcurrentHashMap<String, WatsonCachedOntology>();
	
	private final Memoizer<String[], Collection<String>> ontosThatContainKeyword;
	
	static private final SearchConf defaultConf= new SearchConf();
	
	static{
		defaultConf.setEntities(SearchConf.CLASS);
		defaultConf.setScope(SearchConf.LOCAL_NAME);
		defaultConf.setMatch(SearchConf.EXACT_MATCH);
	}
	
	WatsonCachedService() throws ServiceException { // package visibility in case the exception is raised
		super(Address.KMIaddress);

		ontosThatContainKeyword = new Memoizer<String[], Collection<String>> (new Computable<String[], Collection<String>>(){
			//@Override
			public Collection<String> compute(String[] arg) throws InterruptedException, RemoteException {
				return getOntosForKeywordArray(arg);
			}
		});
	}
	
	// synchronize to avoid getting object partially constructed in case of race. 
	public static synchronized WatsonCachedService getInstance(){
		while (!initialized){
			try {		
				cacheService = new WatsonCachedService(); 
				if (cacheService!=null){
					initialized = true; //to ensure the initialization is appropriate: if an exception occurs
				}
			} catch (ServiceException e) { // should not arrive: the first WatsonCachedRelation is created 
											// after the WatsonCachedOntology -> exception already throwed!
				e.printStackTrace();
				initialized = false;
				Thread.yield();
			}
		} 
		return cacheService;
	}
	
	public WatsonCachedOntology getOntology(String onto){
		if (onto==null){log.info("ontology=null!! WCS 54");}
		if (cachedOntologies.containsKey(onto)){
			return cachedOntologies.get(onto);	
		} else {
			WatsonCachedOntology newlyCreated = new WatsonCachedOntology(onto);
			cachedOntologies.put(onto, newlyCreated); 
			return newlyCreated;
		}
	}
	
	private Collection<String> getOntosForKeywordArray(String[] arg) throws RemoteException{
		Vector<String> result = new Vector<String>();
		for (SemanticContentResult onto : scs.getSemanticContentByKeywords(arg, WatsonCachedService.defaultConf)) {
			String ontoUri = onto.getURI();
			if (ontoUri==null){
				log.info("Could not find an URI for the semantic document " + onto.toString());
			} else {
				result.add(ontoUri);
			}
		}
		return result;
	}
	
	/**
	 * Ontology selection: returns a list of ontologies in which all keywords appear
	 * 
	 * @param source
	 * @param target
	 * @return
	 * @throws RemoteException
	 */
	public Collection<String> getOntosForKeywords(String source, String target) throws RemoteException {
		try {
			return ontosThatContainKeyword.compute(new String[]{source, target});
		} catch (InterruptedException e) {
			return new Vector<String>(); 
		}
	}

	public Collection<String> getOntosForTerm(String term) throws RemoteException{
		try {
			return ontosThatContainKeyword.compute(new String[]{term});
		} catch (InterruptedException e) {
			return new Vector<String>(); 
		}
	}

	
}
