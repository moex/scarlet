package fr.inrialpes.exmo.scarlet.context.watson;

import java.rmi.RemoteException;

import fr.inrialpes.exmo.align.impl.BasicRelation;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.IncompatRelation;
import fr.inrialpes.exmo.align.impl.rel.NonTransitiveImplicationRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumeRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;

import uk.ac.open.kmi.watson.clientapi.EntitySearch;

public class WatsonDirectConcept implements WatsonConceptProxy {

	private EntitySearch es;
	private String onto;
	private String concept;
	private String localName = null;

	public WatsonDirectConcept(String onto, String concept, EntitySearch es) {
		this.onto = onto;
		this.concept = concept;
		this.es = es;
	}
	
	//@Override
	public String[] getAllDisjointWith() throws RemoteException {
		return es.getAllDisjointWith(onto, concept);
	}

	//@Override
	public String[] getAllSubClasses() throws RemoteException {
		return es.getAllSubClasses(onto, concept);
	}

	//@Override
	public String[] getAllSuperClasses() throws RemoteException {
		return es.getAllSuperClasses(onto, concept);
	}

	//@Override
	public String[] getDisjointWith() throws RemoteException {
		return es.getDisjointWith(onto, concept);
	}

	//@Override
	public String[] getIsDisjointWith() throws RemoteException {
		return es.getIsDisjointWith(onto, concept);
	}

	//@Override
	public String[] getPropertiesFrom() throws RemoteException {
		return es.getDomainOf(onto, concept);
	}

	//@Override
	public String[] getPropertiesTo() throws RemoteException {
		return es.getRangeOf(onto, concept);
	}

	//@Override
	public String[] getSubClasses() throws RemoteException {
		return es.getSubClasses(onto, concept);
	}

	//@Override
	public String[] getSuperClasses() throws RemoteException {
		return es.getSuperClasses(onto, concept);
	}

	//@Override
	public String[] getEquivalentClasses() throws RemoteException {
		return es.getEquivalentClasses(onto, concept);
	}
	
	//@Override
	public String[] getLabels() throws RemoteException{
		return es.getLabels(onto, concept);
	}

	//@Override
	public String[] getLitterals() throws RemoteException {
		String[][] litterals = es.getLiteralsFor(onto, concept);
		String[] result = new String[litterals.length];
		for(int i = 0;i<result.length;i++){
			result[i] = litterals[i][2];
		}
		return result;
	}

	//@Override
	public String getLocalName() {
		if (localName==null){
			localName = concept.substring(concept.indexOf("#")+1);
		}
		return localName;
	}
	
}
