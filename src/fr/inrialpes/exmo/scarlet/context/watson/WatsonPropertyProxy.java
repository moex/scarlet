package fr.inrialpes.exmo.scarlet.context.watson;

import java.rmi.RemoteException;

public interface WatsonPropertyProxy {

	String[] getRange() throws RemoteException;

	String[] getDomain() throws RemoteException;

}
