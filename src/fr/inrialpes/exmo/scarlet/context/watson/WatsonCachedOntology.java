package fr.inrialpes.exmo.scarlet.context.watson;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.xml.rpc.ServiceException;

import fr.inrialpes.exmo.scarlet.cache.Computable;
import fr.inrialpes.exmo.scarlet.cache.Memoizer;

public class WatsonCachedOntology implements WatsonOntologyProxy {
	
	private final String ontology;	
	private WatsonDirectOntology insideOntologyProxy; 

	private final ConcurrentMap<String, WatsonCachedClass> cachedConcepts = new ConcurrentHashMap<String, WatsonCachedClass>();
	private final ConcurrentMap<String, WatsonCachedProperty> cachedRelations = new ConcurrentHashMap<String, WatsonCachedProperty>();

	private final Memoizer<String, Collection<String>> anchorsFromKeyword;
	private final Memoizer<String, Collection<String>> anchorsFromExactMatch;
	private Collection<String> allClasses = null;
	
	private boolean initialized = false;
	
	WatsonCachedOntology(String ontology) {
		this.ontology = ontology;
		WatsonService watson;

		while (!initialized){
			try {		
				watson = WatsonDirectService.getInstance();
				if (watson!=null){
					insideOntologyProxy = (WatsonDirectOntology)watson.getOntology(ontology);
					initialized = true; //to ensure the initialization is appropriate: if an exception occurs
				}
			} catch (ServiceException e) { // should not arrive: the first WatsonCachedRelation is created 
											// after the WatsonCachedOntology -> exception already throwed!
				e.printStackTrace();
				initialized = false;
			}
		}

		anchorsFromKeyword = new Memoizer<String, Collection<String>> (new Computable<String, Collection<String>>(){
			//@Override
			public Collection<String> compute(String arg) throws InterruptedException, RemoteException {
				return insideOntologyProxy.getConceptBasedOnKeyword(arg);
			}
		});

		anchorsFromExactMatch = new Memoizer<String, Collection<String>> (new Computable<String, Collection<String>>(){
			//@Override
			public Collection<String> compute(String arg) throws InterruptedException, RemoteException {
				return insideOntologyProxy.getConceptsByExactMatch(arg);
			}
		});
	}
		
	public String getURI() {
		return this.ontology;
	}
	
	//@Override
	public WatsonCachedClass getConcept(String concept){
		if (cachedConcepts.containsKey(concept)){
			return cachedConcepts.get(concept);	
		} else {
			WatsonCachedClass newlyCreated = new WatsonCachedClass(ontology, concept);
			cachedConcepts.put(concept, newlyCreated); 
			return newlyCreated;
		}
	}
	
	//@Override
	public WatsonCachedProperty getRelation(String property){
		if (cachedConcepts.containsKey(property)){
			return cachedRelations.get(property);	
		} else {
			WatsonCachedProperty newlyCreated = new WatsonCachedProperty(this, property);
			cachedRelations.put(property, newlyCreated); 
			return newlyCreated;
		}
	}
	
	//@Override
	 public Collection<String> getConceptBasedOnKeyword (String keyword) throws RemoteException {
		try {
			return anchorsFromKeyword.compute(keyword);
		} catch (InterruptedException e) {
			e.printStackTrace();
			return new Vector<String>(); 
		}
	 }
	
	//@Override
	public Collection<String> getConceptsByExactMatch(String localName) throws RemoteException {
		try {
			return anchorsFromExactMatch.compute(localName);
		} catch (InterruptedException e) {
			e.printStackTrace();
			return new Vector<String>(); 
		}
	}

	//@Override
	public synchronized Collection<String> getAllClasses() throws RemoteException {
		if (allClasses==null){
			allClasses = insideOntologyProxy.getAllClasses();
		}
		return allClasses;
	}

	//@Override
	public int getSize() {
		return insideOntologyProxy.getSize();
	}

	//@Override
	public double getPopularity() {
		return insideOntologyProxy.getPopularity();
	}

}
