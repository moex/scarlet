package fr.inrialpes.exmo.scarlet.cache;

import java.rmi.RemoteException;

import fr.inrialpes.exmo.ontowrap.OntowrapException;

public interface Computable<A, V> {
	
	public V compute(A arg) throws InterruptedException, RemoteException, OntowrapException;
	
}
