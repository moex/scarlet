package fr.inrialpes.exmo.scarlet.cache;

import java.rmi.RemoteException;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import fr.inrialpes.exmo.ontowrap.OntowrapException;
//import com.xoetrope.task.LaunderThrowable; [reproduced in the package]

/**
 * @author Patrick HOFFMANN from Brian Goetz&al, Java Concurrency in practice
 *
 * @param <A> type of the data given as input to the computation
 * @param <V> result type of the computation
 */
public class Memoizer<A, V> implements Computable<A, V> {
	
	private final ConcurrentHashMap<A, Future<V>> cache = new ConcurrentHashMap<A, Future<V>>();
	private final Computable<A, V> computer;
	
	public Memoizer(Computable<A, V> computer){
		this.computer = computer;
	}

	public V compute(final A arg) throws InterruptedException{
		while(true){
			Future<V> future = cache.get(arg);
			
			if (future==null){
				Callable<V> callableComputer = new Callable<V>(){
					//@Override
					public V call() throws InterruptedException, RemoteException, OntowrapException {
						return Memoizer.this.computer.compute(arg);
					}
				};
				FutureTask<V> task = new FutureTask<V>(callableComputer);
				future = cache.putIfAbsent(arg, task);
				
				if (future==null){ // there was indeed no mapping for the key
					future = task; // refers to the new Future which is being computed,
					task.run();
				}
				
				// at this point, future refers to the Future that correspond to the arg value.
			} 

			try {
				return future.get();
			} catch (CancellationException e){
				cache.remove(arg, future);
			} catch (ExecutionException e){
				throw LaunderThrowable.launderThrowable(e.getCause());
			}
		}
	}
}
