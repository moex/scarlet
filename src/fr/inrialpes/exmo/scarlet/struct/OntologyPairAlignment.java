package fr.inrialpes.exmo.scarlet.struct;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;
import org.semanticweb.owl.align.Cell;

import fr.inrialpes.exmo.align.cli.Procalign;
import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.BasicParameters;
import fr.inrialpes.exmo.ontowrap.BasicOntology;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;

public class OntologyPairAlignment {
	
	private String source, target;
	private Set<Mapping> mappings;
	
	public OntologyPairAlignment(String source, String target, String matchingMethod){
		this.source = source;
		this.target = target;
		this.mappings = getMatching(source, target, matchingMethod);
	}
	
	public OntologyPairAlignment(String source, String target, Set<Mapping> alignment){
		this.source = source;
		this.target = target;
		this.mappings = alignment;
	}
	
	public final String getSourceOnto(){
		return source;
	}
	public final String getTargetOnto(){
		return target;
	}

	public final Set<Mapping> getAlignment(){
		return mappings;
	}
	
	@Override
	public boolean equals(Object o){
		if (!(o instanceof OntologyPairAlignment)){
			return false;
		}
		if (this==o){
			return true;
		}
		return (this.source.equals(((OntologyPairAlignment)o).getSourceOnto()) && 
				this.target.equals(((OntologyPairAlignment)o).getTargetOnto())); 
	}
	
	@Override
	public int hashCode() {
		int code = source.hashCode();
		code = 29*code +  target.hashCode();
		
		return code;
	}
	
	@Override
	public String toString(){
		//return "Couple " + coupleID.toString() + ": " + source.toString() + " - " + target.toString();
		return "Couple " + source.toString() + " - " + target.toString();
	}
	
	public Set<Mapping> getMappings(){
		return mappings;
	}
	
	
	public Collection<String> getMappingDomain(){
		Collection<String> domains = null;
		for(Mapping mm : mappings){
			Assertion a = (Assertion)mm;
			domains.add(a.getDomain().toString());
		}
		return domains;
	}
	
	public Collection<String> getMappingRange(Set<Mapping> m){
		Collection<String> ranges = null;
		for(Mapping mm : mappings){
			Assertion a = (Assertion)mm;
			ranges.add(a.getRange().toString());
		}
		return ranges;
	}
	
	public Set<Mapping> getInverseMappings() {
		Set<Mapping> actual = getAlignment();
		Set<Mapping> inverseMappings = new HashSet<Mapping>();
		for(Mapping m : actual){
			Mapping newm = new Mapping(m.getRange(), m.getDomain(), m.getRelation());
			inverseMappings.add(newm);
		}
		return inverseMappings;
	}
	
	public Set<Mapping> getMatching(String source, String target, String alignmentClassName){
		Set<Mapping> result = new HashSet<Mapping>();
		
		URI uri1 = null; 
		URI uri2 = null;
		try {
			uri1 = new URI(source);
			uri2 = new URI(target);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		AlignmentProcess A1 = null;
		try{
		Object[] mparams = {};
		Class<?> alignmentClass = Class.forName(alignmentClassName);
		Class[] cparams = {};
		java.lang.reflect.Constructor alignmentConstructor = alignmentClass.getConstructor(cparams);
		A1 = (AlignmentProcess)alignmentConstructor.newInstance(mparams);
		A1.init( uri1, uri2 );
	    } catch (Exception ex) {
		System.err.println("Cannot create alignment "+alignmentClassName+"\n"
				   +ex.getMessage());
		((Procalign)A1).usage();
		//throw ex;
	    }
	    
		try {			
		 	Properties p = new BasicParameters();
		    A1.align((Alignment)null,p);
		    Alignment result3 = null;
			result3 = (BasicAlignment)A1.clone();
		
			for(Enumeration e = result3.getElements(); e.hasMoreElements(); ){
				Cell c = (Cell)e.nextElement();
				Mapping m = new Mapping(c.getObject1AsURI(result3).toString(), c.getObject2AsURI(result3).toString(), RelationEnum.equiv);
				result.add(m);
			}
		}
		  catch (AlignmentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	

}
