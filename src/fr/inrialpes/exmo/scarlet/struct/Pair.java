package fr.inrialpes.exmo.scarlet.struct;

public class Pair {
	
	private String source, target;
	private String coupleID;
	
	public Pair(String id, String source, String target){
		this.source = source;
		this.target = target;
		this.coupleID = id;
	}
	
	public final String source(){
		return source;
	}
	public final String target(){
		return target;
	}

	public Pair(String source, String target){
		this.source = source;
		this.target = target;
	}
	
	public void setID(String id){
		coupleID = id;
	}
	
	public String getID() {
		return coupleID;
	}
	
	@Override
	public boolean equals(Object o){
		if (!(o instanceof Pair)){
			return false;
		}
		if (this==o){
			return true;
		}
		return (this.source.equals(((Pair)o).source()) && 
				this.target.equals(((Pair)o).target())); 
	}
	
	@Override
	public int hashCode() {
		int code = source.hashCode();
		code = 29*code +  target.hashCode();
		
		return code;
	}
	
	@Override
	public String toString(){
		//return "Couple " + coupleID.toString() + ": " + source.toString() + " - " + target.toString();
		return "Couple " + source.toString() + " - " + target.toString();
	}
	
	public Pair getInverse() {
		return new Pair(target, source);
	}
	

}
