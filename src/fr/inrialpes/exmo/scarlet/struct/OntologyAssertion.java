package fr.inrialpes.exmo.scarlet.struct;

import fr.inrialpes.exmo.scarlet.rel.RelationEnum;



public class OntologyAssertion<X> extends Assertion<X> {
	
	X ontology;
	
	public OntologyAssertion(X ontology, RelationEnum relationType, X domain, X range) {
		super(relationType, domain, range);
		this.ontology = ontology;
	}
	
	public String toString(){
		return ontology.toString() + " : "  + super.toString();
	}

	public X getOntology(){
		return ontology;
	}
}
