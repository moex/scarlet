package fr.inrialpes.exmo.scarlet.struct;

import fr.inrialpes.exmo.scarlet.rel.RelationEnum;


public class Assertion<X> {
	
	private RelationEnum relationType;
	private X domain;
	private X range;
	
	public RelationEnum getRelationType() {
		return relationType;
	}

	public X getDomain() {
		return domain;
	}

	public X getRange() {
		return range;
	}
	
	public Assertion(RelationEnum relationType, X domain, X range){
		this.relationType = relationType;
		this.domain = domain;
		this.range = range;
	}
	
	public RelationEnum getRelation() {
		return relationType;
	}

	public String toString(){
		//return domain.getFragment().toString() + " " + relationType.name() + " " + range.getFragment().toString();
		return domain.toString() + " " + relationType.name() + " " + range.toString();
	}

	public Assertion<X> getInverse() {
		return new Assertion<X>(relationType.inverse(), range, domain);
	}
	
}
