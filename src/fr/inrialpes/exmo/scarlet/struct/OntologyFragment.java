package fr.inrialpes.exmo.scarlet.struct;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import fr.inrialpes.exmo.scarlet.context.watson.WatsonOntologyProxy;

/**
 * @author Patrick HOFFMANN
 * ontology identifier, plus some of it concepts, that have been anchored
 */
public class OntologyFragment<X> implements Iterable<String> {
	
	private X uri;
	private Set<X> concepts;
	private Set<String> conceptsLabels;
	//private WatsonOntologyProxy ontoProxy;
	private boolean restricted = false; 
	
	
	public OntologyFragment(X uri, Collection<X> concepts){
		this.uri = uri;
		this.concepts = new HashSet<X>();
		this.concepts.addAll(concepts);
		restricted = true;
	}
	
	public OntologyFragment(Collection<String> concepts){
		this.conceptsLabels = new HashSet<String>();
		this.conceptsLabels.addAll(concepts);
		restricted = true;
	}
	
	public OntologyFragment(X uri) {
		this.uri = uri;
		this.concepts = new HashSet<X>();
		restricted = false;
	}

	public void remove(String concept){
		concepts.remove(concept);
	}

	public OntologyFragment<String> toStringVersion(){
		Collection<String> stringConcepts = new ArrayList<String>(concepts.size());
		for(X cpt: concepts){
			stringConcepts.add(cpt.toString());
		}
		return new OntologyFragment<String>(uri.toString(), stringConcepts);
	}
	
	public OntologyFragment<String> toLabelVersion(){
		Collection<String> stringConcepts = new ArrayList<String>(concepts.size());
		for(X cpt: concepts){
			stringConcepts.add(cpt.toString().substring(cpt.toString().indexOf('#')+1).toLowerCase());
		}
		return new OntologyFragment<String>(stringConcepts);
	}
	
	public Collection<String> toStringURIVersion(Collection<String> cptLabels){
		Collection<String> stringConcepts = new ArrayList<String>(cptLabels.size());
		for(X cpt: concepts){
			String comp = cpt.toString().substring(cpt.toString().indexOf('#')+1).toLowerCase();
			if(cptLabels.contains(comp)){
				stringConcepts.add(cpt.toString());
			}
		}
		return stringConcepts;
	}
	
	
	public Collection<X> stringToCollectionX(Collection<String> cptLabels){
		Collection<X> stringConcepts = new HashSet<X>();
		for(X cpt : concepts){
			String comp = cpt.toString();
			if(cptLabels.contains(comp)){
				stringConcepts.add(cpt);
			}
		}
		return stringConcepts;
	}
	
	//@Override
	public Iterator<String> iterator() {
		return new Iterator<String>(){

			Iterator<X> iter = concepts.iterator();
			
			//@Override
			public boolean hasNext() {
				return iter.hasNext();
			}

			//@Override
			public String next() {
				return iter.next().toString();
			}

			//@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	public X uri() {
		return uri;
	}

	public void addConcept(X concept) {
		concepts.add(concept);
	}

	public void addAll(Collection<X> anchors) {
		concepts.addAll(anchors);
	}

	public Collection<X> concepts() {
		return concepts;
	}
	
	public Collection<String> conceptsLabels(){
		return conceptsLabels;
	}
	
	public Collection<X> sharedConcepts(OntologyFragment<X> otherOnt){
		Collection<X> result = new HashSet<X>(concepts);
		result.retainAll(otherOnt.concepts());
		return result;
	}
	
	public Collection<String> sharedConceptsLabels(OntologyFragment<String> otherOnt){
		Collection<String> result = new HashSet<String>(conceptsLabels);
		result.retainAll(otherOnt.conceptsLabels());
		return result;
	}
	
	public OntologyFragment<X> minus(Collection<X> sharedConcepts) {
		// works because at initialization, concepts are copied into another collection
		OntologyFragment<X> result = new OntologyFragment<X>(uri, concepts);
		result.concepts.removeAll(sharedConcepts);
		return result;
	}
	
}
