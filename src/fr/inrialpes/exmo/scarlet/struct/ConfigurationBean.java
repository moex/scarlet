package fr.inrialpes.exmo.scarlet.struct;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.inrialpes.exmo.scarlet.param.GlobalPathLengthParam;
import fr.inrialpes.exmo.scarlet.param.GlobalTravMethodParam;
import fr.inrialpes.exmo.scarlet.param.MaxOntologiesParam;
import fr.inrialpes.exmo.scarlet.param.MinAnchorsParam;
import fr.inrialpes.exmo.scarlet.param.OntologySetParam;
import fr.inrialpes.exmo.scarlet.param.LocalPathLengthParam;
import fr.inrialpes.exmo.scarlet.param.PathSelectionParam;
import fr.inrialpes.exmo.scarlet.param.RelationRestrictionParam;
import fr.inrialpes.exmo.scarlet.param.SourceTargetParam;
import fr.inrialpes.exmo.scarlet.param.method.AggregationMethod;
import fr.inrialpes.exmo.scarlet.param.method.CompositionMethod;
import fr.inrialpes.exmo.scarlet.param.method.MatchingMethod;
import fr.inrialpes.exmo.scarlet.param.method.OntologyScaleSelectionMethod;
import fr.inrialpes.exmo.scarlet.param.strategy.ContextMatchingStrategy;

public class ConfigurationBean  {

	private SourceTargetParam sourcetarget;
	private OntologyScaleSelectionMethod poolingSelectionMethod;

	private ContextMatchingStrategy contextMatchingStrategy;
	private MatchingMethod matchingMethod;

	private LocalPathLengthParam lpathlen;
	private GlobalPathLengthParam gpathlen;
	private MinAnchorsParam minanchors;
	private MaxOntologiesParam maxontos;

	private CompositionMethod compositionMethod;

	private AggregationMethod aggregationMethod;
	private OntologySetParam ontologySet;
	private RelationRestrictionParam restriction;
	private PathSelectionParam pathselection;
	private GlobalTravMethodParam gptravmethod;
	
	public ConfigurationBean(){
	}

	/*===SETTERS============ */

	public void setSourceTargetOnto(OntologyFragment<URI> source, OntologyFragment<URI> target){
		sourcetarget = new SourceTargetParam(source, target);
	}
	
	public void setMatchingMethod(Object option) {
		String method = (String)option;
		matchingMethod = new MatchingMethod(method);
	}

	public void setCompositionMethod(CompositionMethod option) {
		compositionMethod = option;
	}

	public void setContextMatchingStrategy(Object option) {
		String ctxt = (String)option;
		contextMatchingStrategy = new ContextMatchingStrategy(ctxt);
	}

	public void setPoolingMethod(OntologyScaleSelectionMethod option) {
		this.poolingSelectionMethod = option;
	}

	public void setAggregationMethod(AggregationMethod option){
		aggregationMethod = option;
	}

	public void setLocalPathLengthParameter(Object option){
		lpathlen = new LocalPathLengthParam(Integer.valueOf((String) option));
	}
	
	public void setGlobalPathLengthParameter(Object option){
		gpathlen = new GlobalPathLengthParam(Integer.valueOf((String) option));
	}
	
	public void setMinimumAnchorsParameter(Object option){
		minanchors = new MinAnchorsParam(Integer.valueOf((String) option));
	}
	
	public void setMaxOntologiesParameter(Object option){
		maxontos = new MaxOntologiesParam(Integer.valueOf((String) option));
	}
	
	public void setRelationRestrictionParameter(Object option){
		restriction = new RelationRestrictionParam((String) option);
	}
	
	public void setPathSelectionParameter(Object option){
		pathselection = new PathSelectionParam((String) option);
	}
	
	public void setGlobalPathTravMethodParameter(Object option){
		gptravmethod = new GlobalTravMethodParam((String) option);
	}
	
	public void setOntologySetParameter(Object option){
		List<String> ontoparam = new ArrayList<String>();
		String ontolist = (String)option;
		String[] optlist = ontolist.split(" ");
		for(int op=0; op < optlist.length; op++){
			ontoparam.add(optlist[op]);
		}
		ontologySet = new OntologySetParam(ontoparam);
	}
	
	
	/*===GETTERS============*/

	public CompositionMethod getCompositionMethod() {
		return compositionMethod;
	}


	public MatchingMethod getMatchingMethod() {
		return matchingMethod;
	}

	public OntologyScaleSelectionMethod getPoolingSelectionMethod() {
		return poolingSelectionMethod;
	}
	
	public AggregationMethod getAggregationMethod() {
		return aggregationMethod;
	}

	public LocalPathLengthParam getLocalPathLengthParameter(){
		return lpathlen;
	}

	public GlobalPathLengthParam getGlobalPathLengthParameter(){
		return gpathlen;
	}
	
	public MinAnchorsParam getMinimumAnchorsParameter(){
		return minanchors;
	}
	
	public MaxOntologiesParam getMaxOntologiesParameter(){
		return maxontos;
	}
	
	public OntologySetParam getOntologySetParameter(){
		return ontologySet;
	}
	
	public RelationRestrictionParam getRelationRestrictionParameter(){
		return restriction;
	}
	
	public PathSelectionParam getPathSelectionParameter(){
		return pathselection;
	}
	
	public String getCtxtMatchingStrategy(){
		return contextMatchingStrategy.getContext();
	}
	
	public String getGlobalPathTravMethod(){
		return gptravmethod.getGPTravMethod();
	}
	
	public List<OntologyFragment<URI>> getSourceTargetOnto(){
		List<OntologyFragment<URI>> ontologies = new ArrayList<OntologyFragment<URI>>();
		ontologies.add(sourcetarget.getSourceOnto());
		ontologies.add(sourcetarget.getTargetOnto());
		return ontologies;
	}
	
}
