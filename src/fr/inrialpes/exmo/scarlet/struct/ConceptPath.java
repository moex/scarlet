package fr.inrialpes.exmo.scarlet.struct;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import uk.ac.open.kmi.scarlet.data.Path;
import fr.inrialpes.exmo.scarlet.matcher.operation.ComposeOperation;
import fr.inrialpes.exmo.scarlet.matcher.task.ExplorationTask;
//import fr.inrialpes.exmo.scarlet.param.method.StatisticalMethod;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;

public class ConceptPath {

	private List<Assertion<String>> assertions;
	private RelationEnum summary;
	private List<RelationEnum> summaryRelations;

	public ConceptPath(List<Assertion<String>> assertions, RelationEnum summary){
		this.assertions = assertions;
		this.summary = summary;
	}

	public ConceptPath(List<Assertion<String>> assertions){
		this.assertions = assertions;
		this.summary = ComposeOperation.compose(assertions);
	}
	/**
	 * translate results into Incompat, Subsumed, Subsume, or more complex (compose)
	 * @param r
	 * @return
	 */
	static public ConceptPath createInstanceFrom(uk.ac.open.kmi.scarlet.data.Relation r) {
		Collection<Path> paths = r.getPathVector();
		List<Assertion<String>> assertions = new ArrayList<Assertion<String>>();
		try {
			for (Path path: paths){
				assertions.addAll(getAssertionsFromAPathInAnOntology(path));
			}
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ConceptPath(assertions, ComposeOperation.compose(assertions));
	}

	public List<Assertion<String>> getAssertions(){
		return assertions;
	}

	public ConceptPath getInverse(){
		List<Assertion<String>> result = new LinkedList<Assertion<String>>();
		for(Assertion<String> a: assertions){ // create same path in reverse order and relations
			Assertion<String> invAssertion = new Assertion<String>(a.getRelation().inverse(), a.getRange(), a.getDomain());
			result.add(0, invAssertion);
		}
		return new ConceptPath(result);
	}


	public static String localName(String cpt){
		int idx = Math.max(cpt.indexOf("#"), cpt.lastIndexOf("/")) + 1;
		return cpt.substring(idx);
	}

	public ConceptPath concatWith(ConceptPath list2) {
		int firstList = 0;
		List<Assertion<String>> result = new LinkedList<Assertion<String>>();
		List<Assertion<String>> defresult = new LinkedList<Assertion<String>>();
		List<Assertion<String>> secondList = new LinkedList<Assertion<String>>();
		for(Assertion cp : list2.getAssertions()){
			secondList.add(cp);
		}
		//if(result.size() > 1 & list2.getAssertions().size() >= 1){
			List<Integer> commonS = new LinkedList<Integer>();
			List<Assertion<String>> invalidAss1 = new LinkedList<Assertion<String>>();
			List<Assertion<String>> invalidAss2 = new LinkedList<Assertion<String>>();
			int count = 0;
			for(Assertion a : assertions){
				//if(0 != assertions.indexOf(a)){
				String domainlabel = localName(a.getDomain().toString()).toLowerCase();
				String rangelabel = localName(a.getRange().toString().toLowerCase());
				for(Assertion b : secondList){
					String domainlabelb = localName(b.getDomain().toString()).toLowerCase();
					String rangelabelb = localName(b.getRange().toString()).toLowerCase();
					if(rangelabel.equals(domainlabelb) && domainlabel.equals(rangelabelb)){
						invalidAss1.add(a);
						invalidAss2.add(b);
					}
				}
				//}
			}
			if(!invalidAss1.isEmpty()){
				for(int ind=0; ind < invalidAss1.size(); ind ++){
					assertions.remove(invalidAss1.get(ind));
				}
			}
			if(!invalidAss2.isEmpty()){
				for(int ind=0; ind < invalidAss2.size(); ind ++){
					secondList.remove(invalidAss2.get(ind));
				}
			}
			if(!assertions.isEmpty()){
				result.addAll(assertions);
				firstList = result.size();
			}
			if(!list2.getAssertions().isEmpty()){
				result.addAll(secondList);
			}

			if(!result.isEmpty()){
				if(result.size() != firstList){
					List<Assertion<String>> joint = new ArrayList<Assertion<String>>();
					joint.add(result.get(firstList-1));
					joint.add(result.get(firstList));
					String adding1 = joint.get(0).getRange();
					String adding2 = joint.get(1).getDomain();
						if(!adding1.equalsIgnoreCase(adding2)){
						Assertion<String> assertion = new Assertion<String>(RelationEnum.equiv, adding1, adding2);
						for(int a = 0; a < firstList; a++){
							defresult.add(result.get(a));
						}
							defresult.add(assertion);
						for(int a = firstList; a < result.size(); a++){
								defresult.add(result.get(a));
						}
						return new ConceptPath(defresult);
					}
				}
				return new ConceptPath(result);
			}
			else{
				return null;
				}

	}

//	public ConceptPath concatWith(ConceptPath list2) {
//		List<Assertion<String>> result = new LinkedList<Assertion<String>>(assertions);
//		result.addAll(list2.getAssertions());
//		return new ConceptPath(result);
//	}


	private static List<Assertion<String>> getAssertionsFromAPathInAnOntology(Path path) throws URISyntaxException{
		List<Assertion<String>> assertions = new ArrayList<Assertion<String>>();

		String ontology = path.getOntology();
		for(uk.ac.open.kmi.scarlet.data.Relation relation: path.getRelationsInPath()){
			assertions.add(new OntologyAssertion<String>(ontology, RelationEnum.toRelation(relation),
						   relation.getDomain(), relation.getRange()));
		}
		return assertions;
	}

	public RelationEnum getSummaryRelation() {
		return this.summary;
	}

	public Pair getCorrespondingPair(){
		String first, last;
		first = assertions.get(0).getDomain();
		last = assertions.get(assertions.size()-1).getRange();
		return new Pair(first, last);
	}

	public Assertion<String> getSummaryAssertion() {
		if ((this.assertions==null)|(this.assertions.isEmpty())){
			return null;
		} else {
			String domainConcept= this.assertions.get(0).getDomain();
			String rangeConcept =  this.assertions.get(assertions.size() - 1).getRange();
			return new Assertion<String>(this.summary, domainConcept, rangeConcept);
		}
	}

	public String toString(){
		StringBuffer printOut = new StringBuffer();
//		printOut.append(ExplorationTask.localName(assertions.get(0).getDomain()));
		printOut.append(assertions.get(0).getDomain());
		for(Assertion<String> assertion: this.assertions){
			printOut.append(" -(" + assertion.getRelation().toBasicRelation().getRelation().toString() + ")-> ");
//			printOut.append(ExplorationTask.localName(assertion.getRange()));
			printOut.append(assertion.getRange());
		}
		//String result = getSummaryAssertion().toString() + ", which summarizes: " + printOut.toString();
		return printOut.toString();
	}

	public Integer attemptsConnectionLength() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setSummaryRelations(List<RelationEnum> relationSuite){
		this.summaryRelations = relationSuite;
	}

	public List<RelationEnum> getSummaryRelations(){
		return this.summaryRelations;
	}

	public int length() {
		return assertions.size();
	}
}
