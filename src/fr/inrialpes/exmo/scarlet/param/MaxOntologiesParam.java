package fr.inrialpes.exmo.scarlet.param;

public class MaxOntologiesParam {

	private int maxonto;
	
	public MaxOntologiesParam(int max){
		this.maxonto = max;
	}
	
	public int getMaxOnto(){
		return maxonto;
	}
	
	public void setMaxOnto(int max){
		this.maxonto = max;
	}
}
