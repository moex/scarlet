package fr.inrialpes.exmo.scarlet.param;

import java.util.Iterator;

public interface Restrictable<E extends Enum<E>> extends Iterable<E> {

	public void removeFromSelection(E el);

	public Iterator<E> iterator();
	
	public boolean isSelected(E el);
	
	public int size();
}
