package fr.inrialpes.exmo.scarlet.param;

import org.semanticweb.owl.align.AlignmentProcess;

import fr.inrialpes.exmo.align.impl.method.ClassStructAlignment;
import fr.inrialpes.exmo.ontosim.vector.CosineVM;
import fr.inrialpes.exmo.ontosim.vector.VectorMeasure;
import fr.inrialpes.exmo.ontosim.vector.model.DocumentCollection;
//import fr.inrialpes.exmo.scarlet.param.evaluation.CompositionEvaluation;
//import fr.inrialpes.exmo.scarlet.param.evaluation.ConceptConnectionConfidenceEvaluation;
//import fr.inrialpes.exmo.scarlet.param.evaluation.CtxtMatchingQualityEvaluation;
//import fr.inrialpes.exmo.scarlet.param.method.StringBasedComparisonMethod;
//import fr.inrialpes.exmo.scarlet.param.method.StatisticalMethod;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;

public enum OptionParameter {
		
	EXPLORATION_MAX_PATHLENGTH(10){} // necessary for the until first success
	,
//	SELECTION_OF_ONTOLOGIES(new String[]{"http://www.cyc.com/2003/04/01/cyc"})
//	,
	USE_GRAPH_TRAVERSING_FOR_UNMATCHED_ANCHORS(true)
	,
	STOP_SEARCH_PATHS_FOR_COUPLE(true)
	,	
	CONCEPT_NEIGHBOOR_DEF(new RelationEnum[]{RelationEnum.subClass, RelationEnum.superClass, 
			RelationEnum.named, RelationEnum.equiv})
	, ONTOLOGY_SMALL_SIZE_THRESHOLD(50)
	, ONTOLOGY_LARGE_SIZE_THRESHOLD(1000)
	, ONTOLOGY_MIN_POPULARITY_THRESHOLD(0.75)
	
	, LABEL_COMPARISON_THRESHOLD(0.75)
	
	, MAX_ANCHORS_BY_CONCEPT(5)
	, EXPLORE_RELATIONTYPE(new RelationEnum[]{RelationEnum.equiv, RelationEnum.subClass, RelationEnum.superClass})
	, EXPLORATION_MAX_PATHWEIGHT(4.0)
	,
	
	COMPOSITION_MAX_LEVEL(4) {}, 
		
	COMPOSITION_MIN_GAINOFINCREASINGLEVEL(0.20) {}, 
	
	CONCEPTCONNECTION_MAX_PATHLENGTH(5) {}, 
	
	//COMPOSITION_LEVELGAIN_EVAL(CompositionEvaluation.BY_NUMBER_OF_RELATIONS_AT_LEVEL_N){}, 
	
	//CONTEXTMATCHING_SATISF_EVAL(CtxtMatchingQualityEvaluation.BY_DENSITY){}, 
	
	CONTEXTMATCHING_SATISF_DEMANDED(0.8){}, 
		
	ONTSELECTION_DISTBYTERMS_VECTORMEASURE(new CosineVM(), VectorMeasure.class){}, 
	
	ONTSELECTION_DISTBYTERMS_DOCUMENTWEIGHT(DocumentCollection.WEIGHT.TF){}, 
	
	ONTDISTANCE_METHOD(OptionParameter.ONTSELECTION_DISTBYTERMS_VECTORMEASURE.getDefaultValue(), VectorMeasure.class){
	//}, ONTSELECTION_DISTBYCPTS_SETMEASURE(new MaxCoupling(new EntityLexicalMeasure()), SetMeasure.class) {
	}, 
	
	//WEIGHTED_SUMMATION_METHOD(StatisticalMethod.MIN){}, 
	
	MEAN_POWER(2){}, 
	
	//LABEL_COMPARISON_METHOD(StringBasedComparisonMethod.STRICT_LABEL_EQUIVALENCE), 
	
	MATCHER(new ClassStructAlignment(),AlignmentProcess.class), 
	
	ANCHORING_ALIGN_SERVER(new ClassStructAlignment(),AlignmentProcess.class) // FIXME
	, 
	
	//CONFIDENCE_EVAL(ConceptConnectionConfidenceEvaluation.ALIGNMENT_STATED_SIM_BASED)
	//, ONTSELECTION_ANCHORINGMETHOD(AnchoringMethod.LABEL_COMPARISON)
	//, 
	
	ONTSELECTION_REQUESTCONSTRUCTION_BY_MERGING_FIRST(false);

	
	private OptionParameterUnion union;
	
	OptionParameter(Object val){
		union = new OptionParameterUnion(val);
	}

	OptionParameter(Object val, Class valInterface){
		union = new OptionParameterUnion(val, valInterface);
	}
		
	public Object getDefaultValue(){
		return union.getDefaultValue();
	}
	
	void testValueType(Object value){
		union.testIfInstance(value);
		return;
	}

	static public Restrictable<OptionParameter> getParameterList(OptionParameter... params){
		return new OptionParameterRestriction(params);
	}
	
	
}