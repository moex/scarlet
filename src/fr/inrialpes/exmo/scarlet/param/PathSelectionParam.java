package fr.inrialpes.exmo.scarlet.param;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PathSelectionParam {

	private String pathselection = null;
	
	public PathSelectionParam(String pathselection){
		this.pathselection = pathselection;
	}
	
	public String getPathSelection(){
		return pathselection;
	}

}
