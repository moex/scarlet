package fr.inrialpes.exmo.scarlet.param;

public class OptionParameterUnion {

	private Object value = null;
	private Class type = null;
	
	public OptionParameterUnion(Object val){
		this.type = val.getClass();		
		this.value = val;
	}
	
	
	public OptionParameterUnion(Object val, Class valInterface) {
		this.type = valInterface;
		testIfInstance(val);
		this.value = val;
	}

	public Object getDefaultValue(){
		return value;
	}
	
	public Class getType(){
		return type;
	}

	public void testIfInstance(Object value){
		if (!this.type.isInstance(value)){
			throw new IllegalArgumentException();
		}
	}
}
