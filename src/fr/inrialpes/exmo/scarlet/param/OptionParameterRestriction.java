package fr.inrialpes.exmo.scarlet.param;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import fr.inrialpes.exmo.scarlet.param.InstanceRestrictable;

public class OptionParameterRestriction implements InstanceRestrictable<OptionParameter> {

	private final List<OptionParameterInstance> selection;
	
	public void removeFromSelection(OptionParameter el) {
		selection.remove(getParameterInstance(el));
	}

	public Iterator<OptionParameter> iterator() {
		return new Iterator<OptionParameter>(){
				final Iterator<OptionParameterInstance> iter = selection.iterator();
			
			//@Override
			public boolean hasNext() {
				return iter.hasNext();
			}

			//@Override
			public OptionParameter next() {
				return iter.next().getOption();
			}

			//@Override
			public void remove() {
				iter.remove(); // throw new UnsupportedOperationException();
			}
		};
	}

	public boolean isSelected(OptionParameter el) {
		return this.containsInstanceFor(el);
	}

	public OptionParameterRestriction(OptionParameter... params){
		selection = new LinkedList<OptionParameterInstance>();
		for(int i=0;i<params.length;i++){
			//System.out.println(params[i]);
			if (!(params[i]==null)){
				selection.add(new OptionParameterInstance(params[i]));
			}
		}	
		//System.out.println("---");
	}
	
	private boolean containsInstanceFor(OptionParameter option){
		for(OptionParameterInstance opi: selection){
			if (opi.getOption().equals(option)){
				return true;
			}
		}
		return false;
	}
	
	private OptionParameterInstance getParameterInstance(OptionParameter option) {
		for(OptionParameterInstance opi: selection){
			if (opi.getOption().equals(option)){
				return opi;
			}
		}
		throw new IllegalArgumentException(); // option not found		
	}

	//@Override
	public Object get(OptionParameter option)throws UnsupportedOperationException {
		return getParameterInstance(option).getValue();
	}

	//@Override
	public void set(OptionParameter option, Object value) throws UnsupportedOperationException {
		getParameterInstance(option).setValue(value);
	}

	//@Override
	public int size() {
		return selection.size();
	}

}
