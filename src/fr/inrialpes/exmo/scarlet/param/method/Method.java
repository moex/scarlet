package fr.inrialpes.exmo.scarlet.param.method;

import fr.inrialpes.exmo.scarlet.param.OptionParameter;
import fr.inrialpes.exmo.scarlet.param.strategy.Feature;

public interface Method extends Feature<OptionParameter> {

}
