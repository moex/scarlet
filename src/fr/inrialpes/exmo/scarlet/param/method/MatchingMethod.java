
package fr.inrialpes.exmo.scarlet.param.method;

import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Evaluator;
import org.semanticweb.owl.align.Parameters;

import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.BasicParameters;
import fr.inrialpes.exmo.align.impl.eval.PRecEvaluator;
import fr.inrialpes.exmo.align.impl.method.EditDistNameAlignment;
import fr.inrialpes.exmo.align.impl.method.NameEqAlignment;
import fr.inrialpes.exmo.align.impl.method.SMOANameAlignment;
import fr.inrialpes.exmo.align.impl.method.StringDistAlignment;
import fr.inrialpes.exmo.align.impl.method.SubsDistNameAlignment;
import fr.inrialpes.exmo.ontowrap.BasicOntology;
import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.inrialpes.exmo.ontowrap.jena25.JENAOntologyFactory;
import fr.inrialpes.exmo.scarlet.context.WatsonContextService;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonCachedService;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonOntologyProxy;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonService;
import fr.inrialpes.exmo.scarlet.matcher.task.MatchingTask;
import fr.inrialpes.exmo.scarlet.param.InstanceRestrictable;
import fr.inrialpes.exmo.scarlet.param.OptionParameter;
import fr.inrialpes.exmo.scarlet.param.OptionParameterRestriction;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.Mapping;
import fr.inrialpes.exmo.scarlet.struct.OntologyFragment;

/**
 * Methods to match two ontologies with one another
 * 
 * @author Patrick HOFFMANN
 * 
 */
public class MatchingMethod {
	/**
	 * Anchors are only the concepts that have actually the same URI in the new
	 * ontology
	 */
private String method = null;
	
	public MatchingMethod(String method){
		this.method = method;
	}
	
	public String getMethod(){
		return method;
	}
	
	public void setMethod(String method){
		this.method = method;
	}

}
