package fr.inrialpes.exmo.scarlet.param.method;

import java.util.LinkedList;
import java.util.List;

import fr.inrialpes.exmo.scarlet.rel.RelationEnum;


public class RelationAlgebra {
	
	public List<RelationEnum> compose(List<RelationEnum> input) {
		List<RelationEnum> oldInput, newInput;
		newInput = composeOnce(input);
		do {
			oldInput = newInput;
			newInput = composeOnce(oldInput);
		} while ((newInput.size()!=oldInput.size())&(!newInput.equals(oldInput)));
		return newInput;
	}
	
	
	private List<RelationEnum> composeOnce(List<RelationEnum> input){
		ComposingQueue queue = new ComposingQueue();
		for(RelationEnum rel: input){
			queue.put(rel);
		}
		return queue;
	}
	
	
	class ComposingQueue extends LinkedList<RelationEnum> implements List<RelationEnum> {

		private void put(RelationEnum last){
			if (this.isEmpty()){
				this.add(last);
			} else {
				RelationEnum lastButOne = this.peek();

				switch(last){

				case equiv:break; 

				case superClass:

					switch(lastButOne){
					case equiv: replaceBy(RelationEnum.superClass); break;
					case superClass: break;
					
					//case subClass: replaceBy(RelationEnum.doNotKnow); break;
					case subClass: replaceBy(RelationEnum.all); break;
					
					//case overlaps: replaceBy(RelationEnum.superClass, RelationEnum.overlaps, RelationEnum.disjoint); break;
					case overlaps: replaceBy(RelationEnum.notSubsumed); break;
					case disjoint:break;
					//from here reversible changes...
					case notIncompatible: replaceBy(RelationEnum.all); break;
					case subsumesOverlaps: replaceBy(RelationEnum.notSubsumed); break;
					case notSubsumed: replaceBy(RelationEnum.notSubsumed); break;
					case notSubsumes: replaceBy(RelationEnum.all); break;
					case subsumedOverlaps: replaceBy(RelationEnum.all); break;
					case all: replaceBy(RelationEnum.all); break;
					
					case doNotKnow:break;
					}
					break;

					
				case subClass:

					switch(lastButOne){
					case equiv: replaceBy(RelationEnum.subClass); break;
					case superClass: replaceBy(RelationEnum.notIncompatible); break;
					case subClass: break;
					case overlaps: replaceBy(RelationEnum.subsumedOverlaps); break;
					case disjoint:replaceBy(RelationEnum.notSubsumes); break;
					//from here reversible changes...
					case notIncompatible: replaceBy(RelationEnum.notIncompatible); break;
					case notSubsumed: replaceBy(RelationEnum.all); break;
					case notSubsumes: replaceBy(RelationEnum.notSubsumes); break;
					case subsumesOverlaps: replaceBy(RelationEnum.notIncompatible); break;
					case subsumedOverlaps: replaceBy(RelationEnum.subsumedOverlaps); break;
					case all: replaceBy(RelationEnum.all); break;
					
					case doNotKnow:break;
					}
					break;

					
				case overlaps:

					switch(lastButOne){
					case equiv: replaceBy(RelationEnum.overlaps); break;
					case superClass: this.add(RelationEnum.subsumesOverlaps); break;
					case subClass:this.add(RelationEnum.notSubsumes); break;
					case overlaps:replaceBy(RelationEnum.all); break;
					case disjoint:replaceBy(RelationEnum.notSubsumes); break;
					//from here reversible changes...
					case notIncompatible: replaceBy(RelationEnum.all); break;
					case notSubsumed: replaceBy(RelationEnum.all); break;
					case notSubsumes: replaceBy(RelationEnum.all); break;
					case subsumesOverlaps: replaceBy(RelationEnum.all); break;
					case subsumedOverlaps: replaceBy(RelationEnum.all); break;
					case all: replaceBy(RelationEnum.all); break;
					
					case doNotKnow:break;
					}
					break;

					
				case disjoint:

					switch(lastButOne){
					case equiv:replaceBy(RelationEnum.disjoint); break;
					case superClass: replaceBy(RelationEnum.notSubsumed); break;
					case subClass:replaceBy(RelationEnum.disjoint); break;
					case overlaps:replaceBy(RelationEnum.notSubsumed); break;
					case disjoint:replaceBy(RelationEnum.all); break;
					//from here reversible changes...
					case notIncompatible: replaceBy(RelationEnum.notSubsumed); break;
					case notSubsumed: replaceBy(RelationEnum.all); break;
					case notSubsumes: replaceBy(RelationEnum.all); break;
					case subsumesOverlaps: replaceBy(RelationEnum.notSubsumed); break;
					case subsumedOverlaps: replaceBy(RelationEnum.notSubsumed); break;
					case all: replaceBy(RelationEnum.all); break;
			
					case doNotKnow:break;
					}
					break;

					
				case notIncompatible:

					switch(lastButOne){
					case equiv: break;
					case superClass: break;
					case subClass:replaceBy(RelationEnum.all); break;
					case overlaps:replaceBy(RelationEnum.all); break;
					case disjoint:replaceBy(RelationEnum.notSubsumes); break;
					//from here reversible changes...
					case notIncompatible: replaceBy(RelationEnum.all); break;
					case notSubsumed: replaceBy(RelationEnum.all); break;
					case notSubsumes: replaceBy(RelationEnum.all); break;
					case subsumesOverlaps: replaceBy(RelationEnum.all); break;
					case subsumedOverlaps: replaceBy(RelationEnum.all); break;
					case all: replaceBy(RelationEnum.all); break;
			
					case doNotKnow:break;
					}
					break;

					
				case subsumesOverlaps:

					switch(lastButOne){
					case equiv: break;
					case superClass: break;
					case subClass:replaceBy(RelationEnum.all); break;
					case overlaps:replaceBy(RelationEnum.all); break;
					case disjoint:replaceBy(RelationEnum.notSubsumes); break;
					//from here reversible changes...
					case notIncompatible: replaceBy(RelationEnum.all); break;
					case notSubsumed: replaceBy(RelationEnum.all); break;
					case notSubsumes: replaceBy(RelationEnum.all); break;
					case subsumesOverlaps: replaceBy(RelationEnum.all); break;
					case subsumedOverlaps: replaceBy(RelationEnum.all); break;
					case all: replaceBy(RelationEnum.all); break;
			
					case doNotKnow:break;
					}
					break;

					
				case subsumedOverlaps:

					switch(lastButOne){
					case equiv: break;
					case superClass: replaceBy(RelationEnum.notIncompatible); break;
					case subClass:replaceBy(RelationEnum.notSubsumes); break;
					case overlaps:replaceBy(RelationEnum.all); break;
					case disjoint:replaceBy(RelationEnum.notSubsumes); break;
					//from here reversible changes...
					case notIncompatible: replaceBy(RelationEnum.all); break;
					case notSubsumed: replaceBy(RelationEnum.all); break;
					case notSubsumes: replaceBy(RelationEnum.all); break;
					case subsumesOverlaps: replaceBy(RelationEnum.all); break;
					case subsumedOverlaps: replaceBy(RelationEnum.all); break;
					case all: replaceBy(RelationEnum.all); break;
			
					case doNotKnow:break;
					}
					break;

					
				case notSubsumed:

					switch(lastButOne){
					case equiv: break;
					case superClass: break;
					case subClass:replaceBy(RelationEnum.all); break;
					case overlaps:replaceBy(RelationEnum.all); break;
					case disjoint:replaceBy(RelationEnum.all); break;
					//from here reversible changes...
					case notIncompatible: replaceBy(RelationEnum.all); break;
					case notSubsumed: replaceBy(RelationEnum.all); break;
					case notSubsumes: replaceBy(RelationEnum.all); break;
					case subsumesOverlaps: replaceBy(RelationEnum.all); break;
					case subsumedOverlaps: replaceBy(RelationEnum.all); break;
					case all: replaceBy(RelationEnum.all); break;
			
					case doNotKnow:break;
					}
					break;

					
				case notSubsumes:

					switch(lastButOne){
					case equiv: break;
					case superClass:replaceBy(RelationEnum.all); break;
					case subClass: break;
					case overlaps:replaceBy(RelationEnum.all); break;
					case disjoint:replaceBy(RelationEnum.all); break;
					//from here reversible changes...
					case notIncompatible: replaceBy(RelationEnum.all); break;
					case notSubsumed: replaceBy(RelationEnum.all); break;
					case notSubsumes: replaceBy(RelationEnum.all); break;
					case subsumesOverlaps: replaceBy(RelationEnum.all); break;
					case subsumedOverlaps: replaceBy(RelationEnum.all); break;
					case all: replaceBy(RelationEnum.all); break;
			
					case doNotKnow:break;
					}
					break;
					
					
				case all:break;

				case doNotKnow:
					this.clear();
					this.add(last);
				}
			}

		}

		private void replaceBy(RelationEnum... newRel){
			this.remove();
			for(RelationEnum rel: newRel){
				this.add(rel);	
			}		
		}

	}


}