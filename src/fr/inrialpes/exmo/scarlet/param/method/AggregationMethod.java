package fr.inrialpes.exmo.scarlet.param.method;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.semanticweb.owl.align.Relation;

import fr.inrialpes.exmo.scarlet.param.InstanceRestrictable;
import fr.inrialpes.exmo.scarlet.param.OptionParameter;
import fr.inrialpes.exmo.scarlet.param.OptionParameterRestriction;
import fr.inrialpes.exmo.scarlet.param.strategy.Feature;
import fr.inrialpes.exmo.scarlet.rel.EquivRelation;
import fr.inrialpes.exmo.scarlet.rel.EveryRelation;
import fr.inrialpes.exmo.scarlet.rel.IncompatRelation;
import fr.inrialpes.exmo.scarlet.rel.NotIncompatibleRelation;
import fr.inrialpes.exmo.scarlet.rel.OverlapRelation;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.rel.NotSubsumedRelation;
import fr.inrialpes.exmo.scarlet.rel.SubsumeOverlapRelation;
import fr.inrialpes.exmo.scarlet.rel.SubsumeRelation;
import fr.inrialpes.exmo.scarlet.rel.SubsumedOverlapRelation;
import fr.inrialpes.exmo.scarlet.rel.SubsumedRelation;
import fr.inrialpes.exmo.scarlet.struct.ConceptPath;
import fr.inrialpes.exmo.scarlet.struct.Mapping;
import fr.inrialpes.exmo.scarlet.struct.Pair;

/**
 * Return a value of truth to mappings obtained from the composition of various ontologies, when there is more than one type of relation
 * returned for mapping between two concepts, especially in case of a conflict (the type of relation that
 * is the intersection of the mappings is Void)
 *
 * @author Patrick HOFFMANN
 *
 */
public enum AggregationMethod implements Method {

	NONE {
		@Override
		public Map<Pair, Collection<ConceptPath>> aggregate(Map<Pair, Collection<ConceptPath>> paths) {
			// TODO Auto-generated method stub
			return paths;
		}
	},

	/**
	 * All mappings are considered to be silmutaneously true, and the resulting mapping is
	 * the conjunction of all mappings (if void, no mapping is returned)
	 *
	 * FIXME is this "disjunctive relation intersection"???
	 */
	LOGICAL_CONJUNCTION {
		@Override
		public Map<Pair, Collection<ConceptPath>> aggregate(Map<Pair, Collection<ConceptPath>> paths) {
			// TODO Auto-generated method stub
			List<Relation> summaryRelations  = new ArrayList<Relation>();
			Map<Pair, Collection<ConceptPath>> pathsConj = new HashMap<Pair, Collection<ConceptPath>>();
			for(Pair couple: paths.keySet()){
				List<RelationEnum> compositionResult = new ArrayList<RelationEnum>();
				for(ConceptPath connection: paths.get(couple)){
					for(RelationEnum sr : connection.getSummaryRelations()){
						compositionResult.add(sr);
					}
				}
				if(compositionResult.size() > 1){
					Map<String, Integer> algebricSigns = new HashMap<String,Integer>();
						for(RelationEnum r : compositionResult){
							Pattern p = Pattern.compile(",");
							String prettyLabel = r.toBasicRelation().getPrettyLabel();
							String[] line = p.split(prettyLabel);
							for(String l : line){
								if(null == algebricSigns.get(l)){
									algebricSigns.put(l, 1);
								}
								else{
									int acc = algebricSigns.get(l);
									acc = acc + 1;
									algebricSigns.put(l, acc);
								}
					       	}
						}
						String composingTheSign = null;
						for(String numRel : algebricSigns.keySet()){
							if(algebricSigns.get(numRel).equals(compositionResult.size())){
								if(null == composingTheSign){
									composingTheSign = numRel;
								}
								else{
									composingTheSign = composingTheSign +"," + numRel;
								}
							}
						}

						if(null != composingTheSign){
							int signNum = -1;
							if(composingTheSign.equals("=")){
								signNum = 0;
							}
							if(composingTheSign.equals("<")){
								signNum = 1;
							}
							if(composingTheSign.equals(">")){
								signNum = 2;
							}
							if(composingTheSign.equals("()")){
								signNum = 3;
							}
							if(composingTheSign.equals("%")){
								signNum = 4;
							}
							if(composingTheSign.contains("<") && composingTheSign.contains("()") && !composingTheSign.contains("=") && !composingTheSign.contains(">") && !composingTheSign.contains("%")){
								signNum = 5;
							}
							if(composingTheSign.contains(">") && composingTheSign.contains("()") && !composingTheSign.contains("=") && !composingTheSign.contains("<") && !composingTheSign.contains("%")){
								signNum = 6;
							}
							if(composingTheSign.contains(">") && composingTheSign.contains("()") && !composingTheSign.contains("=") && !composingTheSign.contains("<") && composingTheSign.contains("%")){
								signNum = 7;
							}
							if(!composingTheSign.contains(">") && composingTheSign.contains("()") && !composingTheSign.contains("=") && composingTheSign.contains("<") && composingTheSign.contains("%")){
								signNum = 8;
							}
							if(composingTheSign.contains(">") && composingTheSign.contains("()") && composingTheSign.contains("=") && composingTheSign.contains("<") && !composingTheSign.contains("%")){
								signNum = 9;
							}
							if(composingTheSign.contains(">") && composingTheSign.contains("()") && composingTheSign.contains("=") && composingTheSign.contains("<") && composingTheSign.contains("%")){
								signNum = 10;
							}
							compositionResult.clear();
							switch (signNum){
								case 0: compositionResult.add(RelationEnum.equiv); break;
								case 1: compositionResult.add(RelationEnum.subClass); break;
								case 2: compositionResult.add(RelationEnum.superClass); break;
								case 3: compositionResult.add(RelationEnum.overlaps); break;
								case 4: compositionResult.add(RelationEnum.disjoint); break;
								case 5: compositionResult.add(RelationEnum.subsumedOverlaps); break;
								case 6: compositionResult.add(RelationEnum.subsumesOverlaps); break;
								case 7: compositionResult.add(RelationEnum.notSubsumed); break;
								case 8: compositionResult.add(RelationEnum.notSubsumes); break;
								case 9: compositionResult.add(RelationEnum.notIncompatible); break;
								case 10: compositionResult.add(RelationEnum.all); break;
							}
							Set<ConceptPath> cps = new HashSet<ConceptPath>();
							for(ConceptPath connection: paths.get(couple)){
								connection.setSummaryRelations(compositionResult);
								cps.add(connection);
							}
							pathsConj.put(couple, cps);
						}// end if composing the sign is not null
						//else{
							//paths.remove(couple);
						//}
					}// end if more than one composition results
					else{ //if compositionresult == 1
						Set<ConceptPath> cps = new HashSet<ConceptPath>();
						for(ConceptPath connection: paths.get(couple)){
							connection.setSummaryRelations(compositionResult);
							cps.add(connection);
						}
						pathsConj.put(couple, cps);
					} // end else only one concept path
				}// end cycling on couple
			return pathsConj;
			}
	},
	/**
	 * The relation type that is returned is true if any of the relations
	 * given as input is true. In the case the mapping states nothing (A <= B Union A <= -B), no mapping
	 * is returned.
	 */
	LOGICAL_DISJUNCTION {
		@Override
		public Map<Pair, Collection<ConceptPath>> aggregate(Map<Pair, Collection<ConceptPath>> paths) {
			// TODO Auto-generated method stub
			Map<Pair, Collection<ConceptPath>> pathsDisj = new HashMap<Pair, Collection<ConceptPath>>();
			List<Relation> summaryRelations  = new ArrayList<Relation>();
			for(Pair couple: paths.keySet()){
				List<RelationEnum> compositionResult = new ArrayList<RelationEnum>();
				for(ConceptPath connection: paths.get(couple)){
					for(RelationEnum sr : connection.getSummaryRelations()){
						compositionResult.add(sr);
					}
				}
				if(compositionResult.size() > 1){
					Set<String> algebricSigns = new HashSet<String>();
						for(RelationEnum r : compositionResult){
							Pattern p = Pattern.compile(",");
							String prettyLabel = r.toBasicRelation().getPrettyLabel();
							String[] line = p.split(prettyLabel);
					       	for(String l : line){
					       		algebricSigns.add(l);
					       	}
						}
						String composingTheSign = null;
						for(String alg : algebricSigns){
							if(null == composingTheSign){
								composingTheSign = alg;
							}
							else if(composingTheSign.contains(alg)){
								// do nothing
							}
							else{
								composingTheSign = composingTheSign +"," + alg;
							}
						}

						int signNum = -1;
						if(composingTheSign.equals("=")){
							signNum = 0;
						}
						if(composingTheSign.equals("<")){
							signNum = 1;
						}
						if(composingTheSign.equals(">")){
							signNum = 2;
						}
						if(composingTheSign.equals("()")){
							signNum = 3;
						}
						if(composingTheSign.equals("%")){
							signNum = 4;
						}
						if(composingTheSign.contains("<") && composingTheSign.contains("()") && !composingTheSign.contains("=") && !composingTheSign.contains(">") && !composingTheSign.contains("%")){
							signNum = 5;
						}
						if(composingTheSign.contains(">") && composingTheSign.contains("()") && !composingTheSign.contains("=") && !composingTheSign.contains("<") && !composingTheSign.contains("%")){
							signNum = 6;
						}
						if(composingTheSign.contains(">") && composingTheSign.contains("()") && !composingTheSign.contains("=") && !composingTheSign.contains("<") && composingTheSign.contains("%")){
							signNum = 7;
						}
						if(!composingTheSign.contains(">") && composingTheSign.contains("()") && !composingTheSign.contains("=") && composingTheSign.contains("<") && composingTheSign.contains("%")){
							signNum = 8;
						}
						if(composingTheSign.contains(">") && composingTheSign.contains("()") && composingTheSign.contains("=") && composingTheSign.contains("<") && !composingTheSign.contains("%")){
							signNum = 9;
						}
						if(composingTheSign.contains(">") && composingTheSign.contains("()") && composingTheSign.contains("=") && composingTheSign.contains("<") && composingTheSign.contains("%")){
							signNum = 10;
						}
						if(signNum == -1){
							signNum = 11;
						}
						compositionResult.clear();
						switch (signNum){
							case 0: compositionResult.add(RelationEnum.equiv); break;
							case 1: compositionResult.add(RelationEnum.subClass); break;
							case 2: compositionResult.add(RelationEnum.superClass); break;
							case 3: compositionResult.add(RelationEnum.overlaps); break;
							case 4: compositionResult.add(RelationEnum.disjoint); break;
							case 5: compositionResult.add(RelationEnum.subsumedOverlaps); break;
							case 6: compositionResult.add(RelationEnum.subsumesOverlaps); break;
							case 7: compositionResult.add(RelationEnum.notSubsumed); break;
							case 8: compositionResult.add(RelationEnum.notSubsumes); break;
							case 9: compositionResult.add(RelationEnum.notIncompatible); break;
							case 10: compositionResult.add(RelationEnum.all); break;
							case 11: compositionResult.add(RelationEnum.disjunctive); break;
						}
						Set<ConceptPath> cps = new HashSet<ConceptPath>();;
						for(ConceptPath connection: paths.get(couple)){
							connection.setSummaryRelations(compositionResult);
							cps.add(connection);
						}
						pathsDisj.put(couple, cps);
					}// end if more than one composition results
					else{
						Set<ConceptPath> cps = new HashSet<ConceptPath>();;
						for(ConceptPath connection: paths.get(couple)){
							connection.setSummaryRelations(compositionResult);
							cps.add(connection);
						}
						pathsDisj.put(couple, cps);
					} // end if there is only one concept path
				}// end cycling on couple
			return pathsDisj;
			}
	}
	,
	/**
	 * The normal behavior is intersection (believe mappings are all trustworthy).
	 * When a conflict occurs (resulting mapping = void), one will remove the less trustworthy mapping,
	 * and redo it; this can be done a few times until the conflict is solved.
	 *
	 *  NB: this is also the option to choose if one is interested to take into account the
	 *  similarity value stated by the mappings that served to find the final correspondence between
	 *  ontology concepts.
	 */
//	CONFIDENCE_BASED(OptionParameter.CONFIDENCE_EVAL) {
//		@Override
//		public Map<Pair, Collection<ConceptPath>> aggregate(Map<Pair, Collection<ConceptPath>> paths) {
//			// TODO Auto-generated method stub
//			return null;
//		}
//	},
	/**
	 * When a mapping is more present than the others, it is the one that is kept
	 * (composed mappings such as equivalence are considered both as subsumption and inverse subsumption)
	 * If mappings are in equal number, we try to do intersection (even if the result is a mapping void).
	 */
	POPULARITY_BASED {
		@Override
		public Map<Pair, Collection<ConceptPath>> aggregate(Map<Pair, Collection<ConceptPath>> paths) {
			// TODO Auto-generated method stub
			Map<Pair, Collection<ConceptPath>> pathsPop = new HashMap<Pair, Collection<ConceptPath>>();
			List<Relation> summaryRelations  = new ArrayList<Relation>();
			for(Pair couple: paths.keySet()){
				List<RelationEnum> compositionResult = new ArrayList<RelationEnum>();
				for(ConceptPath connection: paths.get(couple)){
					for(RelationEnum sr : connection.getSummaryRelations()){
						compositionResult.add(sr);
					}
				}
				if(compositionResult.size() > 1){
					Map<RelationEnum, Integer> algebricSigns = new HashMap<RelationEnum, Integer>();
						for(RelationEnum r : compositionResult){
							if(null == algebricSigns.get(r)){
								algebricSigns.put(r, 1);
							}
							else{
								int acc = algebricSigns.get(r);
								acc = acc + 1;
								algebricSigns.put(r, acc);
							}
						}
						int max = 0;
						for(RelationEnum r : algebricSigns.keySet()){
							if(max < algebricSigns.get(r)){
								max = algebricSigns.get(r);
							}
						}
						compositionResult.clear();
						for(RelationEnum r : algebricSigns.keySet()){
							if(algebricSigns.get(r).equals(max)){
								compositionResult.add(r);
							}
						}
						Set<ConceptPath> cps = new HashSet<ConceptPath>();
						int numcompres = 0;
						for(ConceptPath connection: paths.get(couple)){
							    if(numcompres < compositionResult.size()){
						    	connection.setSummaryRelations(compositionResult);
						    	numcompres++;
								cps.add(connection);
						    }
						}
						pathsPop.put(couple, cps);
					}// end if more than one composition results
				else{//only one relation, put this
					Set<ConceptPath> cps = new HashSet<ConceptPath>();
					for(ConceptPath connection: paths.get(couple)){
					connection.setSummaryRelations(compositionResult);
					cps.add(connection);
					pathsPop.put(couple, cps);
				}
				}
			}// end cycling on couple
			return pathsPop;
			}
	};


	private InstanceRestrictable<OptionParameter> parameters;

	private AggregationMethod(OptionParameter...params){
		parameters = new OptionParameterRestriction(params);
	}

	public abstract Map<Pair, Collection<ConceptPath>> aggregate(Map<Pair, Collection<ConceptPath>> paths);

	public Object get(OptionParameter option) throws UnsupportedOperationException {
		return parameters.get(option);
	}

	public void set(OptionParameter option, Object value) throws UnsupportedOperationException {
		parameters.set(option, value);
	}
}