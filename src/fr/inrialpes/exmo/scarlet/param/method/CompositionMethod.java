package fr.inrialpes.exmo.scarlet.param.method;

import java.util.ArrayList;
import java.util.List;

import fr.inrialpes.exmo.scarlet.param.InstanceRestrictable;
import fr.inrialpes.exmo.scarlet.param.OptionParameter;
import fr.inrialpes.exmo.scarlet.param.OptionParameterRestriction;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.Assertion;
import fr.inrialpes.exmo.scarlet.struct.ConceptPath;

/**
 * Compose relations with one another (or not, depending on their type)
 * 
 * 
 * Cf. <a href="https://exmo.inrialpes.fr/exmotto/2009/01/13/composing-alignments/">Exmotto: Composing alignments</a>1
 *  
 *  By contrast to the description on Exmotto, all methods described here are "ontology-based", or rather may be,
 *  depending on the choice to compose at a given level N. If N is more than one, the composition method will be 
 *  ontology-based.
 *   
 * @author Patrick HOFFMANN
 *
 */
public enum CompositionMethod  implements Method {

	/**
	 * Compose using an algebra of relations
	 */
	RELATIONAL {
		RelationAlgebra algebra = new RelationAlgebra();
		
		@Override
		public List<RelationEnum> compose(ConceptPath path) {
			List<Assertion<String>> assertions = path.getAssertions();
			List<RelationEnum> relations = new ArrayList<RelationEnum>(assertions.size());
			for(Assertion<String> assertion: assertions){
				relations.add(assertion.getRelation());
			}
			return algebra.compose(relations);
		}

		@Override
		public List<RelationEnum> compose(List<Assertion<String>> path) {
			List<RelationEnum> relations = new ArrayList<RelationEnum>(path.size());
			for(Assertion<String> assertion: path){
				relations.add(assertion.getRelation());
			}
			return algebra.compose(relations);
		}
	};

	private InstanceRestrictable<OptionParameter> parameters;
	
	private CompositionMethod(OptionParameter...params){
		parameters = new OptionParameterRestriction(params);
	}
	
	public abstract List<RelationEnum> compose(ConceptPath path);

	public Object get(OptionParameter option) throws UnsupportedOperationException {
		return parameters.get(option);
	}

	public void set(OptionParameter option, Object value) throws UnsupportedOperationException {
		parameters.set(option, value);
	}

	public abstract List<RelationEnum> compose(List<Assertion<String>> path);
	
}
