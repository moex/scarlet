package fr.inrialpes.exmo.scarlet.param.method;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.inrialpes.exmo.scarlet.matcher.task.SelectionTask;
import fr.inrialpes.exmo.scarlet.param.InstanceRestrictable;
import fr.inrialpes.exmo.scarlet.param.OptionParameter;
import fr.inrialpes.exmo.scarlet.param.OptionParameterRestriction;
import fr.inrialpes.exmo.scarlet.struct.OntologyFragment;

/**
 * Various methods to select the ontologies that will serve as intermediary ontologies
 * 
 * @author Patrick HOFFMANN
 *
 */
public enum OntologyScaleSelectionMethod implements Method {
	/**
	 * Select for the pool all ontologies where there is at least an anchor for one concept of any of the two ontologies to match
	 */
	ALL(){
		//@Override
		public Set<String> selectOntologies(SelectionTask selector, OntologyFragment<String> onto) {
			//return selector.getOntologiesForAllConcepts(onto.concepts(), ontologyTypeRestriction.filterCriterion(selector));
			Collection<String> Onto1WithMinAnchores = selector.getOntoAnchoresOnto1().keySet();
			Collection<String> Onto2WithMinAnchores = selector.getOntoAnchoresOnto2().keySet();
			Set<String> result = new HashSet<String>();
			Set<String> commonOnto = new HashSet<String>();
			commonOnto.addAll(Onto1WithMinAnchores);
			commonOnto.addAll(Onto2WithMinAnchores);
			for(String on : commonOnto){
				if(null != selector.getOntoAnchoresOnto1().get(on)){
						//System.out.println("Ontology " + on + "  anchors " + selector.getOntoAnchoresOnto1().get(on));
				 	}else{
				 		//System.out.println("Ontology " + on + "  anchors " + selector.getOntoAnchoresOnto2().get(on));
				 	}
				}
			
			 if(selector.getConfigurationBean().getMaxOntologiesParameter().getMaxOnto() != 0){
				 Collection<String> acc = new HashSet<String>();
				 for(String on1 : commonOnto){
					 acc.add(on1);
				 }
				 return result = selector.getFirstRandomOntologies(acc, selector.getConfigurationBean().getMaxOntologiesParameter().getMaxOnto());
			 } else
			    return commonOnto;
		}
	},	
	
	MIN_TOTAL_ANCHORS_BASED(){
		public Set<String> selectOntologies(SelectionTask selector, OntologyFragment<String> onto){
			
			//System.out.println("MIN ANCHORES: " + selector.getConfigurationBean().getMinimumAnchorsParameter().getMinAnch());
			//System.out.println("MAX ONTOLOGIES SELECTED: " +  selector.getConfigurationBean().getMaxOntologiesParameter().getMaxOnto());
			
			Map<String, Integer> ontosAnchored = new HashMap<String, Integer>();
			Set<String> result = new HashSet<String>();
			Collection<String> Onto1WithMinAnchores = selector.getOntoAnchoresOnto1().keySet();
			Collection<String> Onto2WithMinAnchores = selector.getOntoAnchoresOnto2().keySet();
		 	Collection<String> commonAnchores = Onto1WithMinAnchores;
		 	commonAnchores.retainAll(Onto2WithMinAnchores);
		 	for(String ontouri : commonAnchores){
		 		ontosAnchored.put(ontouri, selector.getOntoAnchoresOnto1().get(ontouri) + selector.getOntoAnchoresOnto1().get(ontouri));
		 	}
		 	for(String ontouri : Onto1WithMinAnchores){
		 		if(!ontosAnchored.containsKey(ontouri)){
		 			ontosAnchored.put(ontouri, selector.getOntoAnchoresOnto1().get(ontouri));
		 		}
		 	}
		 	for(String ontouri : Onto2WithMinAnchores){
		 		if(!ontosAnchored.containsKey(ontouri)){
		 			ontosAnchored.put(ontouri, selector.getOntoAnchoresOnto2().get(ontouri));
		 		}
		 	}
		 	
		 	for(String on : ontosAnchored.keySet()){
		 		//System.out.println("Ontology " + on + "  anchors " + ontosAnchored.get(on));
		 	}
		 	
		 	Collection<String> totAnchorsCut = selector.filterByMinNb(ontosAnchored, selector.getConfigurationBean().getMinimumAnchorsParameter().getMinAnch());
			if(selector.getConfigurationBean().getMaxOntologiesParameter().getMaxOnto() > 0){
				result = selector.getFirstOntologies(ontosAnchored, totAnchorsCut, selector.getConfigurationBean().getMaxOntologiesParameter().getMaxOnto());
			}
			else{
				result.addAll(totAnchorsCut);
			}
			for(String ontoS : result){
				//System.out.println("Selected Ontology " + ontoS + " with number of anchors " + ontosAnchored.get(ontoS));
			}
		 	return result;
		}
	},
	
	
	MIN_ANCHORS_BASED(){
		public Set<String> selectOntologies(SelectionTask selector, OntologyFragment<String> onto){
			
			//System.out.println("MIN ANCHORES: " + selector.getConfigurationBean().getMinimumAnchorsParameter().getMinAnch());
			//System.out.println("MAX ONTOLOGIES SELECTED: " +  selector.getConfigurationBean().getMaxOntologiesParameter().getMaxOnto());
						
			Map<String, Integer> ontosAnchored = new HashMap<String, Integer>();
			Set<String> result = new HashSet<String>();
			Collection<String> Onto1WithMinAnchores = selector.filterByMinNb(selector.getOntoAnchoresOnto1(), selector.getConfigurationBean().getMinimumAnchorsParameter().getMinAnch());
		 	Collection<String> Onto2WithMinAnchores = selector.filterByMinNb(selector.getOntoAnchoresOnto2(), selector.getConfigurationBean().getMinimumAnchorsParameter().getMinAnch());
		 	Collection<String> commonAnchores = Onto1WithMinAnchores;
		 	commonAnchores.retainAll(Onto2WithMinAnchores);
		 	for(String ontouri : commonAnchores){
		 		ontosAnchored.put(ontouri, selector.getOntoAnchoresOnto1().get(ontouri) + selector.getOntoAnchoresOnto1().get(ontouri));
		 	}
		 	for(String ontouri : Onto1WithMinAnchores){
		 		if(ontosAnchored.containsKey(ontouri)){
		 			ontosAnchored.put(ontouri, selector.getOntoAnchoresOnto1().get(ontouri));
		 		}
		 	}
		 	for(String ontouri : Onto2WithMinAnchores){
		 		if(ontosAnchored.containsKey(ontouri)){
		 			ontosAnchored.put(ontouri, selector.getOntoAnchoresOnto2().get(ontouri));
		 		}
		 	}
		 	
		 	for(String on : ontosAnchored.keySet()){
		 		//System.out.println("Ontology " + on + "  anchors " + ontosAnchored.get(on));
		 	}
		 	
			if(selector.getConfigurationBean().getMaxOntologiesParameter().getMaxOnto() > 0){
				result = selector.getFirstOntologies(ontosAnchored, ontosAnchored.keySet(), selector.getConfigurationBean().getMaxOntologiesParameter().getMaxOnto());
			}
		 	else{
		 		result = ontosAnchored.keySet();	
		 	}
			for(String ontoS : result){
				//System.out.println("Selected Ontology " + ontoS + " with number of anchors " + ontosAnchored.get(ontoS));
			}
		 	return result;
		}
	};	

	private InstanceRestrictable<OptionParameter> parameters;
	
	private OntologyScaleSelectionMethod(OptionParameter...params){
		parameters = new OptionParameterRestriction(params);
	}
	
	public abstract Set<String> selectOntologies(SelectionTask selector, OntologyFragment<String> onto);

	public Object get(OptionParameter option) throws UnsupportedOperationException {
		return parameters.get(option);
	}

	public void set(OptionParameter option, Object value) throws UnsupportedOperationException {
		parameters.set(option, value);
	}
}


