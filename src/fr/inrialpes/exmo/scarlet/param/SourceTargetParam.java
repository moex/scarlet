package fr.inrialpes.exmo.scarlet.param;

import java.net.URI;

import fr.inrialpes.exmo.scarlet.struct.OntologyFragment;

public class SourceTargetParam {

private OntologyFragment<URI>  onto1;
private OntologyFragment<URI>  onto2;
	
	public SourceTargetParam(OntologyFragment<URI>  onto1, OntologyFragment<URI>  onto2){
		this.onto1 = onto1;
		this.onto2 = onto2;
	}
	
	public OntologyFragment<URI>  getSourceOnto(){
		return onto1;
	}
	
	public OntologyFragment<URI>  getTargetOnto(){
		return onto2;
	}
		
}
