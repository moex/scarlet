package fr.inrialpes.exmo.scarlet.param;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OntologySetParam {

	private List<String> ontologies = new ArrayList<String>();
	
	public OntologySetParam(List<String> ontoparam){
		this.ontologies = ontoparam;
	}
	
	public List<String> getOntoSet(){
		return ontologies;
	}
	
	public void setOntoSet(String onto){
		this.ontologies.add(onto);
	}
}
