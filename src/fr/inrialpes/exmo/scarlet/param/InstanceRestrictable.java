package fr.inrialpes.exmo.scarlet.param;

import fr.inrialpes.exmo.scarlet.param.strategy.Feature;

public interface InstanceRestrictable<E extends Enum<E>> extends Restrictable<E>, Feature<E> {

}
