package fr.inrialpes.exmo.scarlet.param.strategy;


import java.util.ArrayList;
import java.util.List;

import fr.inrialpes.exmo.scarlet.param.InstanceRestrictable;
import fr.inrialpes.exmo.scarlet.param.OptionParameter;
import fr.inrialpes.exmo.scarlet.param.OptionParameterRestriction;
//import fr.inrialpes.exmo.scarlet.param.evaluation.CtxtMatchingQualityEvaluation;
//import fr.inrialpes.exmo.scarlet.param.evaluation.Evaluation;

/**
 * Decide under which conditions to stop the context-based matching before the end
 * 
 * @author Patrick HOFFMANN
 *
 */
public class ContextMatchingStrategy {
	/**
	 * Match until all known possibilities are examined to the end.
	 */

	private String context = null;
	
	public ContextMatchingStrategy(String ctxt){
		this.context = ctxt;
	}
	
	public String getContext(){
		return context;
	}
	
	public void setContext(String ctxt){
		this.context = ctxt;
	}
	

}
