package fr.inrialpes.exmo.scarlet.param.strategy;


public interface Feature<E> {

	public void set(E param, Object value) throws UnsupportedOperationException;
	
	public Object get(E param) throws UnsupportedOperationException;
	
}
