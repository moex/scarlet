package fr.inrialpes.exmo.scarlet.param;

public class GlobalPathLengthParam {

	private int length;
	
	public GlobalPathLengthParam(int len){
		this.length = len;
	}
	
	public int getLength(){
		return length;
	}
	
	public void setLength(int len){
		this.length = len;
	}
}
