package fr.inrialpes.exmo.scarlet.param;


public class OptionParameterInstance {

	OptionParameter option;
	Object value = null;

	OptionParameterInstance(OptionParameter option){
		this.option = option;
		this.value = option.getDefaultValue();
	}
	
	public OptionParameter getOption(){
		return option;
	}
	
	public Object getValue(){
		return value;
	}
	
	public void setValue(Object val){
		option.testValueType(val);
		value = val;
	}
		
	
}
