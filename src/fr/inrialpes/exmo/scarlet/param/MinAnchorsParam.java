package fr.inrialpes.exmo.scarlet.param;

public class MinAnchorsParam {

	private int anchors;
	
	public MinAnchorsParam(int anch){
		this.anchors = anch;
	}
	
	public int getMinAnch(){
		return anchors;
	}
	
	public void setMinAnch(int anch){
		this.anchors = anch;
	}
}
