package fr.inrialpes.exmo.scarlet.param;

public class LocalPathLengthParam {

	private int length;
	
	public LocalPathLengthParam(int len){
		this.length = len;
	}
	
	public int getLength(){
		return length;
	}
	
	public void setLength(int len){
		this.length = len;
	}
}
