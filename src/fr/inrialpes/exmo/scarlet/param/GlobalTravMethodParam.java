package fr.inrialpes.exmo.scarlet.param;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GlobalTravMethodParam {

	private String method = null;
	
	public GlobalTravMethodParam(String method){
		this.method = method;
	}
	
	public String getGPTravMethod(){
		return method;
	}

}
