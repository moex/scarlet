package fr.inrialpes.exmo.scarlet.param;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RelationRestrictionParam {

	private String restriction = null;
	
	public RelationRestrictionParam(String restriction){
		this.restriction = restriction;
	}
	
	public String getRelRestriction(){
		return restriction;
	}

}
