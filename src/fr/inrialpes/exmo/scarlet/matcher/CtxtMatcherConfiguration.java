package fr.inrialpes.exmo.scarlet.matcher;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Properties;

//import fr.inrialpes.exmo.scarlet.concurrent.Immutable;
import fr.inrialpes.exmo.scarlet.matcher.conf.AbstractConfigurable;
import fr.inrialpes.exmo.scarlet.matcher.conf.Configurable;
import fr.inrialpes.exmo.scarlet.matcher.conf.Parameter;
import fr.inrialpes.exmo.scarlet.matcher.conf.ParameterCombination;
import fr.inrialpes.exmo.scarlet.param.OptionParameter;
import fr.inrialpes.exmo.scarlet.param.OptionParameterUnion;
import fr.inrialpes.exmo.scarlet.param.LocalPathLengthParam;
import fr.inrialpes.exmo.scarlet.param.method.AggregationMethod;
import fr.inrialpes.exmo.scarlet.param.method.CompositionMethod;
import fr.inrialpes.exmo.scarlet.param.method.MatchingMethod;
import fr.inrialpes.exmo.scarlet.param.method.OntologyScaleSelectionMethod;
import fr.inrialpes.exmo.scarlet.param.strategy.ContextMatchingStrategy;
import fr.inrialpes.exmo.scarlet.struct.ConfigurationBean;


//@Immutable
public final class CtxtMatcherConfiguration extends AbstractConfigurable implements Configurable {

	private static final String name = "Scarlet2";
	private static final String description = "Context-based matcher (C) OU, INRIA 2010";

	private static String defaultzero = "0";

	
//setting the Context Service parameter ... ???	
	
	static private final Parameter setAnchoringMethod= new Parameter("AnchoringMethod", 
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setMatchingMethod(option);}},
			defaultzero,
			defaultzero);
	
	static private final Parameter setCompositionMethod= new Parameter("CompositionMethod", 
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setCompositionMethod((CompositionMethod)option);}},
			CompositionMethod.RELATIONAL,
			CompositionMethod.RELATIONAL);

	static private final Parameter setPoolingMethod= new Parameter("PoolingMethod", 
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setPoolingMethod((OntologyScaleSelectionMethod)option);}},
			OntologyScaleSelectionMethod.ALL,
			OntologyScaleSelectionMethod.MIN_TOTAL_ANCHORS_BASED, OntologyScaleSelectionMethod.MIN_ANCHORS_BASED, OntologyScaleSelectionMethod.ALL);
	
	static private final Parameter setMinimunAnchors= new Parameter("MinAnchors",
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setMinimumAnchorsParameter(option);}},
				defaultzero,defaultzero);
	
	static private final Parameter setMaxOntologies= new Parameter("MaxOntologies",
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setMaxOntologiesParameter(option);}},
				defaultzero,defaultzero);
	
	static private final Parameter setOntologySet= new Parameter("OntologySet", 
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setOntologySetParameter(option);}},
			defaultzero,
			defaultzero);

	static private final Parameter setRelationRestriction= new Parameter("RelationRestriction", 
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setRelationRestrictionParameter(option);}},
			"subsumption",
			"subsumption", "disjoint", "all");
	
	static private final Parameter setPathSelection= new Parameter("PathSelection", 
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setPathSelectionParameter(option);}},
			"all",
			"all", "first", "shortest");
	
	static private final Parameter setLocalPathLength= new Parameter("LocalPathLength",
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setLocalPathLengthParameter(option);}},
				"1","1");

	
	static private final Parameter setGlobalPathLength= new Parameter("GlobalPathLength",
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setGlobalPathLengthParameter(option);}},
				defaultzero,defaultzero);
	
	
	static private final Parameter setGlobalTraversingMethod= new Parameter("GlobalTravMethod",
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setGlobalPathTravMethodParameter(option);}},
				"fr.inrialpes.exmo.align.impl.method.NameEqAlignment",defaultzero);
	
	/**
	 * STRATEGIES
	 */

	static private final Parameter setContextMatchingStrategy= new Parameter("ContextMatchingStrategy", 
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setContextMatchingStrategy(option);}},
			defaultzero, 
			defaultzero);

	
	// After the matching...
	
	static private final Parameter setAggregationMethod= new Parameter("AggregationMethod", 
			new Parameter.Function(){ public void run(Object configTool, Object option){
				((ConfigurationBean)configTool).setAggregationMethod((AggregationMethod)option);}},
			AggregationMethod.LOGICAL_CONJUNCTION,
			AggregationMethod.NONE, AggregationMethod.LOGICAL_CONJUNCTION, AggregationMethod.LOGICAL_DISJUNCTION, AggregationMethod.POPULARITY_BASED);
	

	private final ConfigurationBean matcher;
	
	public CtxtMatcherConfiguration(){
		//initAllParameterMethods();
		setAllFieldsOptions(this.getClass().getDeclaredFields()); // fields should start with "set"
		this.matcher = new ConfigurationBean();
	}
	
	/**
	 * Initialization of parameters can either be done by prefixing all parameters (and only them) by "set"
	 * and by calling this method instead of the method setParameters.
	 * @param parameters 
	 */
	protected void setAllFieldsOptions(Field[] parameters) {
		ArrayList<Parameter> params = new ArrayList<Parameter>(parameters.length);
		for(Field field: parameters){
			if (field.getName().startsWith("set")&(field.getType().equals(Parameter.class))){ // parameter
				try {
					params.add((Parameter)(field.get(field)));
				} catch(IllegalAccessException e){
					AbstractConfigurable.log.warn("The field which name begins with -set- is not a valid Parameter"); // ignore parameter
				}
			}
		}
		setParameters(params.toArray(new Parameter[params.size()])); // pass all together, because the initialisation is static
	}
	

	//@Override
	public Object getConfiguredResource() {
		return matcher;
	}

	//@Override
	public String getMatcherDescription() {
		return CtxtMatcherConfiguration.description;
	}

	//@Override
	public String getMatcherName() {
		return CtxtMatcherConfiguration.name;
	}

}
