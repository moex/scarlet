package fr.inrialpes.exmo.scarlet.matcher.operation;

import java.util.Iterator;
import java.util.List;

import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.Assertion;
import fr.inrialpes.exmo.scarlet.struct.ConceptPath;
import fr.inrialpes.exmo.scarlet.struct.OntologyAssertion;

public class ComposeOperation {// extends AbstractOperation implements Operation {

	//This is used ONLY to inizialize the summary relation of a concept path!!!
	public ComposeOperation(){
	}

//	public Operation call() throws Exception {
//		return this; // FIXME
//	}
//
//	public int getNumberOfRelationsForThisLevel() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	public int getNumberOfRelationsForPreviousLevel() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	public int getUndefConnectionsForThisLevel() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	public Iterator<ConceptPath> getRelationsForThisLevel() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	public int getLevel() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
	
	public static RelationEnum compose(List<Assertion<String>> assertions) {
		RelationEnum summaryRelation = null;
		for(Assertion<String> assertion: assertions){
			RelationEnum nextRelation = assertion.getRelation();
			if (nextRelation==null){
				// let things as they are
			} else {
				if (summaryRelation==null){
					summaryRelation = nextRelation;
				} else {
					// FIXME Operation Composition! summaryRelation = nextRelation.compose(summaryRelation);
				}
			}
		}
		return summaryRelation;
	}
}
