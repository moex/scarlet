package fr.inrialpes.exmo.scarlet.matcher.conf;


public final class Parameter  {
	
	final private String name;
	final private Function function;
	final private Object[] options;	
	final private Object defaultOption;
	
	/**
	 * visibility package, to allow initialization in classes that implement PathSearcher
	 * review: need public, because new trials can be done, which need to access to the class!
	 */
	public static abstract class Function {
		abstract public void run(Object toolToConfigure, Object option);
	}

	/**
	 * visibility package, to allow initialization in classes that implement PathSearcher
	 * actually, public: to allow for initialization of configurable pathSearcher too!
	 */
	public Parameter(String name, Function parameterFunction, Object defaultOption, Object... options) {
		this.name = name;
		this.function = parameterFunction;
		this.defaultOption = defaultOption;
		this.options = options.clone();
		}

	public void run(Object toolToConfigure, Object option){
		function.run(toolToConfigure, option);
	}

	/**
	 * run with the default value
	 */
	/* public void run() {
		function.run(defaultOption);
	}*/

	public Object[] options() {
		return options; // TODO test: options are fixed, final... one should not be able to modify the options from outside!
	}

	public Object defaultValue(){
		return defaultOption;
	}
	
	int optionNumber() {
		return options.length;
	}
	
	public String name(){
		return name;
	}
	
	public String toString(){
		return name;
	}
	
	@Override
	public int hashCode(){
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Parameter)){
			return false;
		}
		if (((Parameter)obj).hashCode()!=this.hashCode()){
			return false;
		}
		return name.equals(((Parameter)obj).name());
	}
	
}
