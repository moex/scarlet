package fr.inrialpes.exmo.scarlet.matcher.conf;

import java.util.Collection;
import java.util.Properties;

import org.semanticweb.owl.align.Parameters;

public interface Configurable {
	
	public String getMatcherName();
	
	public String getMatcherDescription();
	
	public boolean contains(Parameter p);

	public Collection<Parameter> getParameters();

	public Object getConfiguredResource();
	
	/**
	 * @param param
	 * @return null if the parameter is not recognized
	 */
	public Parameter getParameter(String param);

	public ParameterCombination getParameterCombinationFromArgs(Properties args);
}
