package fr.inrialpes.exmo.scarlet.matcher.conf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;



public final class ParameterCombination implements Runnable {

	final private Map<Parameter, Object> currentOptions;
	final private Configurable resource;
	final Object toolToConfigure;
	
	/**
	 * prepare a new suite of parameters with default values
	 * @param resource
	 */
	public ParameterCombination(Configurable resource){
		this.resource = resource;
		this.toolToConfigure = resource.getConfiguredResource();
		this.currentOptions = new ConcurrentHashMap<Parameter, Object>();
		for(Parameter param: resource.getParameters()){
			currentOptions.put(param, param.defaultValue());
		}
	}

	public ParameterCombination(Configurable resource, final Map<Parameter, Object> options){
		this(resource);// we do that to avoid duplication, and be absolutely sure that
					 // there is an option for each parameter!
		for(Parameter p:options.keySet()){
			if(options.get(p)!=null){
				this.currentOptions.put(p, options.get(p)); 
			}
		}
	}
	
	ParameterCombination(Configurable resource, final List<Parameter> params, final List<Object> options){
		this.resource = resource;
		this.toolToConfigure = resource.getConfiguredResource();
		this.currentOptions = new ConcurrentHashMap<Parameter, Object>();
		
		for(Parameter param: resource.getParameters()){
			currentOptions.put(param, param.defaultValue());
		}
		for(int i=0;i<params.size();i++){
			if(options.get(i)!=null){
				this.currentOptions.put(params.get(i),options.get(i));
			} 
		}
		for(Parameter param: resource.getParameters()){
			if (!currentOptions.containsKey(param)){
				currentOptions.put(param, param.defaultValue());
			}
		}
	}
	
	
	public Collection<String> getParametersNames(){
		Collection<String> names = new ArrayList<String>();
		for(Parameter param:resource.getParameters()){
			names.add(param.name());
		}
		return names;
	}

	/**
	 * set parameter; if the parameter is not recognized, the option will not be recorded
	 * (it will be recorded as null, and therefore not retrieved, as retrieval is not achieved
	 *  by string)
	 * @param param
	 * @param option
	 * @return
	 */
	public void setParameter(String param, Object option) {
		currentOptions.put(resource.getParameter(param), option);
	}
	
	/**
	 * @param param
	 * @return the corresponding option, null if not found
	 */
	public Object getParameter(String param) {
		Parameter key = resource.getParameter(param);
		if (key==null){
			return null;
		} else {
			return currentOptions.get(key);
		}
	}

	/**
	 * @param param
	 * @return the corresponding option, null if not found
	 */
	public Object getParameter(Parameter param) {
		if (resource.contains(param)){
			return currentOptions.get(param);
		} else {
			return null;
		}
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer("Parameter Combination: \n");
		for(Parameter param: resource.getParameters()){
			sb.append(param.name()).append(":").append(currentOptions.get(param)).append(", ");
		}
		sb.append("\n");
		return sb.toString();
	}
	
	public void run(){
		for(Parameter param: resource.getParameters()){
			param.run(toolToConfigure, currentOptions.get(param));
		}
	}

}
