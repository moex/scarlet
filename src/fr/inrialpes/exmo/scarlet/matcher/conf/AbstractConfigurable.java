package fr.inrialpes.exmo.scarlet.matcher.conf;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.semanticweb.owl.align.Parameters;

//import fr.inrialpes.exmo.scarlet.concurrent.ThreadSafe;

//@ThreadSafe // because setParameter is called only at first, to instantiate the class!
public abstract class AbstractConfigurable implements Configurable {
	static protected Logger log = Logger.getRootLogger();

	private boolean initialized = false;
	
	private final Map<Integer, Parameter> parameters = new HashMap<Integer, Parameter>(20);

	

	public ParameterCombination getParameterCombinationFromArgs(Properties passedArguments) {
		Map<Parameter, Object> result = new HashMap<Parameter, Object>(this.getParameters().size());
		
		for(Enumeration<?> args = passedArguments.propertyNames();args.hasMoreElements();){
			String paramName = (String)args.nextElement();
			Object paramValue = passedArguments.getProperty(paramName);
			//System.out.println("Processing parameter " + paramName + " with value " + paramValue);
			Parameter p = this.getParameter(paramName);
			if (p!=null){
				Object[] options = p.options();
					for(int i=0;i<options.length;i++){
						Object currentOption = options[i];
						if(currentOption.toString().equalsIgnoreCase((String)paramValue)){
							result.put(p,currentOption);
							//log.debug(paramName + ": " + (String) paramValue);
							break;
						}
					}
					if(null == result.get(p)){
						result.put(p, paramValue);
					}
			}
		}
		for(Parameter param: getParameters()){  // set other parameters to default
			if (!result.containsKey(param)){
				result.put(param, param.defaultValue());
			}
		}
		return new ParameterCombination(this, result);
	}
	
	
	protected void setParameters(Parameter... params){
		if (initialized ) return; // set parameters only once!
		for(Parameter param: params){
			parameters.put(param.name().hashCode(), param);
		}
	}

	//@Override
	public Collection<Parameter> getParameters() {
		return parameters.values();
	}

	//@Override
	public boolean contains(Parameter param){
		return (parameters.values().contains(param));
	}
	
	
	//@Override
	public Parameter getParameter(String param) {
		return parameters.get(param.hashCode());
	}

	
	 /*(non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 * This is to serve as a predictable identifier, for keeping the association with the database
	 */
	@Override
	public int hashCode(){
		return 31*this.getClass().getName().hashCode() + parameters.size();
		
	}

}
