package fr.inrialpes.exmo.scarlet.matcher.task;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inrialpes.exmo.scarlet.context.ContextService;
import fr.inrialpes.exmo.scarlet.param.GlobalPathLengthParam;
import fr.inrialpes.exmo.scarlet.param.LocalPathLengthParam;
import fr.inrialpes.exmo.scarlet.param.OptionParameter;
import fr.inrialpes.exmo.scarlet.param.method.OntologyScaleSelectionMethod;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.Assertion;
import fr.inrialpes.exmo.scarlet.struct.ConceptPath;
import fr.inrialpes.exmo.scarlet.struct.ConfigurationBean;
import fr.inrialpes.exmo.scarlet.struct.Mapping;
import fr.inrialpes.exmo.scarlet.struct.OntologyFragment;
import fr.inrialpes.exmo.scarlet.struct.OntologyPairAlignment;
import fr.inrialpes.exmo.scarlet.struct.Pair;
import fr.inrialpes.exmo.scarlet.util.BreadthFirstSearch;


/**
 * @author Patrick HOFFMANN
 * Context matching using many a single intermediary ontology
 */
public class BasicCtxtMatchingTask {

	
	public int counter=0;
	public OntologyFragment<String> sourceOnto, targetOnto;
	private ContextService cs;
	private CtxtMatchingTask fatherTask;
	Map<Pair, Boolean> totalPairs = new HashMap<Pair, Boolean>();
    Map<String, Map<String, Set<Assertion<String>>>> allAssertionsFromSource = new HashMap<String, Map<String, Set<Assertion<String>>>>();
    Map<String, Map<String, Set<Assertion<String>>>> allAssertionsFromTarget = new HashMap<String, Map<String, Set<Assertion<String>>>>();
    Map<String, Set<Mapping>> allMappingsFromSource = new HashMap<String, Set<Mapping>>();
    Map<String, Set<Mapping>> allMappingsFromTarget = new HashMap<String, Set<Mapping>>();
    
	private Set<String> pool;

	private ConfigurationBean confOper;

	Map< Pair, Collection<ConceptPath>> pathsFound;
	private OntologyScaleSelectionMethod poolingSelectionMethod;
	private LocalPathLengthParam lpathlen;
	private GlobalPathLengthParam gpathlen;

	public BasicCtxtMatchingTask(CtxtMatchingTask fatherTask, ConfigurationBean operationFacade,
										OntologyFragment<String> sourceOnto, OntologyFragment<String> targetOnto){
		this.fatherTask = fatherTask;
		this.confOper = operationFacade;
		this.sourceOnto = sourceOnto;
		this.targetOnto = targetOnto;
		this.cs = fatherTask.getCtxtService();
		

		pathsFound = new HashMap< Pair, Collection<ConceptPath>>();
		pool = fatherTask.getPool();

		//oper = new CtxtMatchOperation();

		initParameter();
	}

	private void initParameter() {
		this.gpathlen = confOper.getGlobalPathLengthParameter();
		this.lpathlen = confOper.getLocalPathLengthParameter();
	}

	public void run(){
		// first treat the case of concepts that are in both ontologies (with the same URI!)
		//Angela: it should depend on the contextualisation method. The shared concepts should be compared accordingly.
		//Collection<String> sharedConcepts = sourceOnto.sharedConcepts(targetOnto);
		
//		for(String cpt : sourceOnto){
//			for(String cpt2 : targetOnto){
//				Pair couple = new Pair(cpt,cpt2);
//				totalPairs.put(couple, false);
//			}
//		}

		
		
		for(String ontoSelected: pool){
			for(String cpt : sourceOnto){
				for(String cpt2 : targetOnto){
					Pair couple = new Pair(cpt,cpt2);
					totalPairs.put(couple, false);
				}
			}
			MatchingTask matchingTaskLeft = new MatchingTask(this, confOper, sourceOnto, ontoSelected);
			matchingTaskLeft.run();

			// comme c'est la fin du ctxtBasedMatching, ce n'est pas grave si on ne matche qu'une petite partie!
			MatchingTask matchingTaskRight = new MatchingTask(this, confOper, targetOnto, ontoSelected);
			matchingTaskRight.run();

			//System.out.println("EXPLORATION of " + ontoSelected);
			counter = counter + 1;
			//System.out.println("ONTOLOGY NR " + counter);
			
			ExplorationTask exploringTask = new ExplorationTask(this.fatherTask.getCtxtService(), ontoSelected, confOper, matchingTaskLeft.getAnchors(), matchingTaskRight.getAnchors(), totalPairs, sourceOnto, targetOnto);
			exploringTask.run();
	
			// complete paths (from source to target ontology, including mappings)
			Map< Pair, Collection<ConceptPath>> loopResult = completePaths(matchingTaskLeft, exploringTask.getPaths(), matchingTaskRight);
			
			for(Pair couple: loopResult.keySet()){
			addPaths(loopResult.get(couple), pathsFound);
			totalPairs.put(couple, true);
			}
			
			pathsFound = removeIdentityPaths(pathsFound);
			
			//GRAPH TRAVERSAL DATA STRUCTURES, IF ANY

				if(gpathlen.getLength() > 0){
					//Only if there is a graph traversal...
					exploringTask.computeAssertions();
					
					Map<String, Set<Mapping>> mapleft = matchingTaskLeft.getAlignment();
					Set<Mapping> allleft = new HashSet<Mapping>();
					for(String s : mapleft.keySet()){
						allleft.addAll(mapleft.get(s));
					}
					
					Map<String, Set<Mapping>> mapright = matchingTaskRight.getAlignment();
					Set<Mapping> allright = new HashSet<Mapping>();
					for(String s : mapright.keySet()){
						allright.addAll(mapright.get(s));
					}
					
					allMappingsFromSource.put(ontoSelected, allleft);
					allMappingsFromTarget.put(ontoSelected, allright);
				}
		}


		//FIX IT - GLOBAL EXPLORATION TASK!!!
		if(gpathlen.getLength() > 0){
			
			//this is a good place to do the matching of the pool
			Map<Integer, OntologyPairAlignment> allmatched = new HashMap<Integer, OntologyPairAlignment>();
			LinkedList<String> ontotomatch = new LinkedList<String>();
			Iterator poolit = pool.iterator();
			for( ; poolit.hasNext() ;){
				ontotomatch.add((String)poolit.next());
			}
			
			for(int i=0; i < ontotomatch.size()-1; i++){
						OntologyPairAlignment opa = new OntologyPairAlignment(ontotomatch.get(i), ontotomatch.get(i+1), confOper.getGlobalPathTravMethod());
						allmatched.put(i, opa);
			}
			
			
				LinkedList<OntologyPairAlignment> singleComparison = new LinkedList<OntologyPairAlignment>();
				Integer[] keyset = allmatched.keySet().toArray(new Integer[allmatched.keySet().size()]);
				for(int om = 0; om < keyset.length; om++){
					int index = om;
					if(index < keyset.length){
					for(int op=0; op < gpathlen.getLength(); op++){
						if(index < keyset.length){
							singleComparison.add(allmatched.get(keyset[index]));
							index++;
						}
					}


					String sourceonto = singleComparison.get(0).getSourceOnto();
					String targetonto = singleComparison.get(singleComparison.size()-1).getTargetOnto();
					Set<Mapping> leftsideM = allMappingsFromSource.get(sourceonto);
					Set<Mapping> rightsideM = allMappingsFromTarget.get(targetonto);
					LinkedList<Set<Mapping>> traversing = new LinkedList<Set<Mapping>>();
					traversing.add(leftsideM);
					//Inizializing...
					for(int init=0; init < singleComparison.size(); init++){
						traversing.add(singleComparison.get(init).getAlignment());
					}
					traversing.add(rightsideM);
					
					//Esploring for paths pairwise
					OntologyFragment<String> sourceO = new OntologyFragment(singleComparison.get(0).getSourceOnto());
					OntologyFragment<String> targetO = new OntologyFragment(singleComparison.get(singleComparison.size()-1).getTargetOnto());
					
					Map<Integer, Map<Pair, Collection<ConceptPath>>> pathsFirstOnto = new HashMap<Integer, Map<Pair, Collection<ConceptPath>>>();
					
					Map<Integer, Map<Pair, Collection<ConceptPath>>> pathsLastOnto = new HashMap<Integer, Map<Pair, Collection<ConceptPath>>>();
					
					Map<Integer, LinkedList<Map<Pair, Collection<ConceptPath>>>> pathsChain = new HashMap<Integer, LinkedList<Map<Pair, Collection<ConceptPath>>>>();
					
					for(int pl=0; pl < lpathlen.getLength(); pl++){
					String newlen = Integer.toString(pl);
					confOper.setLocalPathLengthParameter((Object)newlen);
					

						ExplorationTask exploreST = new ExplorationTask(this.fatherTask.getCtxtService(), singleComparison.get(0).getSourceOnto(), confOper, getMappingRange(traversing.get(0)), getMappingDomain(traversing.get(1)), totalPairs, sourceO, sourceO);
						exploreST.run();
						pathsFirstOnto.put(pl, exploreST.getPaths());
					
					for(int trav=1; trav < traversing.size()-2; trav++){
						    LinkedList<Map<Pair, Collection<ConceptPath>>> singleLevel = new LinkedList<Map<Pair, Collection<ConceptPath>>>();
							OntologyFragment<String> sourceOO = new OntologyFragment(singleComparison.get(trav-1).getTargetOnto());
							ExplorationTask exploreINT = new ExplorationTask(this.fatherTask.getCtxtService(), singleComparison.get(trav-1).getTargetOnto(), confOper, getMappingRange(traversing.get(trav)), getMappingDomain(traversing.get(trav+1)), totalPairs, sourceOO, sourceOO);
							exploreINT.run();
							singleLevel.add(exploreINT.getPaths());
							pathsChain.put(pl, singleLevel);
					}
						ExplorationTask exploreSTT = new ExplorationTask(this.fatherTask.getCtxtService(), singleComparison.get(singleComparison.size()-1).getTargetOnto(), confOper, getMappingRange(traversing.get(traversing.size()-2)), getMappingDomain(getInverseMappings(traversing.get(traversing.size()-1))), totalPairs, targetO, targetO);
						exploreSTT.run();
						pathsLastOnto.put(pl, exploreSTT.getPaths());
					} // END COLLECTING PATHS CHAINS...
					
			
					
					// complete paths (from source to target ontology, including mappings)
					//Managing the case where the first and last ontologies have less levels / intermediate ontologies either
					Map<Integer, LinkedList<Map<Pair, Collection<ConceptPath>>>> mappathsforall = new HashMap<Integer, LinkedList<Map<Pair, Collection<ConceptPath>>>>();
					if(singleComparison.size() > 1){
						int min1 = Math.min(pathsFirstOnto.size(), pathsChain.size());
						int minpl = Math.min(min1, pathsLastOnto.size());
						
					for(int lop=0; lop < minpl; lop++){
						LinkedList<Map<Pair, Collection<ConceptPath>>> allLevpaths = new LinkedList<Map<Pair, Collection<ConceptPath>>>();
						
						Map<Pair, Collection<ConceptPath>> mpcfo = pathsFirstOnto.get(lop);
						LinkedList<Map<Pair, Collection<ConceptPath>>> mpcmo = pathsChain.get(lop);
						Map<Pair, Collection<ConceptPath>> mpclo = pathsLastOnto.get(lop);
						allLevpaths = new LinkedList<Map<Pair, Collection<ConceptPath>>>();
						allLevpaths.add(mpcfo);
						allLevpaths.addAll(mpcmo);
						allLevpaths.add(mpclo);
						mappathsforall.put(lop, allLevpaths);
					}
					}
					else{
						int min = Math.min(pathsFirstOnto.size(), pathsLastOnto.size());
						int max = Math.max(pathsFirstOnto.size(), pathsLastOnto.size());
						for(int pl=0; pl < min; pl++){
							LinkedList<Map<Pair, Collection<ConceptPath>>> allLevpaths = new LinkedList<Map<Pair, Collection<ConceptPath>>>();
							Map<Pair, Collection<ConceptPath>> mpcfo = pathsFirstOnto.get(pl);
							Map<Pair, Collection<ConceptPath>> mpclo = pathsLastOnto.get(pl);
							allLevpaths.add(mpcfo);
							allLevpaths.add(mpclo);
							mappathsforall.put(pl, allLevpaths);
						}
						if(min != max){
						for(int pl2=min; pl2 < max; pl2++){
							if(max == pathsFirstOnto.size()){
								LinkedList<Map<Pair, Collection<ConceptPath>>> allLevpaths = new LinkedList<Map<Pair, Collection<ConceptPath>>>();
								Map<Pair, Collection<ConceptPath>> mpcfo = pathsFirstOnto.get(pl2);
								allLevpaths.add(mpcfo);
								mappathsforall.put(pl2, allLevpaths);
							}else{
								LinkedList<Map<Pair, Collection<ConceptPath>>> allLevpaths = new LinkedList<Map<Pair, Collection<ConceptPath>>>();
								Map<Pair, Collection<ConceptPath>> mpcfo = pathsLastOnto.get(pl2);
								allLevpaths.add(mpcfo);
								mappathsforall.put(pl2, allLevpaths);
							}
						}
					}
						
					}
						for(int deflevel=0; deflevel < mappathsforall.size(); deflevel++){
						Map<Pair, Collection<ConceptPath>> loopResult = completeTravPaths(traversing.get(0), mappathsforall.get(deflevel), traversing.get(traversing.size()-1));
							for(Pair couple: loopResult.keySet()){
								addPaths(loopResult.get(couple), pathsFound);
								totalPairs.put(couple, true);
								}
							pathsFound = removeIdentityPaths(pathsFound);
						}
			
						
					singleComparison.clear();
					}
				}// END GLOBAL PATH LENGTH INTERMEDIARY ONTOLOGIES COUNT
				
		} // END GRAPH TRAVERSAL
				
				
			
			
			//Computes the paths not found for a couple
			for(Pair couple : pathsFound.keySet()){
				String cpt1= sourceOnto.uri() + '#' + localName(couple.source());
				String cpt2= targetOnto.uri() + '#' + localName(couple.target());
				Pair realCouple = new Pair(cpt1,cpt2);
				if(null != totalPairs.get(realCouple)){
					totalPairs.put(realCouple, true);
				}
			}

			
			
			//Selecting paths first shortest all
		if(confOper.getPathSelectionParameter().getPathSelection().equalsIgnoreCase("first")){
			for(Pair p : pathsFound.keySet()){
				Collection<ConceptPath> cp = pathsFound.get(p);
				ConceptPath[] cps = cp.toArray(new ConceptPath[cp.size()]);
				cp.clear();
				cp.add(cps[0]);
					pathsFound.put(p, cp);
				}
			}
		
		if(confOper.getPathSelectionParameter().getPathSelection().equalsIgnoreCase("shortest")){
			for(Pair p : pathsFound.keySet()){
				Collection<ConceptPath> cp = pathsFound.get(p);
				ConceptPath[] cps = cp.toArray(new ConceptPath[cp.size()]);
				ConceptPath acc = cps[0];
				Set<ConceptPath> exaequo = new HashSet<ConceptPath>();
				for(int i=1; i < cps.length; i++){
					ConceptPath cpt = cps[i];
					if(acc.length() > cpt.length()){
						acc = cps[i];
					}
				}
				for(int i=0; i < cps.length; i++){
					if(acc.length() == cps[i].length()){
						exaequo.add(cps[i]);
					}
				}
				pathsFound.put(p, exaequo);
			}
		}
		
		
		//STATISTICS
		//System.out.println("LOCAL PATHLENGTH: " + confOper.getLocalPathLengthParameter().getLength());
		//System.out.println("GLOBAL PATHLENGTH: " + confOper.getGlobalPathLengthParameter().getLength());
		
		//System.out.println("Number of Ontologies Used: " + counter);
		if(gpathlen.getLength() > 0){
			//System.out.println("Used Graph traversal for unmatched anchors (traversing pairwise ontologies): YES");
		}
		else{
			//System.out.println("Used Graph traversal for unmatched anchors (traversing pairwise ontologies): NO");
		}


	} // end run


	public static String localName(String cpt){
		int idx = Math.max(cpt.indexOf("#"), cpt.lastIndexOf("/")) + 1;
		return cpt.substring(idx);
	}

	
	
	private Map< Pair, Collection<ConceptPath>> completeTravPaths(Set<Mapping> source,
				LinkedList<Map<Pair, Collection<ConceptPath>>> paths, Set<Mapping> target) {
		Map< Pair, Collection<ConceptPath>> completePaths = new HashMap< Pair, Collection<ConceptPath>>();
		Set<Mapping> mappingsBefore, mappingsAfter;
		Collection<ConceptPath> tempPaths;
		for(int list=0; list < paths.size(); list++){
			for(Pair couple: paths.get(list).keySet()){
			mappingsBefore = cs.getMappingsForConcept(couple.source(), source);
			mappingsAfter = cs.getMappingsForConcept(couple.target(), target);
			addPaths(completePath(mappingsBefore, paths.get(list).get(couple), mappingsAfter), completePaths);
			}
		}
		return completePaths;
	}
	
	
	private Map< Pair, Collection<ConceptPath>> completePaths(MatchingTask source,
											Map<Pair, Collection<ConceptPath>> paths, MatchingTask target) {
		Map< Pair, Collection<ConceptPath>> completePaths = new HashMap< Pair, Collection<ConceptPath>>();
		Set<Mapping> mappingsBefore, mappingsAfter;
		Collection<ConceptPath> tempPaths;
		for(Pair couple: paths.keySet()){
			mappingsBefore = source.getMappingsTo(couple.source());
			mappingsAfter = target.getMappingsTo(couple.target());
			addPaths(completePath(mappingsBefore, paths.get(couple), mappingsAfter), completePaths);
		}
		return completePaths;
	}

	private Collection<ConceptPath> completePath(Set<Mapping> start, Collection<ConceptPath> paths, Set<Mapping> end) {
		Collection<ConceptPath> result = new ArrayList<ConceptPath>();
		for(ConceptPath path: paths){
			for(Mapping mStart: start){
				for(Mapping mEnd: end){

					if(null != path){
						List<Assertion<String>> newList = new LinkedList<Assertion<String>>(path.getAssertions());
					newList.add(0, mStart);
					newList.add(mEnd.getInverse());
					result.add(new ConceptPath(newList));
					}
					else{
						List<Assertion<String>> startend = new LinkedList<Assertion<String>>();
						startend.add(mStart);
						startend.add(mEnd.getInverse());
						result.add(new ConceptPath(startend));
					}
				}
			}
		}
		return result;
	}


	private void printPaths(Map<Pair, Collection<List<RelationEnum>>> paths) {
		for(Pair couple: paths.keySet()){
			//System.out.print(couple.source());
			for(List<RelationEnum> path: paths.get(couple)){
				for(RelationEnum rel: path) {
					//System.out.print(" --" + rel.toString() + " -> " );
				}

			}
			//System.out.println(couple.target());
		}

	}

	public static void addPaths(Collection<ConceptPath> tempPaths, Map<Pair, Collection<ConceptPath>> completePaths) {
		for(ConceptPath path: tempPaths){
			Pair couple = path.getCorrespondingPair();
			if (completePaths.containsKey(couple)){
				completePaths.get(couple).add(path);
			} else {
				Collection<ConceptPath> collection = new HashSet<ConceptPath>();
				collection.add(path);
				completePaths.put(couple, collection);
			}
		}
	}

	/**
	 * @param paths
	 * @param result
	 */
	public static void mergePathsInto(Map<Pair, Collection<ConceptPath>> paths, Map<Pair, Collection<ConceptPath>> result) {
		for(Pair couple: paths.keySet()){
			if (result.containsKey(couple)){
				result.get(couple).addAll(paths.get(couple));
			} else {
				result.put(couple, paths.get(couple));
			}
		}
	}
	
	public void setPool(Set<String> ontologies) {
		this.pool = ontologies;
	}

	public Set<String> getPool() {
		if (pool==null){
			return fatherTask.getPool();
		} else {
			return pool;
		}
	}

	public Map< Pair, Collection<ConceptPath>> getAlignment() {
		return pathsFound;
	}

	public String getSourceOntology() {
		// TODO Auto-generated method stub
		return null;
	}

	public CtxtMatchingTask getFatherTask() {
		return fatherTask;
	}

	public ContextService getFatherCs(){
		return fatherTask.getCtxtService();
	}
	
	public Collection<String> getMappingDomain(Set<Mapping> mappings){
		Collection<String> domains = new HashSet<String>();
		for(Mapping mm : mappings){
			Assertion<String> a = (Assertion<String>)mm;
			domains.add(a.getDomain().toString());
		}
		return domains;
	}
	
	public Collection<String> getMappingRange(Set<Mapping> mappings){
		Collection<String> ranges = new HashSet<String>();
		for(Mapping mm : mappings){
			Assertion<String> a = (Assertion<String>)mm;
			ranges.add(a.getRange().toString());
		}
		return ranges;
	}
	
	public Set<Mapping> getInverseMappings(Set<Mapping> mappings) {
		Set<Mapping> actual = mappings;
		Set<Mapping> inverseMappings = new HashSet<Mapping>();
		for(Mapping m : actual){
			Mapping newm = new Mapping(m.getRange(), m.getDomain(), m.getRelation());
			inverseMappings.add(newm);
		}
		return inverseMappings;
	}
	
	public Map<Pair, Collection<ConceptPath>> removeIdentityPaths(Map<Pair, Collection<ConceptPath>> paths){
	//Remove inside each path false assertions of the kind intermediateOnto c1 - c1 
	Map< Pair, Collection<ConceptPath>> pathsToRemove = new HashMap< Pair, Collection<ConceptPath>>();
	for (Pair p : paths.keySet()){
		String source = p.source();
		String target = p.target();
		for(Pair p1 : totalPairs.keySet()){
			boolean found = totalPairs.get(p1);
			if(found){
				String sourceT = p1.source();
				String targetT = p1.target();
				if(source.equalsIgnoreCase(sourceT) && target.equalsIgnoreCase(targetT)){
					Collection<ConceptPath> allpa = paths.get(p1);
					Collection<ConceptPath> ToMan = new HashSet<ConceptPath>();
					for(ConceptPath cp : allpa){
						List<Assertion<String>> asscleaned = new ArrayList<Assertion<String>>();
						List<Assertion<String>> assertions = cp.getAssertions();
						for(Assertion a : assertions){
							if(a.getDomain().toString().equalsIgnoreCase(a.getRange().toString())){
							}
							else{
								asscleaned.add(a);
							}
						}
						ConceptPath cpcleaned = new ConceptPath(asscleaned, cp.getSummaryRelation());
						ToMan.add(cpcleaned);
					}
					pathsToRemove.put(p1,ToMan);
					}
				}
			}
		}
	for(Pair tr : pathsToRemove.keySet()){
		paths.put(tr, pathsToRemove.get(tr));
	}
		return paths;
	}
	
	
}
