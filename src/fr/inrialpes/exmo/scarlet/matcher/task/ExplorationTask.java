package fr.inrialpes.exmo.scarlet.matcher.task;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inrialpes.exmo.scarlet.context.ContextService;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonCachedClass;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonCachedService;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonConceptProxy;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonService;
import fr.inrialpes.exmo.scarlet.param.OptionParameter;
import fr.inrialpes.exmo.scarlet.param.LocalPathLengthParam;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.Assertion;
import fr.inrialpes.exmo.scarlet.struct.ConceptPath;
import fr.inrialpes.exmo.scarlet.struct.ConfigurationBean;
import fr.inrialpes.exmo.scarlet.struct.OntologyFragment;
import fr.inrialpes.exmo.scarlet.struct.Pair;
import fr.inrialpes.exmo.scarlet.util.BreadthFirstSearch;

public class ExplorationTask {

	private String ontology1 = null;
	private String ontology2 = null;

	private ContextService cs;
	BreadthFirstSearch expandFromSource, expandFromTarget, sourceAssertions, targetAssertions;

	private Collection<String> sourceAnchors, targetAnchors;

	private Collection<String> exploredDuringPreviousLevel;
	private Collection<String> exploredDuringThisLevel;

	private ConfigurationBean confOper;
	private LocalPathLengthParam pathlength;
	private OntologyFragment<String> sourceOnto, targetOnto;
	private static Map<String, String> jointConc1 = new HashMap<String, String>();
	private static Map<String, String> jointConc2 = new HashMap<String, String>();
	private Map<Pair, Boolean> totalPairs;

	private Collection<ConceptPath> allpaths = new HashSet();
	private Map< Pair, Collection<ConceptPath>> result = new HashMap<Pair, Collection<ConceptPath>>();

	private int level = 0;

	public ExplorationTask(ContextService cs){
		this.cs = cs;
	}
	
	public ExplorationTask(ContextService cs, String ontology, ConfigurationBean confOper, Collection<String> sourceAnchors, Collection<String> targetAnchors, Map<Pair, Boolean> totalPairs, OntologyFragment<String> sourceOnto,  OntologyFragment<String> targetOnto) {
		this.totalPairs = totalPairs;
		this.ontology1 = ontology;
		this.sourceOnto = sourceOnto;
		this.targetOnto = targetOnto;
		this.sourceAnchors = sourceAnchors;
		this.targetAnchors = targetAnchors;
		this.cs = cs;
		this.confOper = confOper;
		initParameter();

		this.expandFromSource = new BreadthFirstSearch(this, sourceAnchors, ontology1);
		this.expandFromTarget = new BreadthFirstSearch(this, targetAnchors, ontology1);
		

		this.sourceAssertions = new BreadthFirstSearch(this, sourceAnchors, ontology1);
		this.targetAssertions = new BreadthFirstSearch(this, targetAnchors, ontology1);

	}

	public ExplorationTask(ContextService cs, String ontology11, String ontology22, ConfigurationBean confOper, Collection<String> sourceAnchors, Collection<String> targetAnchors, Map<Pair, Boolean> totalPairs, OntologyFragment<String> sourceOnto,  OntologyFragment<String> targetOnto) {
		this.totalPairs = totalPairs;
		this.ontology1 = ontology11;
		this.ontology2 = ontology22;
		this.sourceOnto = sourceOnto;
		this.targetOnto = targetOnto;
		this.sourceAnchors = sourceAnchors;
		this.targetAnchors = targetAnchors;
		this.cs = cs;

		this.confOper = confOper;
		initParameter();

		this.expandFromSource = new BreadthFirstSearch(this, sourceAnchors, ontology1);
		this.expandFromTarget = new BreadthFirstSearch(this, targetAnchors, ontology2);
		
		this.sourceAssertions = new BreadthFirstSearch(this, sourceAnchors, ontology1);
		this.targetAssertions = new BreadthFirstSearch(this, targetAnchors, ontology2);

	}


	private void initParameter() {
		//this.explorationMethod = confOper.getExplorationMethod();
		this.pathlength = confOper.getLocalPathLengthParameter();
	}

	public void run() {
		// start with simple comparison of anchors from source and target
		// ANGELA: IS IT CORRECT TO REMOVE THEM ONCE A MAPPING HAS BEEN FOUND. PROVE NOT!
		Collection<String> joints = getConceptsWhereAnchorsConnect(sourceAnchors, targetAnchors, ontology1, ontology2);
		if (!joints.isEmpty()){
			Map< Pair, Collection<ConceptPath>> paths = getAtomicPaths(joints, ontology1, ontology2);
			if(null != paths){
				for(Pair couple: paths.keySet()){
					updateAllPathsInResult(paths, couple);
				}
			}
//			if(!confOper.getExplorationStrategy().continueExploringOntologyForCouple()){
			//	removeAnchorsConnected(paths);
//			}
		}

		BreadthFirstSearch[] sourceLevels = new BreadthFirstSearch[getPathLength()+1];
		BreadthFirstSearch[] targetLevels = new BreadthFirstSearch[getPathLength()+1];
		for(int init=0; init < getPathLength()+1; init++){
			sourceLevels[init] = new BreadthFirstSearch(this, sourceAnchors, ontology1);
			targetLevels[init] = new BreadthFirstSearch(this, targetAnchors, ontology1);
		}
		
		int level = 0;
		for(int s=0; s < sourceLevels.length; s++){
     		level = s;
			for(int t=0; t < targetLevels.length; t++){
				level = level + t;
				if(level != 0 && level <= getPathLength()){
					if(s!=0){
					BreadthFirstSearch[] accs = new BreadthFirstSearch[s+1];
					accs[0] = sourceLevels[s];
						for(int n=1; n < s+1; n++){
							accs[n-1].next();
					    	try {
								accs[n] =  (BreadthFirstSearch) accs[n-1].clone();
							} catch (CloneNotSupportedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					    }
						try {
							this.expandFromSource = (BreadthFirstSearch) accs[accs.length-1].clone();
						} catch (CloneNotSupportedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					    exploredDuringPreviousLevel = accs[accs.length-1].peek();
					}
					else{
						 exploredDuringPreviousLevel = expandFromSource.peek();
					}
					
					if(t!=0){
						BreadthFirstSearch[] acct = new BreadthFirstSearch[t+1];
						acct[0] = targetLevels[t];
						for(int n=1; n < t+1; n++){
								acct[n-1].next();
						    	try {
									acct[n] =  (BreadthFirstSearch) acct[n-1].clone();
								} catch (CloneNotSupportedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						}
						try {
							this.expandFromTarget = (BreadthFirstSearch) acct[acct.length-1].clone();
						} catch (CloneNotSupportedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					    exploredDuringThisLevel = acct[acct.length-1].peek();
					}
					else
					{
						exploredDuringThisLevel = expandFromTarget.peek();
					}
			
					if (!exploredDuringThisLevel.isEmpty()){
					joints = getConceptsWhereAnchorsConnect(exploredDuringThisLevel, exploredDuringPreviousLevel, expandFromSource.getOntology(), expandFromTarget.getOntology());
					if (!joints.isEmpty()){
						//System.out.println("concepts where anchors are connected: " + printConceptCollection(joints));
	
						Map< Pair, Collection<ConceptPath>> paths = getPathsJustFormed(joints, expandFromSource.getOntology(), expandFromTarget.getOntology());
						if(null != paths){
							for(Pair couple: paths.keySet()){
								if(null != paths.get(couple)){
									updateAllPathsInResult(paths, couple);
								}
							}
						}
					}
				} // end procedure of joints paths
			
		}
				
		}
			
		} // end while pl
		
		
		
		if(null != result){
			printCollectionOfPaths(result);
		}
	}
	

	public void computeAssertions(){
		int lev = 0;
		while(lev < getPathLength()){
			sourceAssertions.next();
			targetAssertions.next();
			lev++;
		}
	}
		
    public Map<String, Set<Assertion<String>>> getSavedAssertions(String what){
    	Map<String, Set<Assertion<String>>> assertions;
    	if(what.equalsIgnoreCase("source")){
    		assertions = sourceAssertions.getSavedAssertions();
    		printSavedAssertions(sourceAssertions.getSavedAssertions(), "source");
			
    	}
    	else{
    		assertions = targetAssertions.getSavedAssertions();
    		printSavedAssertions(targetAssertions.getSavedAssertions(), "target");
    	}
    	return assertions;
    }
    
	public void printSavedAssertions(Map<String, Set<Assertion<String>>> savedAssertions, String what){
		//System.out.println("SAVED ASSERTIONS FOR ONTOLOGY " + what);
		for(String s : savedAssertions.keySet()){
			//System.out.println("MEGA KEY CONCEPT " + s);
			Set<Assertion<String>> ass = savedAssertions.get(s);
			for(Assertion a : ass){
				//System.out.println("ASSERTION DOMAIN" + a.getDomain().toString() + " rel " + a.getRelation().name() + " RANGE " + a.getRange().toString());
			}
			
		}
		//System.out.println("--------------------------");
	}


	public void updateTotalPairs(Map< Pair, Collection<ConceptPath>> paths){
		for(Pair couple: paths.keySet()){
			String cpt1= sourceOnto.uri() + '#' + localName(couple.source());
			String cpt2= targetOnto.uri() + '#' + localName(couple.target());
			Pair realCouple = new Pair(cpt1,cpt2);
			if(null != totalPairs.get(realCouple)){
				totalPairs.put(realCouple, true);
			}
		}
	}
	
	public void updateAllPathsInResult(Map< Pair, Collection<ConceptPath>> paths, Pair couple){
	    Collection<ConceptPath> pathinres = new HashSet();
		for(ConceptPath path: paths.get(couple)){
			if(null != path){
				pathinres.add(path);
			}
		}
				if(null != result.get(couple)){
					Collection<ConceptPath> whatinres = new HashSet();
					Collection<ConceptPath> uniqueinres = new HashSet();
					whatinres = result.get(couple);
					for(ConceptPath p : pathinres){
						whatinres.add(p);
					}
					HashMap <String, ConceptPath> uniquecp = new HashMap<String, ConceptPath>();
					
					Iterator it = whatinres.iterator();
					for(; it.hasNext(); ){
						ConceptPath wir = (ConceptPath)it.next();
						String s = wir.toString();
						uniquecp.put(s, wir);
					}
					for(String cpunique : uniquecp.keySet()){
						uniqueinres.add(uniquecp.get(cpunique));
					}
					result.put(couple, uniqueinres);
				}
				else{
					result.put(couple, pathinres);
				}

		}

	public static String printConceptCollection(Collection<String> concepts){
		StringBuffer buffer = new StringBuffer("[");
		for(String cpt: concepts){
			buffer.append(cpt).append(", ");
		}
		buffer.append("]");
		return buffer.toString();
	}

	public static String localName(String cpt){
		int idx = Math.max(cpt.indexOf("#"), cpt.lastIndexOf("/")) + 1;
		return cpt.substring(idx);
	}

	public static void printCollectionOfPaths(Map< Pair, Collection<ConceptPath>> paths){
		//System.out.println("I AM HERE ");
		if (!paths.isEmpty()){
			for(Pair couple : paths.keySet()){
				for(ConceptPath path: paths.get(couple)){
					if(null != path){
						//System.out.println(path.toString());
					}
				}
			}
		}
	}

//	/**
//	 * @param level > 0
//	 */
//	private void switchBFS(){
//		BreadthFirstSearch tempBFS = currentBFS;
//		currentBFS = previousBFS;
//		previousBFS = tempBFS;
//	}

	private void removeAnchorsConnected(Map< Pair, Collection<ConceptPath>> paths){
		//updateTotalPairs(paths);
	}


	private Set<String> getConceptsWhereAnchorsConnect(Collection<String> exploredConcepts1, Collection<String> exploredConcepts2, String onto1, String onto2){
		Set<String> conceptsWherePathsMeet = new HashSet<String>();
		jointConc1.clear();
		jointConc2.clear();
		// see if the paths from source and target coincide in some middle concepts
		if(onto1.equalsIgnoreCase(onto2) || null == onto2){
			conceptsWherePathsMeet.addAll(exploredConcepts1);
			conceptsWherePathsMeet.retainAll(exploredConcepts2);
			return conceptsWherePathsMeet;
		}
		else{
			for(String o1 : exploredConcepts1){
				jointConc1.put(localName(o1).toLowerCase(), o1);
			}
			for(String o2 : exploredConcepts2){
				jointConc2.put(localName(o2).toLowerCase(), o2);
			}
			//here you should match of compare with a matching instead
			conceptsWherePathsMeet.addAll(jointConc1.keySet());
			conceptsWherePathsMeet.retainAll(jointConc2.keySet());
			
			
		}
		return conceptsWherePathsMeet;
	}


	private Map< Pair, Collection<ConceptPath>> getPathsJustFormed(Collection<String> joints, String onto1, String onto2) {
		Map< Pair, Collection<ConceptPath>> tempResult;

		// build paths from concept to concept!
		if(onto1.equalsIgnoreCase(onto2) || null == onto2){
			Map<Pair, ConceptPath> pathsFromSource, pathsFromTarget;
			pathsFromSource = expandFromSource.getPathsToOriginToCustomConcepts(joints, null, null);
			pathsFromTarget = expandFromTarget.getPathsToOriginToCustomConcepts(joints, null, null);
			tempResult = mergeAllPaths(pathsFromSource, inversePathAndAssertions(pathsFromTarget));
				return tempResult;
			}
		else{
			Map<Pair, ConceptPath> pathsFromSource, pathsFromTarget;
			String o1 = expandFromSource.getOntology();
			String o2 = expandFromTarget.getOntology();
			String oneUriRandom = getJointConc1().get(joints.toArray()[0]);
			if(oneUriRandom.toLowerCase().contains(expandFromSource.getFragmentOnto())){
				pathsFromSource = expandFromSource.getPathsToOriginToCustomConcepts(joints, o1, getJointConc1());
				pathsFromTarget = expandFromTarget.getPathsToOriginToCustomConcepts(joints, o2, getJointConc2());
			}
			else
			{
				pathsFromSource = expandFromSource.getPathsToOriginToCustomConcepts(joints, o2, getJointConc2());
				pathsFromTarget = expandFromTarget.getPathsToOriginToCustomConcepts(joints, o1, getJointConc1());
			}
			tempResult = mergeAllPaths(pathsFromSource, inversePathAndAssertions(pathsFromTarget));
				return tempResult;
			}
	}


	public static Map< Pair, Collection<ConceptPath>> getAtomicPaths(Collection<String> joints, String onto1, String onto2) {
		Map< Pair, Collection<ConceptPath>> tempResult = new HashMap< Pair, Collection<ConceptPath>>();
		if(onto1.equalsIgnoreCase(onto2) || null == onto2){
		for(String cpt: joints){
				Collection<ConceptPath> paths = new HashSet<ConceptPath>();
				List<Assertion<String>> path = new LinkedList<Assertion<String>>();
				path.add(new Assertion<String>(RelationEnum.equiv, cpt, cpt));
				paths.add(new ConceptPath(path));
				tempResult.put(new Pair(cpt, cpt), paths);
		}
			return tempResult;
		}
		else{
			for(String cpt: joints){
				Collection<ConceptPath> paths = new HashSet<ConceptPath>();
				List<Assertion<String>> path = new LinkedList<Assertion<String>>();
				String cpt1 = getJointConc1().get(cpt);
				String cpt2 = getJointConc2().get(cpt);
				//if(!cpt1.equalsIgnoreCase(cpt2)){
					path.add(new Assertion<String>(RelationEnum.equiv, cpt1, cpt2));
					paths.add(new ConceptPath(path));
					tempResult.put(new Pair(cpt1, cpt2), paths);
				//}
			}
			return tempResult;
		}
	}

	public static Map< Pair, Collection<ConceptPath>> getAtomicPathsFromSharedLabels(Collection<String> joints, String sourceURI, String targetURI) {
		Map< Pair, Collection<ConceptPath>> tempResult = new HashMap< Pair, Collection<ConceptPath>>();
		for(String cpt: joints){
			Collection<ConceptPath> paths = new HashSet<ConceptPath>();
			List<Assertion<String>> path = new LinkedList<Assertion<String>>();
			String sourceString = sourceURI + '#' + cpt;
			String targetString = targetURI + '#' + cpt;
			//if(!sourceString.equalsIgnoreCase(targetString)){
			path.add(new Assertion<String>(RelationEnum.equiv, sourceString, targetString));
			paths.add(new ConceptPath(path));
			tempResult.put(new Pair(cpt, cpt), paths);
			//}
		}
		return tempResult;
	}



	private Map<Pair, Collection<ConceptPath>> mergeAllPaths(Map<Pair, ConceptPath> startPaths,
															  Map<Pair, ConceptPath> endPaths) {
		Map<Pair, Collection<ConceptPath>> resultMap = new HashMap<Pair, Collection<ConceptPath>>();
		Map<Pair, ConceptPath> tempEnd = new HashMap<Pair, ConceptPath>();
		tempEnd.putAll(endPaths);
		for(Pair couple: startPaths.keySet()){
			//System.out.println("merge dei path della coppia " + couple.source() + ", " + couple.target());
			String intermConcept = localName(couple.target());
			//System.out.println("concetto intermedio " + intermConcept);
			for(Pair coupleEnd: tempEnd.keySet()){
				if (localName(coupleEnd.source()).equalsIgnoreCase(intermConcept)){
					//System.out.println("ha trovato corrisp tra end cpt " + coupleEnd.source() + " e " + intermConcept);
					Pair mergedCouple = new Pair(couple.source(), coupleEnd.target());

					////if(null != startPaths.get(couple).concatWith(tempEnd.get(coupleEnd))){
						ConceptPath mergedPath = startPaths.get(couple).concatWith(tempEnd.get(coupleEnd));
					//CODE TO UNDERSTANT - DELETE FROM HERE TO SYSTEM OUT PRINT
//					List<Assertion<String>> assertionFromMergedPath = mergedPath.getAssertions();
//					for(Assertion<String> ass : assertionFromMergedPath){
//						System.out.println("il merged path ha dominio " + ass.getDomain() + " rel " + ass.getRelation().name() + " range " + ass.getRange());
//					}

						updateResultMap(mergedCouple, mergedPath, resultMap);
					////}
				} // else nothing
			}
		}
		return resultMap;
	}


	private void updateResultMap(Pair mergedCouple, ConceptPath mergedPath, Map<Pair, Collection<ConceptPath>> resultMap) {
		if (resultMap.containsKey(mergedCouple)){
			resultMap.get(mergedCouple).add(mergedPath);
		} else {
			Collection<ConceptPath> resultSetForCouple = new HashSet<ConceptPath>();
			resultSetForCouple.add(mergedPath);
			resultMap.put(mergedCouple, resultSetForCouple);
		}
	}

	private Map<Pair, ConceptPath> inversePathAndAssertions(Map<Pair, ConceptPath> pathsFromTarget) {
		Map<Pair, ConceptPath> result = new HashMap<Pair, ConceptPath>();
		for(Pair couple: pathsFromTarget.keySet()){
			result.put(couple.getInverse(), pathsFromTarget.get(couple).getInverse());
		}
		return result;
	}

	
	public Map< Pair, Collection<ConceptPath>> getPaths() {
		return result;
	}

//	public Map<String, Assertion<String>> getNeighbors(String currentConcept, String ontology) {
//		return this.explorationMethod.explore(this, currentConcept, ontology);
//	}

//	public Double weight(RelationEnum rel) {
//		return explorationMethod.weight(rel);
//	}

	public Double weight(RelationEnum rel) {
		return 1.0;
    }
	
	
	public int getCurrentLevel() {
		return this.level ;
	}
	
	public int getPathLength(){
		return pathlength.getLength();
	}

	public Map<Pair, Boolean> getTotalPairs(){
		return totalPairs;
	}

	public static Map<String, String> getJointConc1(){
		return jointConc1;
	}

	public static Map<String, String> getJointConc2(){
		return jointConc2;
	}
	
	public ContextService getCtxtService(){
		return this.cs;
	}

}
