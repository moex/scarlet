package fr.inrialpes.exmo.scarlet.matcher.task;

import java.net.URI;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import fr.inrialpes.exmo.scarlet.context.ContextService;
import fr.inrialpes.exmo.scarlet.param.method.OntologyScaleSelectionMethod;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.Assertion;
import fr.inrialpes.exmo.scarlet.struct.ConceptPath;
import fr.inrialpes.exmo.scarlet.struct.ConfigurationBean;
import fr.inrialpes.exmo.scarlet.struct.OntologyFragment;
import fr.inrialpes.exmo.scarlet.struct.Pair;

public class CtxtMatchingTask {

	private ContextService cs;
	private OntologyFragment<URI> onto1, onto2;
	
	private Set<String> pool;

	private ConfigurationBean confOper;
	private CtxtMatchingTask fatherTask;
	private Map<Pair, Collection<ConceptPath>> pathsFound;

	private OntologyScaleSelectionMethod poolingSelectionMethod;
	private SelectionTask seltask;
	
	public CtxtMatchingTask(ContextService cs, ConfigurationBean facade, OntologyFragment<URI> onto1, OntologyFragment<URI> onto2) {
		this.confOper = facade;
		this.onto1 = onto1;
		this.onto2 = onto2;
		this.cs = cs;

		//this.intermediaryOntologies = new ConcurrentHashMap<String, OntologyFragment<String>>();
		
		this.pool = Collections.emptySet();
		
		fatherTask = this; // initial CtxtMatchingTask has no father task! 
		pathsFound = new HashMap<Pair, Collection<ConceptPath>>();
		
		initParameter();	
	}
	
	private CtxtMatchingTask(ContextService cs, ConfigurationBean facade, CtxtMatchingTask fatherTask, OntologyFragment<URI> onto1, OntologyFragment<URI> onto2) {
		this(cs, facade, onto1,onto2);
		this.fatherTask = fatherTask;
		this.cs = cs;
	}
	

	private void initParameter() {
	}
	
	public void run(){
	
		seltask = new SelectionTask(this, confOper, onto1.toStringVersion(), onto2.toStringVersion()); 
		seltask.run();
		try {
			this.pool = cs.getAllSelectedOntologies();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BasicCtxtMatchingTask subTask = new BasicCtxtMatchingTask(this, this.confOper, onto1.toStringVersion(),  onto2.toStringVersion());
		subTask.run();
		
		BasicCtxtMatchingTask.mergePathsInto(subTask.getAlignment(), pathsFound);
		
	}
	
	public void setPool(Set<String> ontologies){
		this.pool = ontologies;
	}

	public Set<String> getPool() {
		return pool;
	}

	public CtxtMatchingTask getFatherTask() {
		return fatherTask;
	}
	
	public ContextService getCtxtService(){
		return cs;
	}

	public Map<Pair, Collection<ConceptPath>> getAlignment() {
		return pathsFound;
	}
}
