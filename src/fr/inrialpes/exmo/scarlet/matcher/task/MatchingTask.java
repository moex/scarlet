package fr.inrialpes.exmo.scarlet.matcher.task;

import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.semanticweb.owl.align.Alignment;

//import fr.inrialpes.exmo.scarlet.param.method.ComparisonMeasure;
import fr.inrialpes.exmo.align.impl.BasicParameters;
import fr.inrialpes.exmo.scarlet.context.ContextService;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonCachedService;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonOntologyProxy;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonService;
import fr.inrialpes.exmo.scarlet.param.method.MatchingMethod;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.ConfigurationBean;
import fr.inrialpes.exmo.scarlet.struct.Mapping;
import fr.inrialpes.exmo.scarlet.struct.OntologyFragment;


public class MatchingTask {

//	protected static WatsonService watsonService;
//	//private static boolean initialized = false;
//	static {
//
//		watsonService = WatsonCachedService.getInstance();
//	}
	
	private OntologyFragment<String> source;
	private String targetURI;
	
	private BasicCtxtMatchingTask fatherTask;
	private ContextService cs;
	private ConfigurationBean confOper;
	
	// anchors -> mappings. Facilitate the exploration, and the making of a paths for concepts being
	// actually related!
	private Map<String, Set<Mapping>> alignment;
	
	private Set<String> pool;
	private MatchingMethod matchingMethod;
	
	public MatchingTask(){
		
	}
	
	public MatchingTask(BasicCtxtMatchingTask fatherTask, ConfigurationBean confOper, OntologyFragment<String> sourceOnto, String targetURI) {
		this.fatherTask = fatherTask;
		this.confOper = confOper;
		this.cs = fatherTask.getFatherCs();
		this.source = sourceOnto;
		this.targetURI = targetURI;
		
		pool = null;
		initParameter();
	}

	private void initParameter() {
		this.matchingMethod = confOper.getMatchingMethod();
	}
	
	public void run(){
		
		//here calling the interface method, that should call the right anchor method
		alignment = cs.anchor(source, targetURI, this, this.matchingMethod.getMethod());
	}

	public Set<String> getAnchors(){
		return alignment.keySet();
	}

	public Map<String, Set<Mapping>> getAlignment() {
		return alignment;
	}

	public BasicCtxtMatchingTask getFatherTask() {
		return fatherTask;
	}

	public Set<String> getPool() {
		if (pool==null){
			return fatherTask.getPool();
		} else {
			return pool;
		}
	}

	public Set<Mapping> getMappingsTo(String target) {
		return alignment.get(target);
	}

	/**
	 * optimized version: includes the loop inside the function!
	 * @param anchors
	 * @param sourceConcept
	 * @param rel
	 * @param result
	 */
	public static void addMappings(Collection<String> anchors, String sourceConcept, RelationEnum rel, Map<String, Set<Mapping>> result) {
		for(String anchor: anchors){
			Mapping mapping = new Mapping(sourceConcept, anchor, rel);
			addMapping(anchor, mapping, result);
		}
	}

	public static void addMapping(String anchor, Mapping mapping, Map<String, Set<Mapping>> result) {
		if (result.containsKey(anchor)){
			result.get(anchor).add(mapping);
		} else {
			Set<Mapping> mappings = new HashSet<Mapping>();
			mappings.add(mapping);
			result.put(anchor, mappings);
		}
	}

	// TODO see with replacing...
	public static <X,Y> void addToMapCollection(X key, Y el, Map<X, Set<Y>> result) {
		if (result.containsKey(key)){
			result.get(key).add(el);
		} else {
			Set<Y> set = new HashSet<Y>();
			set.add(el);
			result.put(key, set);
		}
	}
	
	public ConfigurationBean getConfigurationBean(){
		return this.confOper;
	}
}
