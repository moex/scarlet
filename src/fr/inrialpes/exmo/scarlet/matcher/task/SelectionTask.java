package fr.inrialpes.exmo.scarlet.matcher.task;

import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.inrialpes.exmo.scarlet.context.ContextService;
import fr.inrialpes.exmo.scarlet.context.OntologySetContextService;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonCachedService;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonConceptProxy;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonService;
import fr.inrialpes.exmo.scarlet.param.OptionParameter;
import fr.inrialpes.exmo.scarlet.param.OptionParameterRestriction;
import fr.inrialpes.exmo.scarlet.param.method.OntologyScaleSelectionMethod;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.ConfigurationBean;
import fr.inrialpes.exmo.scarlet.struct.OntologyFragment;
import fr.inrialpes.exmo.scarlet.util.filter.CollectionFilter;
import fr.inrialpes.exmo.scarlet.util.filter.FilterCriteria;
import fr.inrialpes.exmo.scarlet.util.filter.MapFilter;

public class SelectionTask {

	public static final Integer ESTIMATION_SIZE_ONTSELECTION = 128;
	//protected static WatsonService watsonService;
	
	private CtxtMatchingTask tsk;
	private ContextService cs;
	private OntologyFragment<String> onto2;
	private OntologyFragment<String> onto1;
	private ConfigurationBean conf;
	private Set<String> result;
	private OntologyScaleSelectionMethod poolingSelectionMethod;
	private Map<String, Integer> ontoAnchoresOnto1 = new HashMap<String, Integer>();
	private Map<String, Integer> ontoAnchoresOnto2 = new HashMap<String, Integer>();
	private Map<String, Integer> ontoAnchored = new HashMap<String, Integer>();
	
	
	public SelectionTask(CtxtMatchingTask ctxt, ConfigurationBean conf, OntologyFragment<String> onto1, OntologyFragment<String> onto2){
	this.conf = conf;
	this.onto1 = onto1;
	this.onto2 = onto2;
	this.tsk = ctxt;
	this.cs = ctxt.getCtxtService();
	result = new HashSet<String>(ESTIMATION_SIZE_ONTSELECTION);
	//watsonService = WatsonCachedService.getInstance();
	
	initParameter();
	
	}

	private void initParameter() {
		this.poolingSelectionMethod = conf.getPoolingSelectionMethod();
	}
	
	public void run(){

		for(String sourceConcept: onto1){
			Collection<String> partialRes = new HashSet<String>();
			try {
				partialRes = cs.getOntologiesForConcept(sourceConcept);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(!partialRes.isEmpty()){
				for(String o : partialRes){
					if(null != ontoAnchoresOnto1.get(o)){
						int acc = ontoAnchoresOnto1.get(o);
						acc = acc +1;
						ontoAnchoresOnto1.put(o, acc);
					}
					else{
						ontoAnchoresOnto1.put(o, 1);
					}
				}
			}
			result.addAll(partialRes);
		}
		
		for(String targetConcept: onto2){
			Collection<String> partialRes = new HashSet<String>();
			try {
				partialRes = cs.getOntologiesForConcept(targetConcept);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(!partialRes.isEmpty()){
				for(String o : partialRes){
					if(null != ontoAnchoresOnto2.get(o)){
						int acc = ontoAnchoresOnto2.get(o);
						acc = acc +1;
						ontoAnchoresOnto2.put(o, acc);
					}
					else{
						ontoAnchoresOnto2.put(o, 1);
					}
				}
			}
			result.addAll(partialRes);
		}
		
		result = poolingSelectionMethod.selectOntologies(this, onto1);
		cs.setAllSelectedOntologies(result);
	}
	
	public Collection<String> filterByMinNb(Map<String, Integer> nbTermsAnchored, final int threshold){
		MapFilter<String, Integer> mapFilter = new MapFilter<String, Integer>();
		mapFilter.addFilterCriteria(new FilterCriteria<Integer>(){
		    public boolean passes(Integer nbTerms){
		        return (nbTerms >= threshold);
		    }
		});
		return mapFilter.filterCopy(nbTermsAnchored);
	}
		
	public Set<String> getFirstOntologies(final Map<String, Integer> nbAnchorsInOntology, Collection<String> selection, int nbMaxOntos) {
		Set<String> result = new HashSet<String>(selection);
		if (result.size()<=nbMaxOntos){
			return result;
		} else {
			List<String> sortOntos = new ArrayList<String>(selection);
			Collections.sort(sortOntos, new Comparator<String>(){
				//@Override
				public int compare(String onto1, String onto2) {
					return (nbAnchorsInOntology.get(onto2).compareTo(nbAnchorsInOntology.get(onto1)));
				}
			});
			result.retainAll(sortOntos.subList(0, nbMaxOntos));
			return result;	
		}
	}
	
	
	public Set<String> getFirstRandomOntologies(Collection<String> selection, int maxOnto){
		Set<String> result = new HashSet<String>(selection);
		List<String> sortOntos = new ArrayList<String>(selection);
		result.retainAll(sortOntos.subList(0, maxOnto));
		return result;
	}
	
	
	
	public Map<String, Integer> getOntoAnchoresOnto1() {
		return ontoAnchoresOnto1;
	}

	public Map<String, Integer> getOntoAnchoresOnto2() {
		return ontoAnchoresOnto2;
	}

	public Map<String, Integer> getOntoAnchored() {
		return ontoAnchored;
	}
	
	public ConfigurationBean getConfigurationBean(){
		return this.conf;
	}
	
	public CtxtMatchingTask getCtxtMatchingTask(){
		return this.tsk;
	}
	
	
}
