package fr.inrialpes.exmo.scarlet.matcher;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;
import org.semanticweb.owl.align.AlignmentVisitor;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Parameters;
import org.semanticweb.owl.align.Relation;
import org.xml.sax.ContentHandler;

import fr.inrialpes.exmo.align.impl.URIAlignment;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntologyFactory;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
//import fr.inrialpes.exmo.align.onto.LoadedOntology;
//import fr.inrialpes.exmo.align.onto.OntologyFactory;
import fr.inrialpes.exmo.scarlet.matcher.conf.Configurable;
import fr.inrialpes.exmo.scarlet.matcher.conf.Parameter;
import fr.inrialpes.exmo.scarlet.matcher.conf.ParameterCombination;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;

/**
 * @author Patrick HOFFMANN
 * @responsible for
 *
 */
public abstract class AbstractAlignmentProcess implements AlignmentProcess{

	static final public String PARAMETER_URI = "http://scarlet.open.ac.uk/annotation/parameter#";
	static final public String MATCHER_URI = "http://scarlet.open.ac.uk/annotation/matcher#";
	public static final String EVALUATION_URI = "http://scarlet.open.ac.uk/annotation/evaluation#";

	static final public String MATHER_ANNOT_NAME_LABEL = "name";
	static final public String MATHER_ANNOT_DESCRIPTION_LABEL = "description";

	protected URIAlignment underlyingAlignment = new URIAlignment();

	protected LoadedOntology<?> ontology1;
	protected LoadedOntology<?> ontology2;

	public AbstractAlignmentProcess(){
	}

	public AbstractAlignmentProcess(URIAlignment a){
		this.underlyingAlignment = a;
	}

	//@Override
	public abstract void align(Alignment arg0, Parameters arg1)	throws AlignmentException;


	protected void recordMatcherNameAndDescription(Configurable config) {
		this.setExtension(PARAMETER_URI, MATHER_ANNOT_NAME_LABEL, config.getMatcherName());
		this.setExtension(PARAMETER_URI, MATHER_ANNOT_DESCRIPTION_LABEL, config.getMatcherDescription());
	}

	protected void recordParametersAsExtensions(ParameterCombination params, Collection<Parameter> parametersDefined){
		for(Parameter param: parametersDefined){
			this.setExtension(PARAMETER_URI, param.name(),params.getParameter(param).toString());
			this.getExtensions();
		}
	}


	protected String printRelations(List<Relation> relations, URI cpt1, URI cpt2) throws AlignmentException {
		if ((relations==null)|(relations.isEmpty())){
			return "No relation found between " + cpt1.getFragment() + " and " + cpt2.getFragment() ;
		} else {
			StringBuffer tmp = new StringBuffer();
			tmp.append("Relations between " + cpt1.getFragment() + " and " + cpt2.getFragment()  + " : ");
			for(Relation r: relations){
				if (r==null){
					tmp.append("No relation" + ", ");
				} else {
					tmp.append(RelationEnum.toRelation(r).name() + ", ");
				}
			}
			return tmp.toString();
		}
	}


	protected Collection<URI> getOntologyConcepts(LoadedOntology<?> onto) throws AlignmentException, OntowrapException {
		Collection<URI> result = new ArrayList<URI>();
		for (Object concept : onto.getClasses()) {
			result.add(onto.getEntityURI(concept));
		}
		return result;
	}
	//@Override
    public void init( Object ontoBiz1, Object ontoBiz2 ) throws AlignmentException {
		underlyingAlignment.init(ontoBiz1, ontoBiz2);
		OntologyFactory factory = OntologyFactory.getFactory();
		try {
			this.ontology1 = factory.loadOntology( underlyingAlignment.getOntology1URI() );
		} catch (OntowrapException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			this.ontology2 = factory.loadOntology( underlyingAlignment.getOntology2URI());
		} catch (OntowrapException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	public final void initialize( final URI uri1, final URI uri2 ) throws AlignmentException, OntowrapException{
		OntologyFactory factory = OntologyFactory.getFactory();
		initialize(factory.loadOntology( uri1 ), factory.loadOntology( uri2 ));
	}

	public final void initialize( final LoadedOntology<?> onto1, final LoadedOntology<?> onto2 ){
	}


	public Object clone()  {
		return underlyingAlignment.clone();
	}

	//@Override
	public void accept(AlignmentVisitor arg0) throws AlignmentException {
		underlyingAlignment.accept(arg0);
	}

	//@Override
	public Cell addAlignCell(Object arg0, Object arg1)	throws AlignmentException {
		return underlyingAlignment.addAlignCell(arg0, arg1);
	}

	//@Override
	public Cell addAlignCell(Object arg0, Object arg1, String arg2, double arg3) throws AlignmentException {
		return underlyingAlignment.addAlignCell(arg0, arg1, arg2, arg3);
	}

	//@Override
	public Alignment compose(Alignment arg0) throws AlignmentException {
		return underlyingAlignment.compose(arg0);
		}

	//@Override
	public void cut(double arg0) throws AlignmentException {
		underlyingAlignment.cut(arg0);
	}

	//@Override
	public void cut(String arg0, double arg1) throws AlignmentException {
		underlyingAlignment.cut(arg0, arg1);
	}

	//@Override
	public Alignment diff(Alignment arg0) throws AlignmentException {
		return underlyingAlignment.diff(arg0);
	}

	//@Override
	public void dump(ContentHandler arg0) {
		underlyingAlignment.dump(arg0);
	}

	//@Override
	public Cell getAlignCell1(Object arg0) throws AlignmentException {
		return underlyingAlignment.getAlignCell1(arg0);
	}

	//@Override
	public Cell getAlignCell2(Object arg0) throws AlignmentException {
		return underlyingAlignment.getAlignCell2(arg0);
	}

	//@Override
	public Set<Cell> getAlignCells1(Object arg0) throws AlignmentException {
		return underlyingAlignment.getAlignCells1(arg0);
	}

	//@Override
	public Set<Cell> getAlignCells2(Object arg0) throws AlignmentException {
		return underlyingAlignment.getAlignCells2(arg0);
	}

	//@Override
	public void remCell(Cell arg0) throws AlignmentException {
		underlyingAlignment.remCell(arg0);
	}
	
	//@Override
	public Object getAlignedObject1(Object arg0) throws AlignmentException {
		return underlyingAlignment.getAlignedObject1(arg0);
	}

	//@Override
	public Object getAlignedObject2(Object arg0) throws AlignmentException {
		return underlyingAlignment.getAlignedObject2(arg0);
	}

	//@Override
	public Relation getAlignedRelation1(Object arg0) throws AlignmentException {
		return underlyingAlignment.getAlignedRelation1(arg0);
	}

	//@Override
	public Relation getAlignedRelation2(Object arg0) throws AlignmentException {
		return underlyingAlignment.getAlignedRelation1(arg0);
	}

	//@Override
	public double getAlignedStrength1(Object arg0) throws AlignmentException {
		return underlyingAlignment.getAlignedStrength1(arg0);
	}

	//@Override
	public double getAlignedStrength2(Object arg0) throws AlignmentException {
		return underlyingAlignment.getAlignedStrength2(arg0);
	}

	//@Override
	public Enumeration<Cell> getElements() {
		return underlyingAlignment.getElements();
	}

	//@Override
	public String getExtension(String arg0, String arg1) {
		return underlyingAlignment.getExtension(arg0, arg1);
	}

	//@Override
//	public Parameters getExtensions() {
//		return underlyingAlignment.getExtensions();
//	}
	
	public Collection<String[]> getExtensions() {
	return underlyingAlignment.getExtensions();
    }
	

	//@Override
	public URI getFile1() {
		return underlyingAlignment.getFile1();
	}

	//@Override
	public URI getFile2() {
		return underlyingAlignment.getFile2();
	}

	//@Override
	public String getLevel() {
		return underlyingAlignment.getLevel();
	}

	//@Override
	public Object getOntology1() {
		return underlyingAlignment.getOntology1();
	}

	//@Override
	public URI getOntology1URI() throws AlignmentException {
		return underlyingAlignment.getOntology1URI();
	}

	//@Override
	public Object getOntology2() {
		return underlyingAlignment.getOntology2();
	}

	//@Override
	public URI getOntology2URI() throws AlignmentException {
		return underlyingAlignment.getOntology2URI();
	}

	//@Override
	public String getType() {
		return underlyingAlignment.getType();
	}

	//@Override
	public void harden(double arg0) throws AlignmentException {
		underlyingAlignment.harden(arg0);

	}

	//@Override
	public void init(Object arg0, Object arg1, Object arg2) throws AlignmentException {
		underlyingAlignment.init( arg0, arg1, arg2);
	}

	//@Override
	public Alignment inverse() throws AlignmentException {
		return underlyingAlignment.inverse();
	}

	//@Override
	public Iterator<Cell> iterator() {
		return underlyingAlignment.iterator();
	}

	//@Override
	public Alignment join(Alignment arg0) throws AlignmentException {
		return underlyingAlignment.join(arg0);
	}

	//@Override
	public Alignment meet(Alignment arg0) throws AlignmentException {
		return underlyingAlignment.meet(arg0);
	}

	//@Override
	public int nbCells() {
		return underlyingAlignment.nbCells();
	}

	//@Override
	public void render(AlignmentVisitor arg0) throws AlignmentException {
		underlyingAlignment.render(arg0);

	}

	//@Override
	public void setExtension(String uri, String label, String value) {
		underlyingAlignment.setExtension(uri, label, value);

	}

	//@Override
	public void setFile1(URI arg0) {
		underlyingAlignment.setFile1(arg0);

	}

	//@Override
	public void setFile2(URI arg0) {
		underlyingAlignment.setFile2(arg0);

	}

	//@Override
	public void setLevel(String arg0) {
		underlyingAlignment.setLevel(arg0);

	}

	//@Override
	public void setOntology1(Object arg0) throws AlignmentException {
		underlyingAlignment.setOntology1(arg0);
	}

	//@Override
	public void setOntology2(Object arg0) throws AlignmentException {
		underlyingAlignment.setOntology2(arg0);
	}

	//@Override
	public void setType(String arg0) {
		underlyingAlignment.setType(arg0);
	}
	
}
