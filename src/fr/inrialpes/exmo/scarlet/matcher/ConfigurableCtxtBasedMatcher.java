package fr.inrialpes.exmo.scarlet.matcher;

import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import java.lang.reflect.Constructor;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;
import org.semanticweb.owl.align.AlignmentVisitor;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Parameters;
import org.semanticweb.owl.align.Relation;

import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.BasicParameters;
import fr.inrialpes.exmo.align.impl.URIAlignment;
import fr.inrialpes.exmo.align.impl.method.StringDistAlignment;
import fr.inrialpes.exmo.align.impl.rel.IncompatRelation;
import fr.inrialpes.exmo.align.impl.rel.NonTransitiveImplicationRelation;
import fr.inrialpes.exmo.align.impl.renderer.RDFRendererVisitor;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
//import fr.inrialpes.exmo.scarlet.concurrent.Immutable;
import fr.inrialpes.exmo.scarlet.context.ContextService;
import fr.inrialpes.exmo.scarlet.matcher.conf.Configurable;
import fr.inrialpes.exmo.scarlet.matcher.conf.Parameter;
import fr.inrialpes.exmo.scarlet.matcher.conf.ParameterCombination;
import fr.inrialpes.exmo.scarlet.matcher.task.CtxtMatchingTask;
import fr.inrialpes.exmo.scarlet.param.OptionParameter;
import fr.inrialpes.exmo.scarlet.param.method.AggregationMethod;
import fr.inrialpes.exmo.scarlet.param.method.CompositionMethod;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.Assertion;
import fr.inrialpes.exmo.scarlet.struct.ConceptPath;
import fr.inrialpes.exmo.scarlet.struct.ConfigurationBean;
import fr.inrialpes.exmo.scarlet.struct.OntologyFragment;
import fr.inrialpes.exmo.scarlet.struct.Pair;
import fr.inrialpes.exmo.scarlet.util.AlignmentHelper;
import fr.inrialpes.exmo.scarlet.util.ReflectionHelper;


/**
 * @author Patrick HOFFMANN
 *
 */
//@Immutable
public final class ConfigurableCtxtBasedMatcher extends AbstractAlignmentProcess implements AlignmentProcess {


	ConfigurationBean confOper;
	Configurable config;

	public ConfigurableCtxtBasedMatcher(){
	}


	@Override
	public void align(Alignment arg0, Properties arguments) throws AlignmentException {
		//log.debug(arguments.getNames());
		// Class.forName, HERE.
		
		this.config = setMatcherConfiguration(arguments);
		this.confOper = (ConfigurationBean)config.getConfiguredResource();
		recordMatcherNameAndDescription(config);

		ParameterCombination params = config.getParameterCombinationFromArgs(arguments);
		System.err.println(params.toString());
		params.run();
		recordParametersAsExtensions(params, config.getParameters());

		OntologyFragment<URI> ontologySource = null;
		try {
			ontologySource = new OntologyFragment<URI>(ontology1.getURI(), getOntologyConcepts(ontology1));
		} catch (OntowrapException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		OntologyFragment<URI> ontologyTarget = null;
		try {
			ontologyTarget = new OntologyFragment<URI>(ontology2.getURI(), getOntologyConcepts(ontology2));
		} catch (OntowrapException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		confOper.setSourceTargetOnto(ontologySource, ontologyTarget);
		
		// SNIPPET FOR CREATION
		String CSClassName = confOper.getCtxtMatchingStrategy().toString();
		ContextService cs = null;
		try {
		    // Create alignment object
			Class<?> CSClass = Class.forName( CSClassName );
		    Class[] cparams = {confOper.getClass()};
		    Constructor cSConstructor = CSClass.getConstructor(cparams);
		    Object[] mparams = {(Object) confOper};
		    cs = (ContextService)cSConstructor.newInstance(mparams);
		    cs.init( arguments );
		} catch ( Exception ex ) {
			System.err.println( "Cannot create context service {} "+CSClassName+"\n"
					   +ex.getMessage());
			//usage();
		    //throw ex;
		}
	
		
		CtxtMatchingTask alignTask = new CtxtMatchingTask(cs, confOper, ontologySource, ontologyTarget);
		alignTask.run();

		Map<Pair, Collection<ConceptPath>> paths = alignTask.getAlignment();

		//	compose all paths

		CompositionMethod compositionMethod = confOper.getCompositionMethod();

		Map<Pair, Collection<ConceptPath>> reviewedPaths = new HashMap<Pair, Collection<ConceptPath>>();
		for(Pair couple: paths.keySet()){
			Collection<ConceptPath> composed = new ArrayList<ConceptPath>();
			Collection<ConceptPath> composed2 = new ArrayList<ConceptPath>();
			for(ConceptPath path: paths.get(couple)){
				if(null != path){
					for(int cp=0; cp < path.getAssertions().size(); cp++){
						//System.out.println("Path assertion: " + path.getAssertions().get(cp));
					}
					path.setSummaryRelations(compositionMethod.compose(path));
					for(int r=0; r <   path.getSummaryRelations().size(); r++){
						//System.out.println("path relation " + path.getSummaryRelations().get(r));
					}
					composed.add(path);
				}
			}
			composed2 = composed;
			reviewedPaths.put(couple, composed2);

		}



		//STATISTICS FOR FOUND PATHS
		for(Pair couple: reviewedPaths.keySet()){
			if(null != couple){
				//System.out.println("Number of paths found for Pair: " + couple.source().toString() + " - "+ couple.target().toString() + " : " + reviewedPaths.get(couple).size());
				int len = 0;
				for(ConceptPath p : reviewedPaths.get(couple)){
					if(null != p){
						len = len + p.length();
						for(int cp=0; cp < p.getAssertions().size(); cp++){
							//System.out.println("Path assertion: " + p.getAssertions().get(cp));
						}
						for(int r=0; r <   p.getSummaryRelations().size(); r++){
							//System.out.println("path relation " + p.getSummaryRelations().get(r));
						}
					}
				}
				double averagePathLenths = len / reviewedPaths.get(couple).size();
					System.out.println("       Average Length of the paths: " + averagePathLenths);
			}
		}


		//Remove empty paths
	    //OFFICIAL BEHAVIOUR UNCOMMENT THESE 2 ROWS
		AggregationMethod aggregationMethod = confOper.getAggregationMethod();
		Map<Pair, Collection<ConceptPath>> aggregatedpaths = aggregationMethod.aggregate(reviewedPaths);

		List<Relation> summaryRelations  = new ArrayList<Relation>();
		for(Pair couple: aggregatedpaths.keySet()){
			for(ConceptPath connection: aggregatedpaths.get(couple)){
				//System.out.println(couple+ ", ");
				List<RelationEnum> compositionResult = connection.getSummaryRelations();
				if(null != compositionResult && compositionResult.size() != 0){
					summaryRelations.add(compositionResult.get(compositionResult.size()-1).toBasicRelation());
				}
				if(compositionResult.contains(RelationEnum.disjunctive)){
					//System.out.println("OTHER KIND OF SUMMARY RELATION PRODUCED BY LOGICAL DISJUNCTION "  + couple.source() + " , " + couple.target());
				}
				else{
					//System.out.println("SUMMARY RELATION EMPTY " + couple.source() + " , " + couple.target());
				}

			}
			try {
				// uncomment this comment the other
				//AlignmentHelper.updateAlignement(this.underlyingAlignment, new URI(couple.source()), new URI(couple.target()), summaryRelations);
				AlignmentHelper.updateAlignement(this.underlyingAlignment, new URI(couple.source()), new URI(couple.target()), summaryRelations);
			} catch (URISyntaxException e) {
				// TODO remove and replace by relation URI - String in CtxtMatchingTask or somewhere else!
				e.printStackTrace();
			}
			summaryRelations.clear();
		}
	}

	private Configurable setMatcherConfiguration(Properties arguments) {
		Object configuration = arguments.getProperty("conf");
		if (configuration!=null){
			return ReflectionHelper.initMatcherConfiguration((String)configuration);
		} else {
			return new CtxtMatcherConfiguration();
		}
	}
	
//	private Parameters convertToParam(Properties args){
//		Parameters param;
//		Enumeration<Object> pars = args.keys();
//		for(;pars.hasMoreElements();){
//			String name = (String)pars.nextElement();
//			String value = (String)args.get(name);
//			param.setParameter(name, value);
//		}
//		return param;
//	}
	

	public static String localName(String cpt){
		int idx = Math.max(cpt.indexOf("#"), cpt.lastIndexOf("/")) + 1;
		return cpt.substring(idx);
	}

	@Override
	public void align(Alignment arg0, Parameters arg1)
			throws AlignmentException {
		// TODO Auto-generated method stub
		
	}

}
