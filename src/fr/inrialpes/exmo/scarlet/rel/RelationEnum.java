package fr.inrialpes.exmo.scarlet.rel;


import org.semanticweb.owl.align.Relation;

import fr.inrialpes.exmo.scarlet.rel.BasicRelation;
//import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
//import fr.inrialpes.exmo.align.impl.rel.IncompatRelation;
//import fr.inrialpes.exmo.align.impl.rel.NonTransitiveImplicationRelation;
//import fr.inrialpes.exmo.align.impl.rel.SubsumeRelation;
//import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;

import fr.inrialpes.exmo.scarlet.rel.*;


/**
 * @author Patrick HOFFMANN
 * Enum aimed at translating Scarlet relations to the Relation of the Alignment API
 */
public enum RelationEnum {
	subClass {public BasicRelation toBasicRelation(){return new SubsumedRelation();}
			  public RelationEnum inverse() {return superClass;}},
	superClass {public BasicRelation toBasicRelation(){return new SubsumeRelation();}
			    public RelationEnum inverse() {return subClass;}},
	disjoint {public BasicRelation toBasicRelation(){return new IncompatRelation();}
				public RelationEnum inverse() {return disjoint;}},
	named {public BasicRelation toBasicRelation(){return null;}
			public RelationEnum inverse() {	return named;
	}},
	equiv {public BasicRelation toBasicRelation(){return new EquivRelation();}
			public RelationEnum inverse() {return equiv;
	}},
	nontransitiveimplication {public BasicRelation toBasicRelation(){return new NonTransitiveImplicationRelation();}
							public RelationEnum inverse() {return nontransitiveimplication;}},
	
	//from here reversible changes...
	//overlaps {public BasicRelation toBasicRelation(){return null;}
	//				public RelationEnum inverse() {return overlaps;}},
	//doNotKnow {public BasicRelation toBasicRelation(){return null;}	
	//			public RelationEnum inverse() {	return doNotKnow;}}
	notIncompatible	{public BasicRelation toBasicRelation(){return new NotIncompatibleRelation();}
					public RelationEnum inverse() {return notIncompatible;}},					
	overlaps {public BasicRelation toBasicRelation(){return new OverlapRelation();}
					public RelationEnum inverse() {return overlaps;}}, //inverse of overlap ?
	notSubsumed {public BasicRelation toBasicRelation(){return new NotSubsumedRelation();}
					public RelationEnum inverse() {return notSubsumes;}}, //inverse ?
	notSubsumes {public BasicRelation toBasicRelation(){return new NotSubsumedRelation();}
					public RelationEnum inverse() {return notSubsumed;}}, // inverse ?					
	subsumesOverlaps {public BasicRelation toBasicRelation(){return new SubsumeOverlapRelation();}
					public RelationEnum inverse() {return subsumedOverlaps;}}, // inverse ?
	subsumedOverlaps {public BasicRelation toBasicRelation(){return new SubsumedOverlapRelation();}
					public RelationEnum inverse() {return subsumesOverlaps;}}, // inverse?
	all {public BasicRelation toBasicRelation(){return new EveryRelation();}
					public RelationEnum inverse() {return all;}}, // inverse ?
	doNotKnow {public BasicRelation toBasicRelation(){return null;}	
				public RelationEnum inverse() {	return doNotKnow;}},
	disjunctive {public BasicRelation toBasicRelation(){return new DisjunctiveRelation();}	
	public RelationEnum inverse() {	return disjunctive;}}
	; 
	
	public abstract BasicRelation toBasicRelation();

	public abstract RelationEnum inverse();
	
	public static RelationEnum toRelation(String name){
		try {
			if (name.equals("subClassOf")){
				return RelationEnum.subClass;
			}
			if (name.equals("superClassOf")){
				return RelationEnum.superClass;
			}
            return valueOf(name);
        } 
        catch (Exception ex) {
            return named;
        }	
	}
	
	public static Relation translateBasicRelation(uk.ac.open.kmi.scarlet.data.Relation relation) {
		RelationEnum translation = RelationEnum.toRelation(relation.getName());
		return translation.toBasicRelation();
	}
	
	public static RelationEnum toRelation(uk.ac.open.kmi.scarlet.data.Relation relation){
		return RelationEnum.toRelation(relation.getName());
	}
	
	public static RelationEnum toRelation(Relation relation){
		try {
			String name = relation.getClass().getSimpleName();
			if (name.equals("EquivRelation")){
				return RelationEnum.equiv;
			}
			if (name.equals("IncompatRelation")){
				return RelationEnum.disjoint;
			}
			if (name.equals("NonTransitiveImplicationRelation")){
				return RelationEnum.nontransitiveimplication;
			}
			if (name.equals("SubsumedRelation")){
				return RelationEnum.subClass;
			}
			if (name.equals("SubsumeRelation")){
				return RelationEnum.superClass;
			}
			
			//from here reversible change...
			if (name.equals("NotIncompatibleRelation")){
				return RelationEnum.notIncompatible;
			}
			if (name.equals("OverlapRelation")){
				return RelationEnum.overlaps;
			}
			if (name.equals("NotSubsumedRelation")){
				return RelationEnum.notSubsumed;
			}
			if (name.equals("NotSubsumeRelation")){
				return RelationEnum.notSubsumes;
			}
			if (name.equals("SubsumeOverlapRelation")){
				return RelationEnum.subsumesOverlaps;
			}
			if (name.equals("SubsumedOverlapRelation")){
				return RelationEnum.subsumedOverlaps;
			}
			if (name.equals("EveryRelation")){
				return RelationEnum.all;
			}
			if (name.equals("DisjunctiveRelation")){
				return RelationEnum.disjunctive;
			}
		
		} 
		catch (Exception ex) {
			return named;
		}
		return null;	
	}

	public static Relation toSemanticWebRelation(RelationEnum summaryRelation) {
		return summaryRelation.toBasicRelation();
	}

}
