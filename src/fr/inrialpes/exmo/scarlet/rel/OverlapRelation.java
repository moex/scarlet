package fr.inrialpes.exmo.scarlet.rel;

import java.io.PrintWriter;

import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentVisitor;
import org.semanticweb.owl.align.Relation;

import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.IncompatRelation;
import fr.inrialpes.exmo.scarlet.rel.BasicRelation;

import java.io.PrintWriter;

public class OverlapRelation extends BasicRelation {
	
	public void accept( AlignmentVisitor visitor) throws AlignmentException {
        visitor.visit( this );
    }

    static final String prettyLabel = "()";

    /** Creation **/
    public OverlapRelation(){
	super(prettyLabel);
    }

    private static OverlapRelation instance = null;

    public static OverlapRelation getInstance() {
	if ( instance == null ) instance = new OverlapRelation();
	return instance;
    }

    /*public Relation compose(Relation r) {
    	if ( r.equals(this) || r instanceof EquivRelation )
    		return this;
    	else if ( r instanceof IncompatRelation )
    		return r;
    	return null;
    }*/

    public Relation inverse() {
	return OverlapRelation.getInstance();
    }

    public void write( PrintWriter writer ) {
        writer.print("()");
    }
    
    public String getPrettyLabel() {
    	return prettyLabel;
      }

}


