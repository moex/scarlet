package fr.inrialpes.exmo.scarlet.rel;

import java.io.PrintWriter;

import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentVisitor;
import org.semanticweb.owl.align.Relation;

import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.IncompatRelation;
import fr.inrialpes.exmo.scarlet.rel.BasicRelation;

import java.io.PrintWriter;

public class EveryRelation extends BasicRelation {
	
	public void accept( AlignmentVisitor visitor) throws AlignmentException {
        visitor.visit( this );
    }

    static final String prettyLabel = "<,>,(),=,%";

    /** Creation **/
    public EveryRelation(){
	super(prettyLabel);
    }

    private static EveryRelation instance = null;

    public static EveryRelation getInstance() {
	if ( instance == null ) instance = new EveryRelation();
	return instance;
    }

    /*public Relation compose(Relation r) {
    	if ( r.equals(this) || r instanceof EquivRelation )
    		return this;
    	else if ( r instanceof IncompatRelation )
    		return r;
    	return null;
    }*/

    public Relation inverse() {
	return EveryRelation.getInstance();
    }

    public void write( PrintWriter writer ) {
        writer.print("&lt;,&gt;,(),=,%");
    }

    public String getPrettyLabel() {
    	return prettyLabel;
      }
}


