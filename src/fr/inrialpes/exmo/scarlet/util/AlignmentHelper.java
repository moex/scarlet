package fr.inrialpes.exmo.scarlet.util;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;

import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;

public class AlignmentHelper {

	private AlignmentProcess aligner;

	public AlignmentHelper(AlignmentProcess aligner){
		this.aligner = aligner;
	}

	static public void updateAlignement(BasicAlignment aligner,  URI cpt1, URI cpt2, List<Relation> relations)throws AlignmentException {
		for(Relation r: relations){
			String id = generateCoupleId(cpt1, r, cpt2);
			if (r!=null){
				double relationStrength = evaluateRelationStrength();
				aligner.addAlignCell(id, cpt1, cpt2, r, relationStrength);
			}
		}
	}

	static public void clearAlignement(BasicAlignment aligner)throws AlignmentException {
		ArrayList<Cell> totcells = aligner.getArrayElements();
		for(Cell c1: totcells){
				aligner.removeAlignCell(c1);
			}
		}

	protected static String generateCoupleId(URI cpt1, Relation r, URI cpt2) {
		long currentTime = System.currentTimeMillis();
		String id = "#" + cpt1.getFragment() +  "-" + RelationEnum.toRelation(r).name() + "-" + cpt2.getFragment() + "-" + currentTime;
		return id;
	}

	protected static double evaluateRelationStrength() {
		double relationStrength = 1.0;		// TODO make it depend from the depth, other arguments?
		return relationStrength;
	}

}
