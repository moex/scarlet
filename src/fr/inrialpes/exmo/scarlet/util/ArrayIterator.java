package fr.inrialpes.exmo.scarlet.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ArrayIterator<X> implements Iterator<List<X>>{
	final private X[][] paramOptions;
	private int[] counters;
	private boolean done = false;
	
	/**
	 * @param paramOptions is an array that has at least one cardinal value; 
	 * 			 Cardinal values must be greater than or equal to 1.
	 * 
	 * 	The algorithm respects the order in which the cardinal values are given.
	 */
	public ArrayIterator(final X[][] paramOptions){
		if (!isValidInput(paramOptions)){
			throw new IllegalArgumentException();
		}
		this.paramOptions = paramOptions;
		counters = new int[paramOptions.length];
		reset();
	}
	
	private boolean isValidInput(final X[][] paramOptions){
		for(int i=0;i<paramOptions.length;i++){
			if (paramOptions[i].length<1){
				return false;
			}
		}
		return true;
	}
	
	public void reset() {
		for(int i=0;i<counters.length;i++){
			counters[i] = 0;
		}
		done = false;
	}
		
	//@Override
	public boolean hasNext() {
		return(!done);
	}

	//@Override
	public List<X> next() {
		List<X> result = translate(counters);
		updateCounters();
		return result;
	}

	private void updateCounters() {
		int index=0;
		for(;index<counters.length;index++){
			if (counters[index]==paramOptions[index].length-1){
				counters[index] = 0;
			} else {
				counters[index] += 1;
				break;
			}
		}
		if (index==counters.length){
			done=true;
		}		
	}

	private List<X> translate(int[] counters) {
		List<X> result = new ArrayList<X>(paramOptions.length);
		for(int i=0;i<paramOptions.length;i++){
			result.add(paramOptions[i][counters[i]]);
		}
		return result;
	}

	//@Override
	public void remove() {
		throw new UnsupportedOperationException();
		
	}
}