package fr.inrialpes.exmo.scarlet.util;

import java.lang.reflect.Constructor;

import org.apache.log4j.Logger;
import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentProcess;
import org.semanticweb.owl.align.Evaluator;

import fr.inrialpes.exmo.scarlet.matcher.conf.Configurable;
//import fr.inrialpes.exmo.scarlet.matcher.conf.ConfigurablePathSearcher;

public class ReflectionHelper {
	
	static Logger log = Logger.getLogger("fr.inrialpes.exmo.scarlet"); 
	
	static public Evaluator initEvaluator(String evaluator, Alignment testAlignment, Alignment refAlignment) {
		try {
			Class evaluatorClass = Class.forName(evaluator);
			Constructor<Evaluator> constructor = evaluatorClass.getConstructor(
					Class.forName("org.semanticweb.owl.align.Alignment"),
					Class.forName("org.semanticweb.owl.align.Alignment"));
			return constructor.newInstance(refAlignment, testAlignment); // the first is the ref. one!		
		} catch (ClassNotFoundException e) {
			log.warn("Parameter evaluator is not correct: " + evaluator);
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

//	static public ConfigurablePathSearcher initPathSearcher(String pathSearcher) {
//		try {
//			Class searcherClass = Class.forName(pathSearcher);
//			ConfigurablePathSearcher searcher = (ConfigurablePathSearcher) searcherClass.newInstance();
//			return searcher;
//		} catch (ClassNotFoundException e) {
//			log.warn("Parameter pathSearcher is not correct: " + pathSearcher);
//			e.printStackTrace();
//		} catch (InstantiationException e) {
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
	


	static public AlignmentProcess initMatcher(String matcherName) {
		try {
			Class matcherClass = Class.forName(matcherName);
			AlignmentProcess matcher = (AlignmentProcess) matcherClass.newInstance();
			return matcher;
		} catch (ClassNotFoundException e) {
			log.warn("Parameter impl (matcher name) is not correct: " + matcherName);
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}


	public static Configurable initMatcherConfiguration(String matcherConfName) {
		try {
			Class matcherClass = Class.forName(matcherConfName);
			Configurable config = (Configurable) matcherClass.newInstance();

//			Class matcherClass = Class.forName(matcherConfName);
//			Configurable config = (Configurable) matcherClass.getConstructor(ConfiguredOperationsFacade.class).newInstance(matcher); //newInstance();
			return config;
		} catch (ClassNotFoundException e) {
			log.warn("Parameter impl (matcher configuration name) is not correct: " + matcherConfName);
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
