package fr.inrialpes.exmo.scarlet.util;

import java.util.Collection;
import java.util.Iterator;

public class CombinedIterator<X,Y> implements Iterator<Object[]> {

	private Iterator<X> x;
	private Iterator<Y> y;
	
	private X xCurrent;
	private Y yCurrent;
	private Collection<Y> yModel;
	private boolean started = false;
	
	public CombinedIterator(Iterator<X> x, Collection<Y> yModel){
		this.x = x;
		this.yModel = yModel;
		this.y = yModel.iterator();
	}
	
	//@Override
	public boolean hasNext() {
		return (x.hasNext() | y.hasNext());
	}

	private Object[] prepareResult(X xResult, Y yResult){
		Object[] result = new Object[2];
		result[0] = xResult;
		result[1] = yResult;
		return result;
	}
			
	//@Override
	public Object[] next() {
		if (!started){
			started=true;
			xCurrent = x.next();
		}
		if (y.hasNext()){
			yCurrent = y.next();
		} else {
			xCurrent = x.next();
			y = yModel.iterator();
			yCurrent = y.next();
		}	
		return prepareResult(xCurrent, yCurrent);
	}

	//@Override
	public void remove() {
		throw new UnsupportedOperationException();
		
	}

}
