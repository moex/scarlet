package fr.inrialpes.exmo.scarlet.util.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

@SuppressWarnings("serial")
public class MapFilter<X, Y> implements  java.io.Serializable {

    protected GenericFilter<Y> filter;
    
    
    public MapFilter(){
    	filter = new GenericFilter<Y>();
    }
    
    /**
     * Adds a FilterCriteria to be used by the filter
     * @param filterCriteria
     */
    public void addFilterCriteria(FilterCriteria<Y> filterCriteria){
        filter.addFilterCriteria(filterCriteria);
    }

	  /**
     * Starts the filtering process. For each object in the collection,
     * all FilterCriteria are called. Only if the object passess
     * all FilterCriteria it remains in the collection. Otherwise, it is removed.
     * @param map
     */
    public void filter(Map<X, Y> map){
        if(map != null){
            for(X x: map.keySet()){
            	if (!filter.passesAllCriteria(map.get(x))){
                    map.remove(x);
            	}
            }
        }
    }
      
    public Collection<X> filterCopy(Map<X, Y> map){
    	Collection<X> result = new ArrayList<X>(map.keySet());
    	if(map != null){
            for(X x: map.keySet()){
            	if (!filter.passesAllCriteria(map.get(x))){
                    result.remove(x);
            	}
            }
        }
    	return result;
    }

}
