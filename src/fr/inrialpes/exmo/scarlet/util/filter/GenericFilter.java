package fr.inrialpes.exmo.scarlet.util.filter;

import java.lang.reflect.*;
import java.util.*;



/**
 * From <a href="http://www.javaworld.com/javaworld/jw-10-2004/jw-1018-filter.html?page=1">javaWorld</a>
 * <p>Title: CollectionFilter</p>
 * <p>Description: </p>
 * @author David Rappoport
 * @version 1.0
 * @author Patrick Hoffmann: adapted with generics, and make it work for maps as well
 */

@SuppressWarnings("serial")
public class GenericFilter<X> implements java.io.Serializable {

    protected ArrayList<FilterCriteria<X>> allFilterCriteria = new ArrayList<FilterCriteria<X>>();

    /**
     * Adds a FilterCriteria to be used by the filter
     * @param filterCriteria
     */
    public void addFilterCriteria(FilterCriteria<X> filterCriteria){
        allFilterCriteria.add(filterCriteria);
    }

    /**
     * Makes sure the specified object passes all FilterCriteria's passes method.
     * @param o
     * @return
     */
    protected boolean passesAllCriteria(X o){
        for(int i = 0; i < allFilterCriteria.size(); i ++){
            FilterCriteria<X> filterCriteria = (FilterCriteria<X>)allFilterCriteria.get(i);
            if(!filterCriteria.passes(o)){
                return false;
            }
        }
        return true;
    }

}
