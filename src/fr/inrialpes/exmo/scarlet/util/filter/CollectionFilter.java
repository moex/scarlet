package fr.inrialpes.exmo.scarlet.util.filter;

import java.lang.reflect.*;
import java.util.*;



/**
 * From <a href="http://www.javaworld.com/javaworld/jw-10-2004/jw-1018-filter.html?page=1">javaWorld</a>
 * <p>Title: CollectionFilter</p>
 * <p>Description: </p>
 * @author David Rappoport
 * @version 1.0
 * @author Patrick Hoffmann: adapted with generics, and make it work for maps as well
 */

@SuppressWarnings("serial")
public class CollectionFilter<X> implements java.io.Serializable {
	
	 protected GenericFilter<X> filter;
	    
	 public CollectionFilter(){
	    	filter = new GenericFilter<X>();
	    }
	    
    /**
     * Adds a FilterCriteria to be used by the filter
     * @param filterCriteria
     */
    public void addFilterCriteria(FilterCriteria<X> filterCriteria){
    	filter.addFilterCriteria(filterCriteria);
    }

    /**
     * Starts the filtering process. For each object in the collection,
     * all FilterCriteria are called. Only if the object passes
     * all FilterCriteria it remains in the collection. Otherwise, it is removed.
     * @param collection
     */
    public void filter(Collection<X> collection){
        if(collection != null){
            Iterator<X> iter = collection.iterator();
            while(iter.hasNext()){
                X o = iter.next();
                if(!filter.passesAllCriteria(o)){
                    iter.remove();
                }
            }
        }
    }

    /**
     * This method does the same as the filter method. However, a copy of
     * the original collection is created and filtered. The original collection
     * remains unchanged and the copy is returned. Only use this method
     * for collection classes that define a default constructor
     * @param inputCollection
     * @return a filtered copy of the input collection
     */
    public Collection<X> filterCopy(Collection<X> inputCollection){
        Collection<X> outputCollection = null;

        if(inputCollection != null){
            outputCollection =  new ArrayList<X>();
            outputCollection.addAll(inputCollection);

            Iterator<X> iter = outputCollection.iterator();
            while(iter.hasNext()){
                X o = iter.next();
                if(!filter.passesAllCriteria(o)){
                    iter.remove();
                }
            }
        }
        return outputCollection;
    }
}
