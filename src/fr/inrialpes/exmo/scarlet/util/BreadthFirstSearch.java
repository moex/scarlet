package fr.inrialpes.exmo.scarlet.util;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import fr.inrialpes.exmo.scarlet.context.ContextService;
import fr.inrialpes.exmo.scarlet.matcher.task.ExplorationTask;
import fr.inrialpes.exmo.scarlet.param.InstanceRestrictable;
import fr.inrialpes.exmo.scarlet.param.OptionParameter;
import fr.inrialpes.exmo.scarlet.rel.RelationEnum;
import fr.inrialpes.exmo.scarlet.struct.Assertion;
import fr.inrialpes.exmo.scarlet.struct.ConceptPath;
import fr.inrialpes.exmo.scarlet.struct.Pair;

public class BreadthFirstSearch implements Iterator<Collection<String>>, Cloneable{

	String ontology;
	String fragmentOnto;
	final Queue<String> conceptsToExplore;
	ExplorationTask explorer;
		
	private InstanceRestrictable<OptionParameter> parameters;
	// String: any concept; 2nd string: original concept; double: distance
	Map<String, Map<String, Double>> distance; 
	Map<String, Map<String, Assertion<String>>> vertices;
	Map<String, Set<Assertion<String>>> savedAssertions = new HashMap<String, Set<Assertion<String>>>();
	
	Map<String, Boolean> color; // null = white, false = grey, true = black
	
	public BreadthFirstSearch(ExplorationTask explorer, Collection<String> sourceConcepts, String ontology){
		this.ontology = ontology;
		this.explorer = explorer;
		conceptsToExplore = new LinkedBlockingQueue<String>(sourceConcepts); // will be the source after we switch
		distance = new HashMap<String, Map<String, Double>>();
		color = new HashMap<String, Boolean>();
		vertices = new HashMap<String, Map<String, Assertion<String>>>();

		init(sourceConcepts);
	}
	
	
	private void saveAssertion(String currentconcept, Assertion a){
		Set<Assertion<String>> ass = new HashSet<Assertion<String>>();
		boolean already = false;
		if(null != savedAssertions.get(currentconcept)){
			Set<Assertion<String>>  alreadythere = savedAssertions.get(currentconcept);
			for(Assertion<String> at : alreadythere){
				if(at.getDomain().toString().equalsIgnoreCase(a.getDomain().toString()) && at.getRange().toString().equalsIgnoreCase(a.getRange().toString())){
					already=true;
				}
			}
			if(!already){
				savedAssertions.get(currentconcept).add(a);
			}
			}
		else {
			ass.add(a);
			savedAssertions.put(currentconcept, ass);
		}
	}
	
	public Map<String, Set<Assertion<String>>> getSavedAssertions(){
		return savedAssertions;
	}
	
	public Map<String, Set<Assertion<String>>> invertAssertions(){
		
		
		return savedAssertions;
	}
	
	
	public void exploreOneLevelFurther(){
		Queue<String> result = exploreOneLevelFurther(conceptsToExplore);
		conceptsToExplore.addAll(result);
	}
	
	private Queue<String> exploreOneLevelFurther(Queue<String> input){
		Queue<String> nextInput = new LinkedBlockingQueue<String>();
		RelationEnum[] restriction = this.explorer.getCtxtService().getRelationRestriction(); //(RelationEnum[])get(OptionParameter.EXPLORE_RELATIONTYPE);
		while(!input.isEmpty()){
			String currentConcept = input.poll();
			Map<String, Assertion<String>> neighbors = new HashMap<String, Assertion<String>>();
			try {
				neighbors = this.explorer.getCtxtService().exploreNeighbors(currentConcept, ontology, restriction);
				for(String key : neighbors.keySet()){
					Assertion<String> a = neighbors.get(key);
					saveAssertion(currentConcept, a);
				}
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//.getNeighbors(currentConcept, ontology);
			
			for(String concept: neighbors.keySet()){
				if (!color.containsKey(concept)){ // colour = white
					nextInput.add(concept);
					color.put(concept, false);
					vertices.put(concept, updateVertices(currentConcept, neighbors.get(concept)));
					distance.put(concept, updateDistances(currentConcept, neighbors.get(concept).getRelation()));
				} else {
					if (!color.get(concept)){ // colour = grey
						// update vertices where smaller distance to original concept
						for(String origConcept: distance.get(concept).keySet()){
							if ((distance.get(currentConcept).containsKey(origConcept))&(neighbors.containsKey(concept))){
								//System.out.println("breadth first search: edit when grey"); // to make sure it happens sometimes!!
								Double newDistanceValue = distance.get(currentConcept).get(origConcept)+this.explorer.weight(neighbors.get(concept).getRelation());
								if (newDistanceValue < distance.get(concept).get(origConcept)){
									distance.get(concept).put(origConcept, newDistanceValue); // replace value
									vertices.get(concept).put(origConcept, neighbors.get(concept));
								}
//								else{
//									nextInput.add(concept);
//									vertices.put(concept, updateVertices(currentConcept, neighbors.get(concept)));
//									distance.put(concept, updateDistances(currentConcept, neighbors.get(concept).getRelation()));
//								}
							}
							else{
								nextInput.add(concept);
								vertices.put(concept, updateVertices(currentConcept, neighbors.get(concept)));
								distance.put(concept, updateDistances(currentConcept, neighbors.get(concept).getRelation()));
							}
						}
					}
				}
			}
			color.put(currentConcept, true);
		}
//		for(String verticeKey : vertices.keySet()){
//			System.out.println("printing vertice key " + localName(verticeKey));
//			Map<String, Assertion<String>> mapofVerticeKey = vertices.get(verticeKey);
//			for(String a : mapofVerticeKey.keySet()){
//				System.out.print(" key of verticeKeyMap " + localName(a) + " ");
//				Assertion<String> ass = mapofVerticeKey.get(a);
//				System.out.println("Assertion " + localName(ass.getDomain()) + " " + localName(ass.getRange()) + " " + ass.getRelation().name());
//			}
//		}
		return nextInput;
	}
	
	private Map<String, Assertion<String>> updateVertices(String concept, Assertion<String> assertion){
		Map<String, Assertion<String>> map = new HashMap<String, Assertion<String>>();
		if (vertices.containsKey(concept)){
			for(String origConcept: vertices.get(concept).keySet()){
				map.put(origConcept,assertion);
			}
		}
		return map;
	}
	
	private Map<String, Double> updateDistances(String concept, RelationEnum rel){
		Map<String, Double> value = new HashMap<String, Double>();
		if (distance.containsKey(concept)){
			for(String originalConcept: distance.get(concept).keySet()){
				value.put(originalConcept, distance.get(concept).get(originalConcept)+ this.explorer.weight(rel));
			}
		}
		return value;
	}
	
	public Set<String> getOriginalConceptsRelatedTo(Collection<String> concepts){
		Set<String> result = new HashSet<String>();
		for(String concept: concepts){
			if(vertices.containsKey(concept)){
				for(String orig: vertices.get(concept).keySet()){
					result.add(orig);
				}
			} // else nothing
		}
		return result;
	}
	

	
	/**
	 * @param concepts
	 * @return
	 */
	public Map<Pair, ConceptPath> getPathsToOriginToCustomConcepts(Collection<String> concepts, String onto, Map<String, String> jo1){
		Map<Pair, ConceptPath> result = new HashMap<Pair, ConceptPath>();
		for(String concept: concepts){
				if(null == onto){
				assert(vertices.containsKey(concept));
				Map<String, Assertion<String>> conceptAssertions = vertices.get(concept);
				for(String source: conceptAssertions.keySet()){
						Pair couple = new Pair(source, concept);
						result.put(couple, buildPath(source, concept, null));
				}
			}
			else{
				String conccompl = jo1.get(concept);
				assert(vertices.containsKey(conccompl));
				Map<String, Assertion<String>> conceptAssertions = vertices.get(conccompl);
				if(null != conceptAssertions){
					for(String source: conceptAssertions.keySet()){
							Pair couple = new Pair(source, conccompl);
							result.put(couple, buildPath(source, conccompl, onto));
					}
				}
			}
		}
		return result;
	}
	
	
	
	/**
	 * We only need to remove concepts for the next level search (others are not considered anymore)
	 * To do this, we need to know which original concepts have been found
	 * 
	 * @param origConcepts concepts for which it has been decided that there is no need to explore any longer
	 */
	public void stopSearchingPathForConcepts(Collection<String> origConcepts){
		for(String origConcept: origConcepts){
			for(String concept: conceptsToExplore){
				//assert(vertices.containsKey(concept));
				vertices.get(concept).remove(origConcept); // if does not contain concept, it is ignored
				distance.get(concept).remove(origConcept); // if does not contain concept, it is ignored
				
				if (vertices.get(concept).isEmpty()){ 
					conceptsToExplore.remove(concept); // nothing left to do
				}
			}
		}
	}
	
	private ConceptPath buildPath(String source, String concept, String onto){
		List<Assertion<String>> path = new LinkedList<Assertion<String>>();
		Assertion<String> assertion = null;
		String intermConcept = concept;
		if(null == onto){
			//while(!intermConcept.equals(source)){ // so as not to include the first relation, which is made up!
				assertion = vertices.get(intermConcept).get(source);
				//if(!assertion.getDomain().equalsIgnoreCase(assertion.getRange())){
					path.add(0,assertion);
					intermConcept = assertion.getDomain();
				//}///?????
			//}
		}
		else
		{
			//while(null == assertion || !assertion.equals(vertices.get(intermConcept).get(source))){ // so as not to include the first relation, which is made up!
			while(null == assertion || !intermConcept.equals(source)){
				assertion = vertices.get(intermConcept).get(source);
				//if(!assertion.getDomain().equalsIgnoreCase(assertion.getRange())){
					//Assertion<String> tempAss = assertion;
					path.add(0,assertion);
					intermConcept = assertion.getDomain();
				//}
			}
		}
		return new ConceptPath(path);
	}
	

	private void init(Collection<String> sourceConcepts){
		
		for(String concept: sourceConcepts){
			color.put(concept, false);

			Map<String, Assertion<String>> assertion = new HashMap<String, Assertion<String>>();
			assertion.put(concept, new Assertion<String>(RelationEnum.equiv, concept, concept));
			vertices.put(concept, assertion);
			
			Map<String, Double> value = new HashMap<String, Double>();
			value.put(concept, 0.0);
			distance.put(concept, value);
			String prev = localRoot(concept);
			this.fragmentOnto = prev;
		}
	}
	
	public String localRoot(String concept){
		String prev = null;
		if(-1 == concept.lastIndexOf('#')){
			prev = concept.substring(0, concept.lastIndexOf('/'));
		}
		else{
			prev = concept.substring(0, concept.lastIndexOf('#'));
		}
		return prev;
	}
	
	public String localName(String concept){
		String prev = null;
		if(-1 == concept.lastIndexOf('#')){
			prev = concept.substring(concept.lastIndexOf('/')+1);
		}
		else{
			prev = concept.substring(concept.lastIndexOf('#')+1);
		}
		return prev;
	}

	//@Override
	public boolean hasNext() {
		return (!conceptsToExplore.isEmpty());
	}

	//@Override
	public Collection<String> next() {
		Queue<String> result = this.exploreOneLevelFurther(conceptsToExplore);
		conceptsToExplore.addAll(result);
		return new ArrayList<String>(conceptsToExplore);
	}

	//@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	public Collection<String> peek() {
		return new ArrayList<String>(conceptsToExplore);
	}
	
	public String getOntology(){
		return ontology;
	}
	public String getFragmentOnto(){
		return fragmentOnto;
	}
	
	public Object get(OptionParameter option) throws UnsupportedOperationException {
		return parameters.get(option);
	}
	
	public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
