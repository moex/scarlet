package fr.inrialpes.exmo.scarlet.util;

import java.util.Iterator;

public class EmptyIterator<X> implements Iterator<X> {

	//@Override
	public boolean hasNext() {
		return false;
	}

	//@Override
	public X next() {
		return null;
	}

	//@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
