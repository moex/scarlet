package uk.ac.open.kmi.scarlet;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Vector;

import org.apache.log4j.Logger;

import uk.ac.open.kmi.scarlet.data.Path;
import uk.ac.open.kmi.scarlet.data.Relation;
import uk.ac.open.kmi.scarlet.utils.Utilities;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonCachedService;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonOntologyProxy;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonService;

/**
 * Provides the main algorithms for discovering relations between two given terms by combining relations from two or more ontologies.
 * 
 *It implements strategy S2 of the Scarlet methodology.
 * 
 * 
 * @author rms364
 * @version 1.1
 */
public class RelationComposer {

	static Logger log = Logger.getLogger("uk.ac.open.kmi.scarlet");
		
	//PARAMETERS
	//true if a single relation is returned (the one that is found first)
	//false if we wish to return all relations that can be found with the given configuration
	private boolean stopAtFirstRelation; 
	
	//checkX - these are set true to indicate that the corresponding relations should be tested for;
	private boolean checkParents;
	private boolean checkChildren;
	private boolean checkDisjoint;
	private boolean checkRelsTo;
	private boolean checkRelsFrom;
	private int depth;
	
	//GLOBALS
	//Warson Related + Utilities
	/*
	private EntitySearch es;
	private Utilities utils = new Utilities();
	private OntologyFinder of = new OntologyFinder();
	*/

	private WatsonService watsonService;
	
	private RelationFinder rf = new RelationFinder();
	
	
	//CACHES
	private Vector<String> wasCalled = new Vector<String>();
	private Vector<String> wasCalledChild = new Vector<String>();
	
	public RelationComposer(){
		/*
		EntitySearchServiceLocator locator = new EntitySearchServiceLocator();
		try{
			es = locator.getUrnEntitySearch();			
		}
		catch (Exception e) {
			e.printStackTrace();
			//log.info(e.getCause());
		}
		*/

		//try {
			watsonService = WatsonCachedService.getInstance();
		/*} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		//setting the default values
		this.setStopAtFirstRelation(true);
		this.setCheckForAllRelationTypes(true);
		
		//setting the relation finder defaults
		rf.setCheckForInheritedRelations(true);
		rf.setDepth(1);
	}
	
	public static void main(String[] args) throws RemoteException {
		RelationComposer app = new RelationComposer();
		Vector<Relation> rel;
		rel = app.findComposedRelationBetweenTerms("Age+Group", "Elderly");
		for (Relation r: rel){
			log.info(r.toString());
		}
	}
	
	/**
	 * Finds relations between two given terms. This is the main method of this class. 
	 * 
	 * 
	 * Its functionality depends on a set of parameters, which need to be set before the actual calling.
	 * The main parameters refer to:
	 * 
	 * <ul>
	 * <li> the number of relations to be retrieved - the algorithm can either stop as soon as a relation is found, 
	 * or continue until all possible relations are found. By default, it will stop at the first identified relation. 
	 * This parameter can be modified with the {@link setStopAtFirstRelation} method. </li>
	 * <li> the type of the discovered relations - the algorithm can search for various types of relations. 
	 * By default, it will search for all the relation types. Using the "setCheckForXXXRelation" methods, the 
	 * algorithm can be instructed to search for certain relation types only.</li>
	 * <li> </li>
	 * </ul>
	 * 
	 * @param source  the source term
	 * @param target  the target term
	 * @return a Vector of the {@link Relation} objects, each containing the details of a relation found between the two input terms
	 * @throws RemoteException
	 */
	public Vector<Relation> findComposedRelationBetweenTerms(String source, String target) {
		Vector<Relation> result = new Vector<Relation>();
		Collection<String> res;

		try{
			res = watsonService.getOntosForTerm(source);

			if (res.size() > 0) {
				if (this.stopAtFirstRelation){
					for(String ontology: res){
						log.info("Onto " + ontology);
						result = this.findComposedRelationInThisOntology(ontology, source, target);
						if (result.size() > 0) {
							break;
						}
						//for (Relation r : result ) {
						//log.info(" FOUND " + r.print() +  " In ontology " + onto);
						//log.info(" ======================================= ");
						//log.info();
						//}
					}						
				} else {
					for (String s : res) {
						result.addAll(this.findComposedRelationInThisOntology(s, source, target));
						//for (Relation r : tempResult) {
						//log.info(" FOUND " + r.toString() + " In ontology " + s);
						//log.info(" ======================================= ");
						//log.info();
						//}
					}
				}
			} else {
				log.info("NO ontologies for " + source);
			}

		} catch (Exception exc) {
			exc.printStackTrace();
		}

		//log.info("==================ALL Result PATHS ================");
		//for (Relation r : result ) {
		//log.info(r.toString() + "  because  ");
		//log.info(r.getPath().toString());

		//}

		return  result;
	}
	
	public Vector<Relation> findComposedRelationInThisOntology(String onto, String source, String target) throws RemoteException {
		Collection<String> urls = watsonService.getOntology(onto).getConceptBasedOnKeyword(source);
		Vector<Relation> result = new Vector<Relation>();
		Vector<Relation> tempResult;
		
			for(String sourceUrl: urls){
				String path = onto + "," + sourceUrl;
				//for all parents
				tempResult = findRelationForParents(onto,sourceUrl,target, this.depth, path);
				
				if (tempResult.size() > 0) {
					result.addAll(tempResult);
					if (this.stopAtFirstRelation) {
						break;
					}
				}
				//for all children
				tempResult = findRelationForChildren(onto,sourceUrl,target, this.depth, path);
				if (tempResult.size() > 0) {
					result.addAll(tempResult);
					if (this.stopAtFirstRelation) {
						break;
					}
				}
			}
		return result;
	}
	
	private Vector<Relation> findRelationForParents(String onto, String sourceUrl, String target, int theDepth, String path) throws RemoteException{
		String pLabel, pUrl, parent;
		Vector<Relation> resRF, tempResult;
		String source = Utilities.getIDFromURL(sourceUrl);
		Relation newRel;
		Vector<Relation> result = new Vector<Relation>();
		boolean continua = true;
		int i = 0;
		
		WatsonOntologyProxy ontoProxy = watsonService.getOntology(onto);
		
		log.info("DEPTH " + theDepth);
		String[] parents = ontoProxy.getConcept(sourceUrl).getSuperClasses();
		while (continua && i<parents.length) {
			
			rf.setCheckForAllRelationTypes(false);
			if (this.checkParents) rf.setCheckForSubClassRelations(true);
			if (this.checkDisjoint) rf.setCheckForDisjointRelations(true);
			if (this.checkRelsFrom) rf.setCheckNamedRelationsFrom(true);
			if (this.checkRelsTo) rf.setCheckNamedRelationsTo(true);
			
			//mechanism to avoid checking more times the same relation
			parent = parents[i];
			pLabel = Utilities.getIDFromURL(parent);
			
			i = i+1;
			if (wasCalled.contains(pLabel)) {
				log.info("Already Called rf for " + pLabel + " and " +  target);
			} else {
				log.info("Calling rf for " + pLabel + " and " +  target);
				
				resRF = rf.findRelationBetweenTerms(pLabel, target);
				wasCalled.add(pLabel);
				
				for (Relation r : resRF) {
					//go through the relations and decide what to return
					if (r.getName().equals("subClass")) {
						newRel = new Relation(source, target, "subClass");
					}  else {
							if (r.getName().equals("disjoint")) {
								newRel = new Relation(source, target, "disjoint");
								
							} else {
								newRel = new Relation(source, target, r.getName());
								
							}
					}
					newRel.setPath(new Path(path + ",subClassOf," + parent));
					newRel.setPath(r.getPath());
					log.info("first path " + path + ",subClassOf," + parent);
					log.info("newRel " + newRel.toString());
					
					Vector<Path> paths = newRel.getPathVector();
					for (Path pp : paths) {
						log.info("PATH");
						pp.printPath();
					}
					
					result.add(newRel);
					
					if (this.stopAtFirstRelation) continua = false;
				}
			}
			
		}
		
		//if we can continue and there is more depth to explore
		if (continua && (theDepth - 1) > 0) {
			
			i = 0;
			while (continua && i<parents.length) {
				pUrl = parents[i];
				tempResult = findRelationForParents(onto,pUrl,target, theDepth - 1, path + ",subClassOf," + parents[i]);
				if (tempResult.size() > 0) {
					result.addAll(tempResult);
					if (this.stopAtFirstRelation) continua = false;
				}
				i = i + 1;
			}
		}
		return result;
	}
	private Vector<Relation> findRelationForChildren(String onto, String sourceUrl, String target, int theDepth, String path) throws RemoteException{
		String cLabel, cUrl;
		Vector<Relation> resRF, tempResult;
		String source = Utilities.getIDFromURL(sourceUrl);
		Relation newRel;
		Vector<Relation> result = new Vector<Relation>();
		boolean continua = true;
		int i = 0;
		
		WatsonOntologyProxy ontoProxy = watsonService.getOntology(onto);
		
		String[] children = ontoProxy.getConcept(sourceUrl).getSubClasses();
		while (continua && i<children.length) {
			rf.setCheckForAllRelationTypes(false);
			if (this.checkParents) rf.setCheckForSubClassRelations(true);
			cLabel = Utilities.getIDFromURL(children[i]);
			if (wasCalledChild.contains(cLabel)) {
				
			} else {
				log.info("Calling rf for " + target + " and " +  cLabel);
				resRF = rf.findRelationBetweenTerms(target, cLabel);
				wasCalledChild.add(cLabel);
				for (Relation r : resRF) {
					//go through the relations and decide what to return
					if (r.getName().equals("subClass")) {
						newRel = new Relation(source, target, "subClass");
						log.info("newRel " + newRel.toString());
						log.info("firstPath " + path + ",superClassOf," + children[i]);
						result.add(newRel);
						if (this.stopAtFirstRelation) continua = false;
					}  
				}
			}
			i++;
		}
		
		//if we can continue and there is more depth to explore
		if (continua && (theDepth - 1) > 0) {
			i = 0;
			while (continua && i<children.length) {
				cUrl = children[i];
				tempResult = findRelationForChildren(onto,cUrl,target, theDepth - 1, path + ",superClassOf," + cUrl);
				if (tempResult.size() > 0) {
					result.addAll(tempResult);
					if (this.stopAtFirstRelation) continua = false;
				}
				i = i + 1;
			}
		}
		return result;
	}
	
	///================ Methods for setting the main parameters ==========
	
    /**
     * Indicates whether the algorithm should stop as soon as the first relation is found or whether 
     * it should identify all possible relations. By default, the algorithm stops at the first identified relation.
     * 
     * @param value If <i>true</i>, the algorithm stops as soon as a relation is found (default value). 
     * 				If <i>false</i>, the algorithm identifies as many relations as possible.
     */
	public void setStopAtFirstRelation(boolean value) {
		stopAtFirstRelation = value;
		rf.setStopAtFirstRelation(true);
		
	}

	/**
	 * Indicates whether the algorithm should check for all types of relations or not.
	 * By default, the algorithm checks for all types of relations. In order to check for
	 * certain type of relations only (for examples, some applications might only require
	 * disjointness relations), these should be selected with the appropriate, 
	 * individual set methods. 
	 * 
	 * @param value If <i>true</i>, the algorithm checks for all types of relations (default value). 
     * 				If <i>false</i>, the algorithm does not check for any type of relations. 
     * 				This value can be used when one wishes to reset the functionality of the algorithm.
	 */
	public void setCheckForAllRelationTypes(boolean value){
		checkChildren = value;
		checkParents = value;
		checkDisjoint = value;
		checkRelsTo = value;
		checkRelsFrom = value;
		rf.setCheckForAllRelationTypes(value);
	}
	
	/**
	 * Indicates whether the RelationFinder should check for subClass relations between the source and the target.
	 * 
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckForSubClassRelations(boolean value) {
		checkParents = value;
		rf.setCheckForSubClassRelations(value);
	}
	
	/**
	 * Indicates whether the RelationFinder should check for superClass relations between the source and the target.
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckForSuperClassRelations(boolean value) {
		checkChildren = value;
		rf.setCheckForSuperClassRelations(value);
	}
	
	/**
	 * Indicates whether the RelationFinder should check for Disjoint relations between the source and the target.
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckForDisjointRelations(boolean value) {
		checkDisjoint = value;
		rf.setCheckForDisjointRelations(value);
	}
	
	/**
	 * Indicates whether the RelationFinder should check for generic named relations between the source and the target.
	 * 
	 * This method is equivalent with calling both {@link setCheckNamedRelationsTo} and {@link setCheckNamedRelationsFrom}.
	 * 
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckAllNamedRelations(boolean value) {
		checkRelsTo = checkRelsFrom = value;
		rf.setCheckAllNamedRelations(value);
	}
	
	/**
	 * Indicates whether the RelationFinder should check for generic named relations having the source as range and target as domain.
	 * 
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckNamedRelationsTo(boolean value) {
		checkRelsTo = value;
		rf.setCheckNamedRelationsTo(value);
	}
	
	/**
	 * Indicates whether the RelationFinder should check for generic named relations having the source as domain and target as range.
	 * 
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckNamedRelationsFrom(boolean value) {
		checkRelsFrom = value;
		rf.setCheckNamedRelationsFrom(value);
	}
	/**
	 * Indicates which depth the RelationFinder should use
	 * 
	 * @param value default = 1;
	 */
	public void setDepth(int value) {
		this.depth = value;
		rf.setDepth(this.depth);
	}
}
