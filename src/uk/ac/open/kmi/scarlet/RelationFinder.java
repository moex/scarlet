package uk.ac.open.kmi.scarlet;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

import org.apache.log4j.Logger;

import uk.ac.open.kmi.scarlet.data.Path;
import uk.ac.open.kmi.scarlet.data.Relation;
import uk.ac.open.kmi.scarlet.utils.Utilities;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonCachedService;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonConceptProxy;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonOntologyProxy;
import fr.inrialpes.exmo.scarlet.context.watson.WatsonService;
//import uk.ac.open.kmi.watson.clientapi.WatsonService;
/**
 * Provides the main algorithms for discovering relations between two given terms.
 * 
 * Version 1.0 implements strategy S1 of the Scarlet methodology which identifies a relation 
 * within a single ontology (it does not combine information derived from multiple, different ontologies).
 * 
 * Changes in 1.2.:
 * - discovery of sibling relations
 * - use of inheritance to discover more disjoints, related_to and related_From
 * 
 * Changes in version 1.3:
 * - use of the watson-client-api-v2
 * 
 * @author rms364
 * @version 1.3
 */
public class RelationFinder {

	static Logger log = Logger.getLogger("uk.ac.open.kmi.scarlet");
	
	//PARAMETERS
	//true if a single relation is returned (the one that is found first)
	//false if we wish to return all relations 
	// false will mean to return all relations from each ontology (where one ontology can lead to more than one relations)
	public boolean stopAtFirstRelation; 
	
	//checkIndirectRelations = true if one wants to check indirect (i.e., inherited) relations
	private boolean checkIndirectRelations;
	
	//checkX - these are set true to indicate that the corresponding relations should be tested for;
	private boolean checkParents;
	private boolean checkChildren;
	private boolean checkDisjoint;
	private boolean checkRelsTo;
	private boolean checkRelsFrom;
	private boolean checkSiblings;
	private boolean checkDeepInheritance;
	
	//keeps a record of already checked parents in order to reduce checking the same parent several times
	private Vector<String> checkedParents = new Vector<String>();
	
	//all parents of a term in the current ontology, to avoid multiple readings for the sibling function;
	private Vector<String> allParentsOfTarget = new Vector<String>();
	
	private Path currentPath;
	
	//makes sure that only the sibling relation corresponding to the LCS is returned
	private boolean foundSiblingInCurrentOnto;
	
	//the depth to which indirect relations should be checked
	//i.e., how many levels of parents/children should be investigated
	//e.g, depth = 1 is the direct relations; depth = 2 is two parents above, two children bellow 
	private int depth;
	
	//private EntitySearch es;
	private Utilities utils = new Utilities();
	//public OntologyFinder of = new OntologyFinder();
	
	private WatsonService watsonService;
	
	private HashMap<String,Vector<String>> childParentPath = new HashMap<String,Vector<String>>();
	
	public RelationFinder(){
		/*
		EntitySearchServiceLocator locator = new EntitySearchServiceLocator();
		try{
			es = locator.getUrnEntitySearch();			
		}
		catch (Exception e) {
			e.printStackTrace();
			log.info(e.getCause());
		}
		*/
		watsonService = WatsonCachedService.getInstance();
		/*try {
			watsonService = WatsonCachedService.getInstance();
			// watsonService = WatsonDirectService.getInstance();
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		this.setStopAtFirstRelation(true);
		this.setCheckForAllRelationTypes(true);
		this.setCheckForInheritedRelations(false);
	
	}
	
	public static void main(String[] args) throws RemoteException {
		RelationFinder app = new RelationFinder();
		Vector<Relation> rel;
		
		app.setStopAtFirstRelation(false);
		app.setCheckForInheritedRelations(true);
		app.setCheckForAllRelationTypes(true);
		app.setDepth(4);
		app.setCheckDeepInheritance(true);
		
		rel = app.findRelationBetweenTerms("vegetable_product", "agricultural_product");
		Vector<Path> paths;
		for (Relation r:rel) {
			log.info(r.toStringNoURIs());
			log.info("In onto " + r.getPath().getOntology());
			paths = r.getPathVector();
			for (Path p: paths){
				log.info(p.toStringNoURI());

			}

		}
		log.info("Number of rels " + rel.size());
	}
	
	public void testtest() throws RemoteException{
	     Vector<String> v =utils.findFirstPathFromChildToParent("http://www.cyc.com/2003/04/01/cyc",
						"http://paoli.open.ac.uk/watson-cache/f/48d/d2d6/47bf9/580b858398/2b74cd7b58b52e768#Person", 
						 "http://paoli.open.ac.uk/watson-cache/f/48d/d2d6/47bf9/580b858398/2b74cd7b58b52e768#AnimalX",
						 "http://paoli.open.ac.uk/watson-cache/f/48d/d2d6/47bf9/580b858398/2b74cd7b58b52e768#Person");
	     log.info("ALL PATHS");
	     if (v!=null) {
	    	 for(String p:v){
	    		 log.info(p);
	    	 }
	     }
	}
	/**
	 * Finds relations between two given terms. This is the main method of this class. 
	 * 
	 * 
	 * Its functionality depends on a set of parameters, which need to be set before the actual calling.
	 * The main parameters refer to:
	 * 
	 * <ul>
	 * <li> the number of relations to be retrieved - the algorithm can either stop as soon as a relation is found, 
	 * or continue until all possible relations are found. By default, it will stop at the first identified relation. 
	 * This parameter can be modified with the {@link setStopAtFirstRelation} method. </li>
	 * <li> the type of the discovered relations - the algorithm can search for various types of relations. 
	 * By default, it will search for all the relation types. Using the "setCheckForXXXRelation" methods, the 
	 * algorithm can be instructed to search for certain relation types only.</li>
	 * <li> the use of inherited relations - when exploring online ontologies, the algorithm can use both direct 
	 * and inherited relations. By default, it will only check direct relations. Using the {@link setCheckForInheritedRelations()}
	 * method instructs the algorithm to also take into account inherited relations to a certain depth ({@link setDepth}).</li>
	 * <li> </li>
	 * 
	 * </ul>
	 * 
	 * @param source  the source term
	 * @param target  the target term
	 * @return a Vector of the {@link Relation} objects, each containing the details of a relation found between the two input terms
	 * @throws RemoteException
	 */
	public Vector<Relation> findRelationBetweenTerms(String source, String target) {
		boolean found = false;
		int i = 0;
		Vector<Relation> result = new Vector<Relation>();
		Vector<Relation> tempResult = new Vector<Relation>();
		
		try{
			Collection<String> res = watsonService.getOntosForKeywords(source, target);

			if (res.size() > 0) {
				if (this.stopAtFirstRelation){
					for(String ontology: res){
						result = this.findRelationInThisOntology(ontology, source, target);
						if (result.size()>0) {
							found = true;
							break;
						}
						//for (Relation r : result ) {
						//log.debug(" FOUND " + r.print() +  " In ontology " + onto);
						//log.debug(" ======================================= ");
						//}
					}
				} else {
					for (String s : res) {
						tempResult = this.findRelationInThisOntology(s, source, target);
						result.addAll(tempResult);
						//for (Relation r : tempResult) {
						//log.debug(" FOUND " + r.toString() + " In ontology " + s);
						//log.debug(" ======================================= ");
						//}
					}
				}
			} else {
				log.info("NO ontologies for " + source + " and " + target);
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}

		//log.debug("==================ALL Result PATHS ================");
		//for (Relation r : result ) {
			//log.debug(r.toString() + "  because  ");
			//log.debug(r.getPath().toString());
		//}
		return  result;
	}
	
	/**
	 * 
	 * Finds relations between two terms within a specified ontology. Note that some ontologies define two or more relations between the same concepts,
	 * for example, a named relation and its inverse (student studiesAt university; university hasStudent student). 
	 * 
	 * This method should only be called if it is known that the ontology in question contains concepts corresponding to the source and
	 * target terms. The functionality of this method depends on the same parameters as the {@link findRelationBetweenTerms}.
	 * 
	 * @param the URI of an ontology
	 * @param source term
	 * @param target term
	 * @return a Vector of the {@link Relation} objects, each containing the details of a relation found between the two input terms in the specified ontology
	 * @throws RemoteException
	 */
	
	public Vector<Relation> findRelationInThisOntology(String onto, String source, String target) throws RemoteException{
		String indivResult ="NoResult";
		
		Vector<Relation> result = new Vector<Relation>();
		Vector<Relation> temp, vr;
		boolean found = false;
		int d = depth;
		Relation r;
		String[] tempVector;
		foundSiblingInCurrentOnto = false;
		
		log.debug("onto " + onto);
		
		//read all parents of target
		allParentsOfTarget.removeAllElements();
		
		WatsonOntologyProxy ontoProxy =  watsonService.getOntology(onto);
		WatsonConceptProxy conceptProxy; 
		if (ontoProxy==null){
			System.out.println("ontoProxy null, onto=" + onto);
		}
		for (String conceptFound : ontoProxy.getConceptBasedOnKeyword(target)) {
			conceptProxy = ontoProxy.getConcept(conceptFound);
			 for (String targetParent : conceptProxy.getAllSuperClasses()) {
				 if (!allParentsOfTarget.contains(targetParent)) {
					 allParentsOfTarget.add(targetParent);
					 log.debug("   parent of target " + targetParent);
				 }
			 }
		}
		
		//for each entity
		for (String e : watsonService.getOntology(onto).getConceptBasedOnKeyword(source)) 
		{
			String path = onto + "," + e;
		
			if (checkParents) 
				if (!this.stopAtFirstRelation) {
					if (this.isDirectParentOfInOnto(onto, e, target, "\t", path)) {
						r = new Relation(source, target, "subClass");
						r.setPath(this.currentPath);
						result.add(r);
					}
				
				} else {
					if (!found) {
						if (this.isDirectParentOfInOnto(onto, e, target, "\t", path)) {
							r = new Relation(source, target, "subClass");
							r.setPath(this.currentPath);
							result.add(r);
							found = true;
						}
					}
				}
				
			if (checkChildren) 
				if (!this.stopAtFirstRelation) {
					if (this.isDirectChildOfInOnto(onto, e, target, "\t", path)){
						r = new Relation(source, target, "superClass");
						r.setPath(this.currentPath);
						result.add(r);
					}
				
				} else {
					if (!found) {
						if (this.isDirectChildOfInOnto(onto, e, target, "\t", path)) {
							r = new Relation(source, target, "superClass");
							r.setPath(this.currentPath);
							result.add(r);
							found = true;
						}
					}
				}
				
			if (checkDisjoint) 
				if (!this.stopAtFirstRelation) {
					r = this.isDisjointInOnto(onto, e, target, path);
					if (r!= null) {
						result.add(r);
					}
				} else {
					if (!found) {
						r = this.isDisjointInOnto(onto, e, target, path);
						if (r!= null) {
							result.add(r);
							found = true;
						}
					}
				}
			
			if (checkRelsTo) 
				if (!this.stopAtFirstRelation) {
					vr = this.hasRelationTo(onto, e,  target, path);
					if (vr != null)
					for (Relation rr : vr) {
						if (rr!= null) {
							result.add(rr);
						}
					}
				} else {
					if (!found) {
						vr = this.hasRelationTo(onto, e,  target, path);
						if (vr != null)
						for (Relation rr : vr) {
							if (rr!= null) {
								result.add(rr);
							}
						}
					}
				}
			
			if (checkRelsFrom) 
				if (!this.stopAtFirstRelation) {
					vr = this.hasRelationFrom(onto, e,  target, path);
					if (vr != null)
					for (Relation rr : vr) {
						if (rr!= null) {
							result.add(rr);
						}
					}
				} else {
					if (!found) {
						vr = this.hasRelationFrom(onto, e,  target, path);
						if (vr != null)
						for (Relation rr : vr) {
							if (rr!= null) {
								result.add(rr);
							}
						}
					}
				}
			
			//// ==== checking indirect relations =======////
			/// if depth = 1 then all the direct relations have already been checked
			if (this.checkIndirectRelations && (depth > 1)){
				
				if (checkParents || checkDisjoint || checkRelsTo || checkRelsFrom || checkSiblings) 
					checkedParents.removeAllElements();
					if (!this.stopAtFirstRelation) {
						temp = this.getInheritedRelations(onto,e, source, target, d--, "\t", path);
						result.addAll(temp);
					} else {
						if (!found) {
							temp = this.getInheritedRelations(onto,e, source, target, d--, "\t", path);
							if (!temp.isEmpty()) {
								result.addAll(temp);
								found = true;
							}
						}
					}
				if (checkChildren) 
					if (!this.stopAtFirstRelation) {
						
						if (isIndirectChildInOnto(onto, e, target, d--, "\t", path)) {
							r = new Relation(source, target, "superClass");
							r.setPath(this.currentPath);
							result.add(r);
						}
					} else {
						if (!found) {
							if (isIndirectChildInOnto(onto, e, target, d--, "\t", path)) 
								{
								r = new Relation(source, target, "superClass");
								r.setPath(this.currentPath);
								result.add(r);
								found = true;
								}
						}
					}
			}
		}//end for each entity
		return result;
	}
	
	
//=================Individual Matchers ====
	
	private boolean isDirectParentOfInOnto(String onto, String source, String target, String tab, String path) throws RemoteException {
		boolean result = false;
		WatsonOntologyProxy ontoProxy = watsonService.getOntology(onto);
		WatsonConceptProxy conceptProxy = ontoProxy.getConcept(source);
		
		String label;
		for (String parentConcept : conceptProxy.getSuperClasses()) {
			log.debug("\t\t T parent: " + parentConcept);
			label = utils.getIDFromURL(parentConcept);
		
			
			if (utils.sameClassBasedOnID(label, target)) {
				result = true;	
				log.info("FINAL_PATH " + path + ",subClassOf," + parentConcept);
				this.currentPath = new Path(path + ",subClassOf," + parentConcept);
			}
				
			}
		
		return result;
	}
	
	
	private boolean isDirectChildOfInOnto(String onto, String source, String target, String tab, String path) throws RemoteException {
		boolean result = false;
		String label;

		WatsonOntologyProxy ontoProxy =  watsonService.getOntology(onto);
		WatsonConceptProxy conceptProxy = ontoProxy.getConcept(source); 
		
		for (String child : conceptProxy.getSubClasses()) {
			log.debug("\t\t child: " + child);
			
			label = Utilities.getIDFromURL(child);
						
			if (Utilities.compareCompound(label, target)) {
				result = true;
				this.currentPath = new Path(path + ",superClassOf," + child);
				log.debug("FINAL_PATH " + path + ",superClassOf," + child);
			}
		}
		return result;
	}
	
	private Relation isDisjointInOnto(String onto, String source, String target,String path) throws RemoteException {
		Relation result = null;
		String label, key;
		Vector<String> paths = null;
		
		WatsonOntologyProxy ontoProxy = watsonService.getOntology(onto);
		
		for (String disjoint : this.getLogicalDisjunctsOf(onto, source)) {
			log.debug("\t\tDDD" + source +" disjoint with: " + disjoint);
			label = Utilities.getIDFromURL(disjoint);
			//if target literally matches one of the disjuncts
			if (Utilities.sameClassBasedOnID(label, target)) {
				log.debug("FINAL_PATH " + path + ",disjoint," + disjoint);
				this.currentPath = new Path(path + ",disjoint," + disjoint);
				result = new Relation(source, target, "disjoint");
				result.setPath(this.currentPath);
				
			} else {
				if (checkDeepInheritance){
					//if target is a subclass of this disjunct
					WatsonConceptProxy conceptProxy = ontoProxy.getConcept(disjoint);

					for(String subClassOfDisjunct : conceptProxy.getAllSubClasses()) {
						if(Utilities.sameClassBasedOnID(Utilities.getIDFromURL(subClassOfDisjunct), target)) {
							
							key = Utilities.getIDFromURL(subClassOfDisjunct) + "-" + Utilities.getIDFromURL(disjoint) +"-" + onto;
							if (childParentPath.containsKey(key)) {
								paths = childParentPath.get(key);
								log.info("retrieved " + key);
							} else {
								paths = utils.findFirstPathFromChildToParent(onto, subClassOfDisjunct, disjoint, onto+","+subClassOfDisjunct);
								childParentPath.put(key, paths);
							}
							log.info("Disj$$$_findFirstPathFromChildToParent " + utils.getIDFromURL(subClassOfDisjunct)+ ", parent " + utils.getIDFromURL(disjoint));
							if (paths != null) {
								this.currentPath = new Path(path + ",disjoint," + disjoint);
								result = new Relation(source, target, "disjoint");
								result.setPath(this.currentPath);
								result.setPath(new Path(paths.firstElement()));
							}
						}
					}
				}
			}
			}
		return result;
	}
	
	
	
	//returns the name of the relation for which source is a domain and target is a range
	// or "NoResult" if no such relation exists
	private Vector<Relation> hasRelationFrom(String onto, String source, String target, String path) throws RemoteException {
		Relation result = null;
		String label, key;
		Vector<String> paths = null;
		Vector<Relation> resultSet = null;

		WatsonOntologyProxy ontoProxy = watsonService.getOntology(onto);
		WatsonConceptProxy conceptProxy = ontoProxy.getConcept(source);

		log.debug("\t\t hasRelationFrom: ");
		String[] domainRels = conceptProxy.getPropertiesFrom(); //es.getDomainOf(onto, source);
		for (String propertiesFrom: domainRels) {
			log.debug("\t\t domain Of: " + propertiesFrom);
			for (String propertyRange : ontoProxy.getRelation(propertiesFrom).getRange()) {//es.getRange(onto,p);
				label = utils.getIDFromURL(propertyRange);
				if (utils.sameClassBasedOnID(label, target)) {	
					propertiesFrom = "R_FROM-" + propertiesFrom;
					log.debug("REL_FROM_FINAL_PATH " + path + "," + propertiesFrom + "," + propertyRange);
					this.currentPath = new Path(path + "," + propertiesFrom + "," + propertyRange);
					result = new Relation(source, target, propertiesFrom);
					result.setPath(this.currentPath);
					if (resultSet == null) resultSet = new Vector<Relation>();
					resultSet.add(result);
					
				} else {
					//if target is a subclass of this domain
					if (checkDeepInheritance){
						for(String sd: ontoProxy.getConcept(propertyRange).getAllSubClasses()) {
							if(utils.sameClassBasedOnID(utils.getIDFromURL(sd), target)) {
								key = utils.getIDFromURL(sd) + "-" + utils.getIDFromURL(propertyRange) +"-" + onto;
								if (childParentPath.containsKey(key)) {
									paths = childParentPath.get(key);
									log.info("retrieved " + key);
								} else {
									paths = utils.findFirstPathFromChildToParent(onto, sd, propertyRange, onto+","+sd);
									childParentPath.put(key, paths);
								}
								
								log.info("RFROM_$$$_findFirstPathFromChildToParent " + utils.getIDFromURL(sd)+ ", parent " + utils.getIDFromURL(propertyRange));
								if (paths != null) {
									propertiesFrom = "R_FROM-" + propertiesFrom;
									this.currentPath = new Path(path + "," + propertiesFrom + "," + propertyRange);
									result = new Relation(source, target, propertiesFrom);
									result.setPath(this.currentPath);
									result.setPath(new Path(paths.firstElement()));
									if (resultSet == null) resultSet = new Vector<Relation>();
									resultSet.add(result);
								}
							}
						}
					}
					
				}
			}
		}
		return resultSet;
	}
	
	//returns the name of the relation for which target is the domain and source is the range
	// or "NoResult" is no such relation exists
	private Vector<Relation> hasRelationTo(String onto, String source, String target, String path) throws RemoteException {
		Vector<Relation> resultSet = null;
		Relation result = null;
		Vector<String> paths = null;
		log.debug("\t\t hasRelationTo: ");
		

		WatsonOntologyProxy ontoProxy = watsonService.getOntology(onto);
		String label, key;
		
		log.debug("GET RELS TO ");
		for (String propertyTo: ontoProxy.getConcept(source).getPropertiesTo()){ //.getRangeOf(onto, source);) {
			log.debug(source + "\t\t range Of: " + propertyTo);
			String[] domains = ontoProxy.getRelation(propertyTo).getDomain();
			for (String domain : domains) {
				label = Utilities.getIDFromURL(domain);
				if (Utilities.sameClassBasedOnID(label, target)) {
					propertyTo = "R_TO-" + propertyTo;
					log.debug("REL_TO_FINAL_PATH " + path + "," + propertyTo + "," + domain);
					this.currentPath = new Path(path + "," + propertyTo + "," + domain);
					result = new Relation(target, source, propertyTo);
					result.setPath(this.currentPath);
					if (resultSet == null) resultSet = new Vector<Relation>();
					resultSet.add(result);
					
					//result = "R_TO-" + p;
					log.debug("\t\t rel : " + source + " ==>" + propertyTo + " <== " + domain);
					log.debug("FINAL_PATH " + path + "," + result + "," + domain);
					
					//this.currentPath = new Path(path + "," + result + "," + d);
				} else {
					if (checkDeepInheritance){
						//if target is a subclass of this domain
						String[] subRange = ontoProxy.getConcept(domain).getAllSubClasses();
						for(String sd:subRange) {
							if(Utilities.sameClassBasedOnID(Utilities.getIDFromURL(sd), target)) {
								
								key = Utilities.getIDFromURL(sd) + "-" + Utilities.getIDFromURL(domain) +"-" + onto;
								if (childParentPath.containsKey(key)) {
									paths = childParentPath.get(key);
									log.info("retrieved " + key);
								} else {
									paths = utils.findFirstPathFromChildToParent(onto, sd, domain, onto+","+sd);
									childParentPath.put(key, paths);
								}
								
								log.info("RTO_$$$_findFirstPathFromChildToParent " + utils.getIDFromURL(sd) + ", parent " + utils.getIDFromURL(domain));
								if (paths != null) {
									propertyTo = "R_TO-" + propertyTo;
									this.currentPath = new Path(path + "," + propertyTo + "," + domain);
									result = new Relation(target,source, propertyTo);
									result.setPath(this.currentPath);
									result.setPath(new Path(paths.firstElement()));
									if (resultSet == null) resultSet = new Vector<Relation>();
									resultSet.add(result);
								}
							}
						}
					}
				}
			}
		}
		
		return resultSet;
	}
	
	private boolean isSiblingAtThisParent(String parent, String path){
		boolean result = false;
		Path tempPath;
		
		log.debug("Sibling For parent " + parent);
		if (this.allParentsOfTarget.contains(parent)) {
			
			tempPath = new Path(path);
			result = true;
			this.currentPath = tempPath;
			/**
			if (!tempPath.containsConceptURL(parent)) {
				result = true;
				this.currentPath = tempPath;
			}
			**/
		}
		return result;
	}
	/**
	 * Checks for several types of inherited relations: subclass, disjoint, relTo, relFrom.
	 * 
	 * @param onto
	 * @param source
	 * @param target
	 * @param d
	 * @param tab
	 * @return all the inherited relations between source and target in this onto
	 * @throws RemoteException
	 */
	private Vector<Relation> getInheritedRelations(String onto, String sourceE, String origSource, String target, int d, String tab, String path) throws RemoteException {
		Vector<Relation> result = new Vector<Relation>();
		Vector<Relation> temp;
		boolean found = false;
		Relation r;
		Vector<Relation> vr;
		Path tempPath;

		WatsonOntologyProxy ontoProxy = watsonService.getOntology(onto);
		
		log.debug("Called with d  " + d);
		
		for (String parent : ontoProxy.getConcept(sourceE).getSuperClasses()) {
			
			if (!checkedParents.contains(parent)) {
			
				if (checkParents && (d > 1)) 
					if (!this.stopAtFirstRelation) {
						if (this.isDirectParentOfInOnto(onto, parent, target, "\t", path + ",subClassOf," + parent)) {
							r = new Relation(origSource, target, "subClass");
							r.setPath(this.currentPath);
							log.debug("PATH " + this.currentPath.toString());
							result.add(r);
						}
					
					} else {
						if (!found) {
							if (this.isDirectParentOfInOnto(onto, parent, target, "\t", path + ",subClassOf," + parent)) {
								r = new Relation(origSource, target, "subClass");
								r.setPath(this.currentPath);
								result.add(r);
								found = true;
							}
						}
					}
				
				//we only return one sibling relation within one ontology;
				//namely the one corresponding to the least common subsumer
				if (checkSiblings && !foundSiblingInCurrentOnto){
					log.debug("Checking siblings");
					if (!this.stopAtFirstRelation) {
						if (isSiblingAtThisParent(parent, path + ",subClassOf," + parent)) {
							r = new Relation(origSource, target, "sibling");
							r.setPath(this.currentPath);
							tempPath = getPathFromChildToParent(onto, target, parent);
							if (tempPath != null) {
								r.setPath(tempPath);
								result.add(r);
								foundSiblingInCurrentOnto = true;
							}
						}
					
					} else {
						if (!found) {
							if (isSiblingAtThisParent(parent, path + ",subClassOf," + parent)) {
								r = new Relation(origSource, target, "sibling");
								r.setPath(this.currentPath);
								tempPath = getPathFromChildToParent(onto, target, parent);
								if (tempPath != null) {
									r.setPath(tempPath);
									result.add(r);
									foundSiblingInCurrentOnto = true;
								}
								found = true;
							}
						}
					}
				}
					
					
				if (checkDisjoint) 
					if (!this.stopAtFirstRelation) {
						r = this.isDisjointInOnto(onto, parent, target, path + ",subClassOf,"+ parent);
						if (r!= null) {
							r.setDomain(origSource);
							result.add(r);
						}
					} else {
						if (!found) {
							r = this.isDisjointInOnto(onto, parent, target, path + ",subClassOf,"+ parent);
							if (r!= null) {
								r.setDomain(origSource);
								result.add(r);
								found = true;
							}
						}
					}
				
				if (checkRelsTo) 
					if (!this.stopAtFirstRelation) {
						vr = this.hasRelationTo(onto, parent,  target, path + ",subClassOf," + parent);
						if (vr != null)
						for (Relation rr : vr) {
							if (rr!= null) {
								rr.setRange(origSource);
								result.add(rr);
							}
						}
					
					} else {
						if (!found) {
							vr = this.hasRelationTo(onto, parent,  target, path + ",subClassOf," + parent);
							if (vr != null)
							for (Relation rr : vr) {
								if (rr!= null) {
									rr.setRange(origSource);
									result.add(rr);
									found = true;
								}
							}
						}
					}
				
				if (checkRelsFrom) 
					if (!this.stopAtFirstRelation) {
						vr = this.hasRelationFrom(onto, parent,  target, path + ",subClassOf," + parent);
						if (vr != null)
						for (Relation rr : vr) {
							if (rr!= null) {
								rr.setDomain(origSource);
								result.add(rr);
							}
						}
					
					} else {
						if (!found) {
							vr = this.hasRelationFrom(onto, parent,  target, path + ",subClassOf," + parent);
							if (vr != null)
							for (Relation rr : vr) {
								if (rr!= null) {
									rr.setDomain(origSource);
									result.add(rr);
									found = true;
								}
							}
						}
					}
				//if nothing has been found for this parent
				//initiate recursion 
				if (!found && (d>1)) { 
					log.debug("DD " + d + " path " + path + " calling " +  (d-1));
					temp = getInheritedRelations(onto, parent, origSource, target, d-1 ,tab +"\t", path + ",subClassOf," + parent);
					
					if (temp.size() > 0) {
						result.addAll(temp);
						found = true;
					}
				}
				
				checkedParents.add(parent);
			} //end if not already checked
		}//end for each parent
		
		
		return result;
	}
	
	/**
	 * Returns a path from a child to its parent in a given ontology.
	 * This is a help function that supports the sibling relation.
	 * @param child - the target term;
	 * @param parent - the URI of the parent;
	 * @return
	 */
	private Path getPathFromChildToParent(String onto,String child, String parent) throws RemoteException{
		RelationFinder rf = new RelationFinder();
		rf.setStopAtFirstRelation(true);
		rf.setCheckForAllRelationTypes(false);
		rf.setCheckForSubClassRelations(true);
		rf.setCheckForInheritedRelations(true);
		rf.setDepth(8);
		Relation r = new Relation();
		Path p;
		Utilities utils = new Utilities(); 
		
		Vector<Relation> rels = rf.findRelationInThisOntology(onto, child, Utilities.getIDFromURL(parent));
		
		if (rels.size() > 0) {
			r = rels.get(0);
			p = r.getPath();	
		} else {
			p = null;
		}
		
		return p;	
	}
	
	
	//returns true if target equals one of the children of source 
	//children are inspected recursively until a match is found or all children have been checked
	private boolean isIndirectChildInOnto(String onto, String source, String target, int d, String tab, String path) throws RemoteException {
		WatsonOntologyProxy ontoProxy = watsonService.getOntology(onto);
		WatsonConceptProxy conceptProxy = ontoProxy.getConcept(source);
		
		boolean directChild, indirectChild;
		
		for(String child: conceptProxy.getSubClasses()){

			log.debug("\t\t child: " + child);
			directChild = isDirectChildOfInOnto(onto, child, target, tab + "\t", path + ",superClassOf," + child); 
			if (directChild) {
				return true; 
			}  else if (d > 1) {
				indirectChild = isIndirectChildInOnto(onto, child, target, d--,tab +"\t", path + ",superClassOf," + child);
				if (indirectChild){
					return true;
				}
			}
		}
		return false;
	}
		
	///================ Helpers ====================///
	/**
	 * Returns the logical disjuncts by taking into account that this relation is inverse
	 */
	private String[] getLogicalDisjunctsOf(String onto, String concept) throws RemoteException{
		/*String[][] x = es.getRelationsFrom(onto, term);
		String[][] y = es.getRelationsTo(onto, term);
		
		for (String[] s : x) {
			if (s[1].equalsIgnoreCase("disjointWith")) {
				result.add(s[2]);
				log.debug(s[2]);
			}
		}
		for (String[] s : y) {
			if (s[1].equalsIgnoreCase("disjointWith")) {
				result.add(s[2]);
				log.debug(s[2]);
			}
		}*/
		
		String[] conceptsDeclaredAsDisjoint = watsonService.getOntology(onto).getConcept(concept).getIsDisjointWith();
		return conceptsDeclaredAsDisjoint;
	}
	
	///================ Methods for setting the main parameters ==========
	
    /**
     * Indicates whether the algorithm should stop as soon as the first relation is found or whether 
     * it should identify all possible relations. By default, the algorithm stops at the first identified relation.
     * 
     * @param value If <i>true</i>, the algorithm stops as soon as a relation is found (default value). 
     * 				If <i>false</i>, the algorithm identifies as many relations as possible.
     */
	public void setStopAtFirstRelation(boolean value) {
		stopAtFirstRelation = value;
	}

	/**
	 * Indicates whether the algorithm should check for all types of relations or not.
	 * By default, the algorithm checks for all types of relations. In order to check for
	 * certain type of relations only (for examples, some applications might only require
	 * disjointness relations), these should be selected with the appropriate, 
	 * individual set methods. 
	 * 
	 * @param value If <i>true</i>, the algorithm checks for all types of relations (default value). 
     * 				If <i>false</i>, the algorithm does not check for any type of relations. 
     * 				This value can be used when one wishes to reset the functionality of the algorithm.
	 */
	public void setCheckForAllRelationTypes(boolean value){
		checkChildren = value;
		checkParents = value;
		checkDisjoint = value;
		checkRelsTo = value;
		checkRelsFrom = value;
		checkSiblings = value;
	}
	
	/**
	 * Indicates whether the RelationFinder should check for subClass relations between the source and the target.
	 * 
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckForSubClassRelations(boolean value) {
		checkParents = value;
	}
	
	/**
	 * Indicates whether the RelationFinder should check for superClass relations between the source and the target.
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckForSuperClassRelations(boolean value) {
		checkChildren = value;
	}
	
	/**
	 * Indicates whether the RelationFinder should check for Disjoint relations between the source and the target.
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckForDisjointRelations(boolean value) {
		checkDisjoint = value;
	}
	
	/**
	 * Indicates whether the RelationFinder should check for generic named relations between the source and the target.
	 * 
	 * This method is equivalent with calling both {@link setCheckNamedRelationsTo} and {@link setCheckNamedRelationsFrom}.
	 * 
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckAllNamedRelations(boolean value) {
		checkRelsTo = checkRelsFrom = value;
	}
	
	/**
	 * Indicates whether the RelationFinder should check for generic named relations having the source as range and target as domain.
	 * 
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckNamedRelationsTo(boolean value) {
		checkRelsTo = value;
	}
	
	/**
	 * Indicates whether the RelationFinder should check for generic named relations having the source as domain and target as range.
	 * 
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckNamedRelationsFrom(boolean value) {
		checkRelsFrom = value;
	}
	
	/**
	 * Indicates whether the RelationFinder should check for a sibling relation between the source and target terms.
	 * 
	 * @param value "True" if such a relation will be checked, otherwise "False".
	 */
	public void setCheckSibling(boolean value) {
		checkSiblings = value;
	}
	
	/**
	 * Indicates whether the RelationFinder should check for inherited relations.
	 * @param value "True" if inherited relation should be checked. "False" if only direct relations will be checked.
	 * @see setDepth
	 */
	public void setCheckForInheritedRelations(boolean value) {
		checkIndirectRelations = value;
	}
	
	/**
	 * Sets the depth to which inherited relations should be checked.	This indicates how many levels of parents/children should be investigated. 
	 * E.g, For depth = 1 only the direct relations will be checked; For depth = 2 the algorithm checks two parents and two children a.s.o.
	 * @param value The depth to which inherited relations should be checked.
	 */
	public void setDepth(Integer value) {
		depth = value;
	}
	
	/**
	 * Indicates whether the RelationFinder should check for inherited relations.
	 * @param value "True" if inherited relation should be checked. "False" if only direct relations will be checked.
	 * @see setDepth
	 */
	public void setCheckDeepInheritance(boolean value) {
		checkDeepInheritance = value;
	}
	
}
