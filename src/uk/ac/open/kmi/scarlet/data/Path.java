package uk.ac.open.kmi.scarlet.data;
/**
 * Data structure for representing a path that lead to deriving a given relation.
 * @author rms364
 * @version 1.0
 */
import java.util.Vector;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import uk.ac.open.kmi.scarlet.utils.Utilities;
public class Path {
	static Logger log = Logger.getLogger("uk.ac.open.kmi.scarlet");
	
	private String ontology;
	private String path;
	private String htmlPath;
	private Utilities utils = new Utilities();
	private Vector<Relation> rels = new Vector<Relation>();
	private Pattern safeOntologyTrimmer = Pattern.compile(" ");
	
	public Path (String p) {
		this.processPath(p);
	}
	
	public Path () {
	}
	
	/**
	 * Adds a relation to the path.
	 * @param r
	 */
	public void addRelation(Relation r){
		rels.add(r);
	}
	
	private void processPath(String path) {
		Utilities util = new Utilities();
		Relation r = new Relation();
		String source, target;
		
		setOntology(path.substring(0, path.indexOf(",")));
		path = path.substring(path.indexOf(",") + 1, path.length()) + ",";
		
		String tempPath = "";
		htmlPath = "";
		String term, rel, shortRel = "";
		
		term = path.substring(0, path.indexOf(","));
		source = term;
		tempPath = tempPath + util.getIDFromURL(term);
		//log.debug("TempPath  " + tempPath);
		htmlPath = "<a href=\"http://watson.kmi.open.ac.uk/WatsonWUI/entity_look_up.html?q=" + term + " \" target=\"_blank\">" + util.getIDFromURL(term) + "</a>";
		path = path.substring(path.indexOf(",") + 1, path.length());
		
		while (path.length() > 0) {
			rel = path.substring(0, path.indexOf(","));
			path = path.substring(path.indexOf(",") + 1, path.length());
			
			term = path.substring(0, path.indexOf(","));
			target = term;
			
			if (rel.startsWith("R_")){
				shortRel = rel.substring(rel.indexOf("-") + 1, rel.length());
				
				if (rel.startsWith("R_TO")){
					htmlPath = htmlPath  + " <- " 
							+ "<a href=\"http://watson.kmi.open.ac.uk/WatsonWUI/entity_look_up.html?q=" + shortRel + " \" target=\"_blank\">" + util.getIDFromURL(shortRel) + "</a>" 
							+ " - ";
					tempPath = tempPath + " <- " + util.getIDFromURL(shortRel) + " - ";
					log.debug("TempPath  " + tempPath);
					r = new Relation(target, source, rel);
					log.debug("new rel  " + r.toString());
				}
			
				if (rel.startsWith("R_FROM")){
					shortRel = rel.substring(rel.indexOf("-") + 1, rel.length());
					htmlPath = htmlPath  + " - " 
							+ "<a href=\"http://watson.kmi.open.ac.uk/WatsonWUI/entity_look_up.html?q=" + shortRel + " \" target=\"_blank\">" + util.getIDFromURL(shortRel) + "</a>" 
							+ " -> ";
					tempPath = tempPath + " - " + util.getIDFromURL(shortRel) + " -> ";
					log.debug("TempPath  " + tempPath);
					r = new Relation(source, target, rel);
					log.debug("new rel  " + r.toString());
				} 
			} else {
				htmlPath = htmlPath  + " - " + rel + " -> ";
				r = new Relation(source, target, rel);
				log.debug("new rel  " + r.toString());
				tempPath = tempPath + " - " + rel + " - ";
				log.debug("TempPath  " + tempPath);
				}
			
			//tempPath = tempPath + util.getIDFromURL(term);
			log.debug("TempPath  " + tempPath);
			htmlPath = htmlPath + "<a href=\"http://watson.kmi.open.ac.uk/WatsonWUI/entity_look_up.html?q=" + term + " \" target=\"_blank\">" + util.getIDFromURL(term) + "</a>";
			path = path.substring(path.indexOf(",") + 1, path.length());
			
			this.addRelation(r);
			//in the next relation, the target of this relation will play the role of source
			source = target;
		}
		//path = tempPath;
	}
	
	/**
	 * Prints the path to the standard output.
	 */
	public void printPath(){
		for (Relation r: rels) {
			log.info(r.toString());
		}
	}
	
	/**
	 * Returns a String representation of the path.
	 */
	public String toString(){
		String result = "";
		String d1, r1, d2, r2;
		int i =0;
		
		Relation r = rels.elementAt(0);
		d1 = r.getDomain();
		r1 = r.getRange();
		
		result = r.toString();
		log.debug("res " + result);
		
		for ( i =1; i < rels.size(); i++) {
			r = rels.elementAt(i);
			d2 = r.getDomain();
			r2 = r.getRange();
			
			if (d2.equals(r1)) {
				result = result + "--" + r.getName() + "-->" + r2;
				log.debug("res " + result);
				r1 = r2;
			} else {
				result = result + "<--" + r.getName() + "--" + d2;
				log.debug("res " + result);
				r1 = d2;
			}	
		}
		
		return result;
	}
	
	/**
	 * Returns a String representation of the path where the URI's are excluded to 
	 * make the path easier to read.
	 */
	public String toStringNoURI(){
		String result = "";
		String d1, r1, d2, r2;
		int i =0;
		Utilities utils = new Utilities();
		
		Relation r = rels.elementAt(0);
		d1 = r.getDomain();
		r1 = r.getRange();
		
		result = r.toStringNoURIs();
		log.debug("res " + result);
		
		for ( i =1; i < rels.size(); i++) {
			r = rels.elementAt(i);
			d2 = r.getDomain();
			r2 = r.getRange();
			
			if (d2.equals(r1)) {
				result = result + "--" + utils.getIDFromURL(r.getName()) + "-->" +  utils.getIDFromURL(r2);
				log.debug("res " + result);
				r1 = r2;
			} else {
				result = result + "<--" +  utils.getIDFromURL(r.getName()) + "--" + utils.getIDFromURL(d2);
				log.debug("res " + result);
				r1 = d2;
			}	
		}
		
		return result;
	}
	
	/**
	 * Returns the relations that make up this path.
	 * @return a Vector of Relation objects.
	 */
	public Vector<Relation> getRelationsInPath(){
		return (Vector<Relation>)rels.clone();
	}
	
	/**
	 * Returns an HTML representation of the path.
	 * 
	 * @return a String containing the HTML representation of the path.
	 */
	public String htmlPath() {
		return htmlPath;
	}
	
	/**
	 * Returns the ontology from which the path was derived.
	 * @return
	 */
	public String getOntology() {
		return ontology;
	}
	
	public void setOntology(String onto){
		this.ontology = safeOntologyTrimmer.split(onto, 2)[0];
	}
	
	/**
	 * Returns the length of this path in terms of the number of relations it contains.
	 * @return
	 */
	public int getPathLength() {
		return rels.size();
	}
	
	/**
	 * Returns an abstract term contained by the path
	 * @return
	 */
	public String containsAbstracConcept(){
		boolean found = false;
		String[] abstracts = {"Thing", "Root", "Agent", "Event", "Collection", "List", "Individual"};
		int i = 0, j;
		Relation r;
		String s, t, ac, result = null;
		
		while (i < rels.size() && !found){
			r = rels.get(i);
			s = r.getDomain();
			t = r.getRange();
			j = 0;
			while (j < abstracts.length && !found) {
				ac = abstracts[j];
				if (ac.equalsIgnoreCase(s) || ac.equalsIgnoreCase(t) || s.endsWith(ac) || t.endsWith(ac)) {
					found = true;
					result = ac;
				}
			}
			i++;
		}
		return result;
	}
	
	/**
	 * checks whether the url of a concept is mentioned in a path
	 * @param c
	 * @return
	 */
	public boolean containsConceptURL(String c){
		boolean found = false;
		int i = 0;
		Relation r;
		String s, t;
		log.info("c" + c);
		String idc = utils.getIDFromURL(c);
		
		while (i < rels.size() && !found){
			r = rels.get(i);
			s = r.getDomain();
			t = r.getRange();
			
			if (utils.sameClassBasedOnID(utils.getIDFromURL(s), idc) || 
					utils.sameClassBasedOnID(utils.getIDFromURL(t), idc)) {
				found = true;
			}
			i++;
		}
		
		log.info("found " + c + " in path is " + found);
		
		return found;
	}
}
