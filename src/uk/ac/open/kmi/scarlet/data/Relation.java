package uk.ac.open.kmi.scarlet.data;
import java.util.Vector;
import uk.ac.open.kmi.scarlet.utils.Utilities;
/**
 * Data structure for representing a relation.
 * @author rms364
 * @version 1.0
 */
public class Relation {
	
	private String domain;
	private String range;
	private String name;
	private Path p;
	private Vector<Path> paths = new Vector<Path>();
	
	public Relation() {
	}
	
	/**
	 * Constructor.
	 * 
	 * @param domain the domain of the relation
	 * @param range the range of the relation
	 * @param name the name of the relation
	 */
	public Relation(String domain, String range, String name) {
		this.domain = domain;
		this.range = range;
		this.name = name;
	}
	
	/**
	 * Returns a string representation of the relation. 
	 * 
	 * @return a String of the form: domain --- name --> range;
	 */
	public String toString(){
		return domain + "--" + name + "-->" + range;
	}
	
	
	/**
	 * Returns a string representation of the relation by excluding the URI's in order to increase redability. 
	 * 
	 * @return a String of the form: domain --- name --> range;
	 */
	public String toStringNoURIs(){
		Utilities utils = new Utilities();
		return utils.getIDFromURL(domain) + "--" + utils.getIDFromURL(name) + "-->" + utils.getIDFromURL(range);
	}
	
	/**
	 * Sets the domain of the relation.
	 * @param domain - the domain of the relation. This string can either represent an URI or just a term label.
	 */
	public void setDomain(String domain)
	{
		this.domain = domain;
	}
	
	/**
	 * Sets the range of the relation.
	 * @param range - the range of the relation. This string can either represent an URI or just a term label.
	 */
	public void setRange(String range)
	{
		this.range = range;
	}
	
	/**
	 * Sets the name of the relation.
	 * @param name - the name of the relation. This string can either represent an URI or just a term label.
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	/**
	 * Sets the path which lead to discovering this relation. 
	 * @param path
	 */
	public void setPath(Path path)
	{
		paths.add(path);
		//this.p = path;
	}
	
	/**
	 * Gets the domain of the relation.
	 * @return
	 */
	public String getDomain()
	{
		return domain;
	}
	
	/**
	 * Gets the range of the relation.
	 * @return
	 */
	public String getRange()
	{
		return range;
	}
	
	/**
	 * Gets the name of the relation.
	 * @return
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Gets the path which lead to the discovery of the relation.
	 * 
	 * @deprecated
	 * Use getPathVector instead.
	 * @return
	 */
	public Path getPath()
	{
		//return p;
		return paths.get(0);
	}
	
	/**
	 * Returns the vector of paths which led to the discovery of the relation.
	 */
	public Vector<Path> getPathVector(){
		return paths;
	}
}
