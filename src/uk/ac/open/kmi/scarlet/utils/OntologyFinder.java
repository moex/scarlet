package uk.ac.open.kmi.scarlet.utils;
import java.rmi.RemoteException;
import java.util.Vector;
import org.apache.log4j.Logger;

import uk.ac.open.kmi.watson.clientapi.EntitySearch;
import uk.ac.open.kmi.watson.clientapi.EntityResult;
import uk.ac.open.kmi.watson.clientapi.EntitySearchServiceLocator;
import uk.ac.open.kmi.watson.clientapi.SearchConf;
import uk.ac.open.kmi.watson.clientapi.SemanticContentResult;
import uk.ac.open.kmi.watson.clientapi.SemanticContentSearch;
import uk.ac.open.kmi.watson.clientapi.SemanticContentSearchServiceLocator;

/**
 * Utility class for locating ontologies containing given terms.
 * @author rms364
 * @version 1.3
 */
public class OntologyFinder {
	
	static Logger log = Logger.getLogger("uk.ac.open.kmi.scarlet");

	private static SearchConf conf;
	private EntitySearch es;
	private SemanticContentSearch scs;
	private Utilities utils = new Utilities();
	
	static {
	 	conf = new SearchConf();
	 	conf.setEntities(SearchConf.CLASS);
		conf.setScope(SearchConf.LOCAL_NAME);
		conf.setMatch(SearchConf.EXACT_MATCH);
	}
	
	public OntologyFinder(){
		
		EntitySearchServiceLocator locator = new EntitySearchServiceLocator();
		try{
			es = locator.getUrnEntitySearch();			
		}
		catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
				
		SemanticContentSearchServiceLocator scslocator = new SemanticContentSearchServiceLocator();
		try{
			scs = scslocator.getUrnSemanticContentSearch();			
		}
		catch (Exception e) {
			e.printStackTrace();
			log.info(e.getCause());
		}
	}
	
/**
 * Ontology selection: returns a list of ontologies in which all keywords appear
 * 
 * @param source
 * @param target
 * @return
 * @throws RemoteException
 */
public Vector<String> getOntosForKeywords(String source, String target) throws RemoteException {
	String[] params = {source, target};
	Vector<String> result = new Vector<String>();
	
	for (SemanticContentResult onto : scs.getSemanticContentByKeywords(params, conf)) {
		result.add(onto.getURI());
	}
	return result;
}

public Vector<String> getOntosForTerm(String term) throws RemoteException{
	Vector<String> result = new Vector<String>();
	String[] query = {term};
	
	for (SemanticContentResult onto : scs.getSemanticContentByKeywords(query, conf)) {
		result.add(onto.getURI());
	}
	return result;
}

/*Searching for the exact URL's
 * 
 * Given a term and an ontology returns all the URL's that have this term as an exact match.
 * Deals with both simple and compound labels
 * TODO: this could be extended to take into account WN or to do the anchoring
 */
 public Vector<String> getURLsForTerm (String term, String onto) throws RemoteException {
	 Vector<String> result = new Vector<String>();
	 String label;
	 
	 for (EntityResult e : es.getEntitiesByKeyword(onto, term, conf)) {	//for those that match the keyword exactly
		 label = utils.getIDFromURL(e.getURI());
		 if (utils.sameClassBasedOnID(term, label)){
			 result.add(e.getURI());
		 }
	 }
	 return result;
 }
 
}
