package uk.ac.open.kmi.scarlet.utils;

import java.rmi.RemoteException;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.log4j.Logger;

import uk.ac.open.kmi.watson.clientapi.EntitySearch;
import uk.ac.open.kmi.watson.clientapi.EntitySearchServiceLocator;

/**
 * Utility class containing various, frequently used operations on labels.
 * @author rms364
 * @version 1.0
 */
public class Utilities {

	static Logger log = Logger.getLogger("uk.ac.open.kmi.scarlet");
	
	private EntitySearch es;
	
	public Utilities(){
		EntitySearchServiceLocator locator = new EntitySearchServiceLocator();
		try{
			es = locator.getUrnEntitySearch();			
		}
		catch (Exception e) {
			e.printStackTrace();
			log.warn(e.getCause());
		}
	}
	
	//=====Utilities====//
	
	/**
	 * This method compares whether two classes are equivalent.
	 * 
	 * The implementation can be wither based on label/id similarity or 
	 * on more complex matching criteria (e.g., taking into account context).
	 */
	public static boolean sameClassBasedOnID(String id1, String id2){
		boolean result = false;
		
		//TODO: implement support for compound labels
		result = compareCompound(id1, id2);
		//TODO: implement support for sameAs and owl:equivalentClass
		//result = id1.equalsIgnoreCase(id2);
		return result;
		
	}
	
	//compares two compound terms
	public static boolean compareCompound(String id1, String id2){
		boolean result = true;
		String token1, token2;
		
		//log.info.println("Compare compound " + id1 + "  and " +  id2);
		StringTokenizer st1 = new LabelSplitter().split(id1);
		StringTokenizer st2 = new LabelSplitter().split(id2);
		
		//comparison is only performed if they have the same amount of tokens
		if (st1.countTokens() == st2.countTokens()){
			
			while (st1.hasMoreTokens() && st2.hasMoreTokens() && result) {
				token1 = st1.nextToken();
				token2 = st2.nextToken();
				
				//log.info.println("Compare token " + token1 + "  and " +  token2);
				if (!token1.equalsIgnoreCase(token2)) result = false;
			}
			
		} else {
			result = false;
		}
		
		//log.info.println("Result " + result);
		return result;
	}
	
	public static String getIDFromURL(String url){
		String result = "";
		
		//log.info.println("TEST URL " + url);
		
		if (url.lastIndexOf("#") > -1){
			result =  url.substring(url.lastIndexOf("#") + 1, url.length());
		} else {
			result =  url.substring(url.lastIndexOf("/") + 1, url.length());
		}
		
		//log.info.println("CONV: " + url + "  " + result);
		return result;
	}
	
	public Vector<String> findAllPathsFromChildToParent(String onto,String child, String parent, String path) throws RemoteException {
		String[] parents = es.getSuperClasses(onto,child);
		Vector<String> results = null;
		Vector<String> tempResult;
		String pathR = "";
		
		for(String p:parents) {
			if (Utilities.sameClassBasedOnID(p, parent)) {
				pathR = path + ",subClassOf," + p;
				log.info("final path  " + pathR);
				if (results == null) results = new Vector<String>();
				results.add(pathR);
			} else {
				tempResult  = findAllPathsFromChildToParent(onto, p, parent, path + ",subClassOf," +p);
				if (tempResult != null) {
				if (results == null) results = new Vector<String>();
				results.addAll(tempResult);
				}
			}
		}
		return results;
	}
	
	public Vector<String> findFirstPathFromChildToParent(String onto,String child, String parent, String path) throws RemoteException {
		String[] parents = es.getSuperClasses(onto,child);
		Vector<String> results = null;
		Vector<String> tempResult;
		String pathR = "", p;
		boolean found = false;
		int i = 0;
		
		while (!found && (i < parents.length)) {
			p = parents[i];
			if(Utilities.sameClassBasedOnID(p, parent)) {
				pathR = path + ",subClassOf," + p;
				log.info("final path  " + pathR);
				if (results == null) results = new Vector<String>();
				results.add(pathR);
				found = true;
			} else {
				//tempResult  = findAllPathsFromChildToParent(onto, p, parent, path + ",subClassOf," +p);
				tempResult  = findFirstPathFromChildToParent(onto, p, parent, path + ",subClassOf," +p);
				if (tempResult != null) {
					found = true;
					if (results == null) results = new Vector<String>();
					results.addAll(tempResult);
				}
			}
			i = i + 1;
		}
		return results;
	}
	

}
